#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned char   uint8_t;
typedef unsigned int 	uint16_t;
typedef unsigned long	uint32_t;

typedef signed char     sint8_t;
typedef signed int 	sint16_t;
typedef long	        sint32_t;

#endif
 