/*******************************************************************************
* Description : Fisier header pentru comunicatia cu PC
* File name   : pc_prot.h
* Author      : Ing. Mihai Nechita   
* Date	      : 2020.02.23          
* Copyright   : (C) 2020 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __PC_PROT_H
#define __PC_PROT_H

#include "types.h"

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
/* ID-uri comenzi primite de la PC */
#define CMD_ID_BP     (((unsigned int)'B' << 8U) | 'P') /* Boiler power 0- OFF; 1 - ON  */
#define CMD_ID_TT     (((unsigned int)'T' << 8U) | 'T') /* Test type 01 - manual; 02 - automat; 03 - anduranta */
#define CMD_ID_EE     (((unsigned int)'E' << 8U) | 'E') /* Execution Elements */
#define CMD_ID_BT     (((unsigned int)'B' << 8U) | 'T') /* Boiler type 00 - Naturala; 01 - Fortat fixa; 02 - Fortata variabila; 03 - Semicondensata ;*/
#define CMD_ID_RQ     (((unsigned int)'R' << 8U) | 'Q') /* Execution Elements */
     
     
/* ID-uri status trimis catre PC */     
#define FBK_ID_SS     (((unsigned int)'S' << 8U) | 'S') /* System Status  */     
     

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/

extern uint16_t PC_Command_ID;
extern uint8_t PC_Command_DATA[2];
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions
 *----------------------------------------------------------------------------*/
void PC_Protocol( void );
void PC_Prot_Decode_CMD(void);

/* __PC_PROT_H */ 
#endif 
 