/*******************************************************************************
* File name   : rtc.h
* Author      : Ing. Mihai Nechita  
* Description : Fisier header pentru real time clock
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __rtc_h
#define __rtc_h
#include "system.h"

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/

/* DEFINE-URI PENTRU DS1307 */
/*****************************************************************************/
#if(SYS_RTC_MODEL == DS1307)
#define DS1337_READMODE             0xD1u
#define DS1337_WRITEMODE            0xD0u
#define DS1337_CTRL_REG_ADDRESS     0x0Eu
#define DS1337_STATUS_REG_ADDRESS   0x0Fu

#define DS1337_SEC_ADDR             0x00u
#define DS1337_MIN_ADDR             0x01u
#define DS1337_HOUR_ADDR            0x02u
#define DS1337_DAYOFWEEK_ADDR       0x03u
#define DS1337_DATE_ADDR            0x04u
#define DS1337_MONTH_ADDR           0x05u
#define DS1337_YEAR_ADDR            0x06u

#define DS1337_SEC_MASK             0x0Fu
#define DS1337_TENS_SEC_MASK        0x70u

#define DS1337_MIN_MASK             0x0Fu
#define DS1337_TENS_MIN_MASK        0x70u

#define DS1337_HOUR_MASK            0x0Fu
#define DS1337_TENS_HOURS_MASK      0x30u

#define DS1337_DAYSOFWHEEK_MASK     0x07u

#define DS1337_DATE_MASK            0x0Fu
#define DS1337_TENS_DATE_MASK       0x30u

#define DS1337_DATE_MASK            0x0Fu
#define DS1337_TENS_DATE_MASK       0x30u

#endif
/*****************************************************************************/

/* DEFINE-URI PENTRU PCF8523T */
/*****************************************************************************/
#if(SYS_RTC_MODEL == PCF8523T)

// Control registers Addresses
#define PCF8523T_CTRL_1_ADDRESS         0x00
#define PCF8523T_CTRL_2_ADDRESS         0x01
#define PCF8523T_CTRL_3_ADDRESS         0x02

// Time and Date Registers Addresses
#define PCF8523T_SECONDS_ADDRESS        0x03
#define PCF8523T_MINUTES_ADDRESS        0x04
#define PCF8523T_HOURS_ADDRESS          0x05
#define PCF8523T_DAYS_ADDRESS           0x06
#define PCF8523T_WEEKDAYS_ADDRESS       0x07
#define PCF8523T_MONTHS_ADDRESS         0x08
#define PCF8523T_YEARS_ADDRESS          0x09

// Alarm registers Addresses
#define PCF8523T_MINUTE_ALARM_ADDRESS   0x0A
#define PCF8523T_HOUR_ALARM_ADDRESS     0x0B
#define PCF8523T_DAY_ALARM_ADDRESS      0x0C
#define PCF8523T_WEEKDAY_ALARM_ADDRESS  0x0D

// Offset register address
#define PCF8523T_OFFSET_ADDRESS         0x0E

// CLOCKOUT and timer registers addresses
#define PCF8523T_TMR_CLKOUT_CTRL_ADDRESS         0x0F
#define PCF8523T_TMR_A_FREQ_CTRL_ADDRESS         0x10
#define PCF8523T_TMR_A_REG_ADDRESS               0x11
#define PCF8523T_TMR_B_FREQ_CTRL_ADDRESS         0x12
#define PCF8523T_TMR_B_REG_ADDRESS               0x13


// Registers masks

// Control_1 register bit masks
#define PCF8523T_CTRL_1_CAP_SEL                 0x80    // bit 7 crystal capacitor selection 0-7 pF  1-15 pF
#define PCF8523T_CTRL_1_STOP                    0x20    // bit 5, 6 is not used
#define PCF8523T_CTRL_1_SOFT_RESET              0x10    // bit 4 software reset 0-no reset 1-initiate soft reset
#define PCF8523T_CTRL_1_12_24                   0x08    // bit 3 hour mode selection 0-24 hour 1-12 hour
#define PCF8523T_CTRL_1_SIE                     0x04    // bit 2 second interrupt 0-disabled 1-enabled
#define PCF8523T_CTRL_1_AIE                     0x02    // bit 1 alarm interrupt 0-disabled 1-enabled
#define PCF8523T_CTRL_1_CIE                     0x01    // bit 0 correction interrupt 0-disabled 1-enabled

// Control_2 register bit masks
#define PCF8523T_CTRL_2_WTAF                    0x80    // bit 7 watchdog timer A interrupt 
#define PCF8523T_CTRL_2_CTAF                    0x40    // bit 6 countdown timer A interrupt
#define PCF8523T_CTRL_2_CTBF                    0x20    // bit 5 countdown timer B interrupt
#define PCF8523T_CTRL_2_SF                      0x10    // bit 4 second interrupt 
#define PCF8523T_CTRL_2_AF                      0x08    // bit 3 alarm interrupt 
#define PCF8523T_CTRL_2_WTAIE                   0x04    // bit 2 watchdog timer A interrupt
#define PCF8523T_CTRL_2_CTAIE                   0x02    // bit 1 countdown timer A interrupt
#define PCF8523T_CTRL_2_CTBIE                   0x01    // bit 0 countdown timer B interrupt

// Control_3 register bit masks
#define PCF8523T_CTRL_3_PM                      0x80    // bit 7-5 battery switch-over and battery low detection control 
#define PCF8523T_CTRL_3_BSF                     0x08    // bit 3 battery switch-over interrupt
#define PCF8523T_CTRL_3_BLF                     0x04    // bit 2 battery status 0-status ok; 1-battery status low
#define PCF8523T_CTRL_3_BSIE                    0x02    // bit 1 interrupt generation from switch-over flag BSF
#define PCF8523T_CTRL_3_BLIE                    0x01    // bit 0 interrupt generation from battery low flag

// Seconds register masks
#define PCF8523T_SEC_REGISTER_OS                0x80    // bit 7 clock integrity 0-guaranteed 1-not guaranteed
#define PCF8523T_SEC_REGISTER_TENS_OF_SECONDS   0x70    // bit 6-4 mask for seconds
#define PCF8523T_SEC_REGISTER_SECONDS           0x0F    // bit 3-0 mask for seconds

// Minutes register masks
#define PCF8523T_MIN_REGISTER_TENS_OF_MINUTES   0x70    // bits 6-4 tens of minutes
#define PCF8523T_MIN_REGISTER_MINUTES           0x0F    // bits 3-0 minutes

// Hours register masks
#define PCF8523T_HOUR_REGISTER_TENS_OF_HOURS    0x70    // bits 5-4 tens of hours
#define PCF8523T_HOUR_REGISTER_HOURS            0x0F    // bits 3-0 hours

// Days register masks
#define PCF8523T_DAYS_REGISTER_TENS_OF_DAYS     0x70    // bits 5-4 tens of days
#define PCF8523T_DAYS_REGISTER_DAYS             0x0F    // bits 3-0 days

// Weekdays register masks
#define PCF8523T_WEEKDAYS_REGISTER_WEEKDAYS     0x07    // bits 2-0 weekdays

// Months register masks
#define PCF8523T_MONTHS_REGISTER_TENS_OF_MONTHS 0x10    // bit 4 tens of months
#define PCF8523T_MONTHS_REGISTER_MONTHS         0x0F    // bits 3-0 months

// Years register masks
#define PCF8523T_YEAR_REGISTER_TENS_OF_YEARS    0xF0    // bits 7-4 tens of years
#define PCF8523T_YEAR_REGISTER_YEARS            0x0F    // bits 3-0 years

#define LUNI		0x01
#define MARTI		0x02
#define MIERCURI	0x03
#define JOI		0x04
#define VINERI		0x05
#define SAMBATA		0x06
#define DUMINICA	0x00

#define IANUARIE	0x01
#define FEBRUARIE	0x02
#define MARTIE		0x03
#define APRILIE		0x04
#define MAI		0x05
#define IUNIE		0x06
#define IULIE		0x07
#define AUGUST		0x08
#define SEPTEMBRIE	0x09
#define OCTOMBRIE	0x10
#define NOIEMBRIE	0x11
#define DECEMBRIE	0x12

#define RTC_SEC		0
#define RTC_MIN		1
#define RTC_HOUR	2
#define RTC_DAYS	3
#define RTC_WK_DAY	4
#define RTC_MONTH	5
#define RTC_YEAR	6
     


#endif
/*****************************************************************************/


/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/
//typedef struct
//{
//  unsigned char sec;
//  unsigned char min;
//  unsigned char hour;
//  unsigned char weekday;
//  unsigned char day;
//  unsigned char month;
//  unsigned char year;  
//}TIME;
/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
extern unsigned char TEMP_Time[7];	//buffer ce contine datele ce trebuiesc incarcate in PCF
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
unsigned char BCD2DEC(unsigned char bcd_code);
unsigned char DEC2BCD(unsigned char dec_code);
  #endif //_rtc_h_
 