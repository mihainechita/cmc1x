/*******************************************************************************
* Description : Fisier sursa pentru comunicatia cu PC
* File name   : pcmsg.c
* Author      : Ing. Mihai Nechita   
* Date	      : 23.08.2019          
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "msg.h"    // Headers include 
#include "stdmacros.h"
#include "crc.h"
#include "types.h"



/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private (static) defines
 *----------------------------------------------------------------------------*/
// Base of ASCII string to be converted (used as parameter by ASCII2Hex())
#define MSG_BASE_DEC      (unsigned char)10U
#define MSG_BASE_HEX      (unsigned char)16U

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public variables
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
static uint8_t MSG_Message[ MSG_LEN ]; // Message to be sent/received


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private functions
 *----------------------------------------------------------------------------*/

/******************************************************************************
 * Name       : PCMSG_Hex2ASCII
 * Parameters :-inString=string containing nibbles to be converted to ASCII
 *            :-outString=string where ASCII chars are to be stored
 * Return     : Nothing
 * Description:-Converts a string of bytes to ASCII chars
 *            : (each nibble is converted in an ASCII char)
 * Note       : Assumes input string is 2 bytes
 ****************************************************************
 * Index:  0      1                  0    1    2    3
 * -----------------------------------------------------
 * IN:   0xA1, 0x47   =======> OUT: 'A', '1', '4', '7'
 * -----------------------------------------------------
 *     outs[0] = (ins[0] >> 4  ) - 10 + 'A'
 *     outs[1] = (ins[0] & 0x0F) + '0'
 *     outs[2] = (ins[1] >> 4  ) + '0'
 *     outs[3] = (ins[0] & 0x0F) + '0'
 ******************************************************************************/
static void MSG_Hex2ASCII( const uint8_t*inString, unsigned char* outString )
{
    uint8_t l_nibble;         // nibble to be converted to ASCII char
    uint8_t l_nibbleIndex;    // nibble index: input string is seen as a string of nibbles, not of bytes !!!
    uint8_t l_inByte;         // input byte: byte to be converted to ASCII
    uint8_t l_outByte;        // output byte: ASCII byte

    l_nibbleIndex = 0U;
    do
    {
        //1 Read byte to be converted
        l_inByte = inString[ l_nibbleIndex >> 1U ];

        //2 Identify nibble to be converted
        if( 0x00 == (l_nibbleIndex & 0x01) ) // Even index
        {
            l_nibble = (l_inByte >> 4U);     // High nibble
        }
        else
        {
            l_nibble = (l_inByte & 0x0F);    // Low nibble
        }

        //3 Convert nibble to ASCII char
        if( l_nibble < (unsigned char)10U )
        {
            l_outByte = l_nibble + '0';
        }
        else
        {
            l_outByte = (l_nibble - 10U) + 'A' ;
        }
        //4 Move to next position/nibble
        *outString++ = l_outByte;
        l_nibbleIndex++;
    }
    while( l_nibbleIndex < 4U ); // Loop 4 times as there are four nibbles in two bytes
}

/******************************************************************************
 * Name       : PCMSG_ASCII2Hex
 * Parameters :-inbase= base of the ASCII string: 10=DEC-ASCII, 16=HEX-ASCII
 *            :-inString=string containing ASCII chars to be converted
 *            :-outString=string with bytes(nibbles) obtained after conversion
 * Return     : Nothing
 * Description:-Converts a string of ASCII chars to hex-bytes
 *            :
 * Note       : Assumes input string is 4 bytes
 ****************************************************************
 * 1. HEX-ASCII -> HEX
 * ------------------------------------------------------
 * Index:  0    1    2    3                 0    1
 * ------------------------------------------------------
 * IN:    'A', '1', '4', '7'  =====> OUT: 0xA1  0x47
 * ------------------------------------------------------
 *     outs[0] =           (ins[0] - 'A' + 10) << 4
 *     outs[0] = outs[0] | (ins[1] - '0'     )
 *     outs[1] =           (ins[2] - '0'     ) << 4
 *     outs[1] = outs[1] | (ins[3] - '0'     )
 * ------------------------------------------------------
 * 2. DEC-ASCII -> HEX
 * ------------------------------------------------------
 * Index:  0    1    2    3                 0    1
 * ------------------------------------------------------
 * IN:    '0', '2', '5', '7'  =====> OUT: 0x02  0x39 (57)
 * ------------------------------------------------------
 *     outs[0] =           (ins[0] - '0' ) * 10
 *     outs[0] = outs[0] + (ins[1] - '0' )
 *     outs[1] =           (ins[2] - '0' ) * 10
 *     outs[1] = outs[1] + (ins[3] - '0' )
******************************************************************************/
void MSG_ASCII2Hex( uint8_t inbase, const uint8_t* inString, uint8_t* outString )
{
    uint8_t l_nibble;         // nibble/digit to be converted from ASCII char
    uint8_t l_index;          // index (output string is seen as a string of nibbles, not of bytes)
    uint8_t l_inByte;         // input byte:  ASCII byte
    uint8_t l_outByte;

    l_index   = 0U;
    l_outByte = 0U;
    do
    {
        //1 Read byte to be converted
        l_inByte = *inString;

        //2 Convert ASCII char to HEX nibble/digit
        if( l_inByte < (unsigned char)'A' )
        {
            l_nibble = l_inByte - '0';
        }
        else// Only possible for HEX-ASCII
        {
            l_nibble = (l_inByte - 'A') + (unsigned char)10U;
        }

        //3 Identify place where to store nibble/digit
        // Note: First time will enter on even index and l_outByte will thus be initialized !
        if( 0x00 == (l_index & 0x01) )      // Even index
        {
            l_outByte = (unsigned char)( l_nibble * inbase );  // High nibble
        }
        else
        {
            l_outByte = (unsigned char)(l_outByte + l_nibble); // Low nibble
        }
        //4 Move to next position/nibble
        outString[ (l_index >> 1U) ] = l_outByte;
        l_index++;
        inString++;
    }
    while( l_index < 4U ); // Loop 4 times as there are four nibbles in two bytes
}



/*------------------------------------------------------------------------------
 * Public functions
 *----------------------------------------------------------------------------*/

/******************************************************************************
 * Name       : MSG_Put
 * Parameters : msgID=ID of message to be created
 *            : msgData=data to be put in message data field
 * Return     :-If message is put in USART TX buffer, returns PUT_OK
 *            :-if USART buffer is not EMPTY, returns !PUT_OK
 * Description:-Inserts message content in a message frame
 *            :-Puts message in USART TX buffer if it is empty
 * Note       : Big Endian storage is used for data and CRC fields !
 ******************************************************************************/
void MSG_Put( unsigned int msgID, unsigned int msgData )
{
    unsigned int  l_CRC;
    uint8_t string[2];
    uint8_t* lp_msg = &MSG_Message[0];

    //1. Create message frame (insert SOM, DEN, EOM chars) and fill frame with message contents
    //1.1 Write Start-of-Message char
    *lp_msg++ = MSG_SOM_CHAR;

    //1.2 Insert message ID in message frame (data is stored Big-Endian in message)
    *lp_msg++ = HI_BYTE( msgID );
    *lp_msg++ = LO_BYTE( msgID );

    //1.3 Insert message data in message frame (data is stored Big-Endian in message)
    string[0] = HI_BYTE( msgData );
    string[1] = LO_BYTE( msgData );
    MSG_Hex2ASCII( string, lp_msg );
    lp_msg += MSG_DATA_FIELD_LEN;

    //1.4 Insert DEN char (Data-ENd char)
    *lp_msg++ = MSG_DEN_CHAR;

    //1.5 Compute message CRC and insert it in message frame
    //l_CRC = PCMSG_ComputeCRC();
    l_CRC = CRC_ComputeAreaCRC( &MSG_Message[0], (MSG_DEN_OFFSET + 1U) );
    string[0] = HI_BYTE( l_CRC );   // High byte of int saved on byte 0 of string (Big Endian)
    string[1] = LO_BYTE( l_CRC );   // Low  byte of int saved on byte 1 of string
    MSG_Hex2ASCII( string, lp_msg );
    lp_msg += MSG_CRC_FIELD_LEN;

    //1.6 Write End-Of-Message char (at end of message)
    *lp_msg = MSG_EOM_CHAR;

    //2. Put message in USART TX buffer (if buffer is empty)
     USART_Transmit( MSG_Message );

}





