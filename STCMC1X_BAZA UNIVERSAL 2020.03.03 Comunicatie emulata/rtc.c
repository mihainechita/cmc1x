/*******************************************************************************
* Description : Fisier sursa pentru ceas de timp real PCF8523T
* File name   : rtc.c
* Author      : Ing. Mihai Nechita               
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "rtc.h"         


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char SYS_Time[7];
/*Forma TEMP_Temp ce contine date in zecimal, inca neformatate in BCD, formatarea se face in zona de incarcare date in TWI_Buffer
TEMP_Temp[0] contine valoare registrului de secunde 
TEMP_Temp[1] contine valoare registrului de minute 
TEMP_Temp[2] contine valoare registrului de ore 
TEMP_Temp[3] contine valoare registrului de zile 
TEMP_Temp[4] contine valoare registrului de zile ale saptamanii
TEMP_Temp[5] contine valoare registrului de luni 
TEMP_Temp[6] contine valoare registrului de ani */
unsigned char TEMP_Time[7];
/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/


unsigned char BCD2DEC(unsigned char bcd_code)
{
    unsigned char result,tens,units;
    tens=(bcd_code & 0xF0)>>4;
    tens=tens*10;
    units=bcd_code&0x0F;
    result=tens+units;
    return result;
    
}

unsigned char DEC2BCD(unsigned char dec_code)
{
    unsigned char result,high_nibble,low_nibble;
    high_nibble=(dec_code/10);
    high_nibble=high_nibble<<4;
    low_nibble=dec_code%10;
    result=high_nibble|low_nibble;
    return result;
    
}