/*******************************************************************************
* File name   : keys.h
* Author      : Ing. Mihai Nechita
* Description : Fisier header pentru citire butoane
* Date        :        
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __keys_h
#define __keys_h
#include "system.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Public defines
*----------------------------------------------------------------------------*/
// Masti selectie biti impari


// Masti apasare simpla butoane
#define KEY_LEFT         0x80
#define KEY_RIGHT        0x40
#define KEY_DOWN         0x20
#define KEY_UP           0x10
#define KEY_OK           0x08
#define KEY_ENTER        0x04
#define KEY_MENU         0x02
#define KEY_BACK         0x01

// Masti apasare 2 butoane simultan
#define KEYS_RIGHT_UP    ( KEY_RIGHT | KEY_UP )
#define KEYS_RIGHT_DOWN  ( KEY_RIGHT | KEY_DOWN )
#define KEYS_RIGHT_LEFT  ( KEY_RIGHT | KEY_LEFT )
#define KEYS_RIGHT_BACK  ( KEY_RIGHT | KEY_BACK )
#define KEYS_RIGHT_MENU  ( KEY_RIGHT | KEY_MENU )
#define KEYS_RIGHT_ENTER ( KEY_RIGHT | KEY_ENTER )
#define KEYS_RIGHT_OK    ( KEY_RIGHT | KEY_OK )
#define KEYS_UP_DOWN     ( KEY_UP | KEY_DOWN )
#define KEYS_UP_LEFT     ( KEY_UP | KEY_LEFT )
#define KEYS_UP_BACK     ( KEY_UP | KEY_BACK )
#define KEYS_UP_MENU     ( KEY_UP | KEY_MENU )
#define KEYS_UP_ENTER    ( KEY_UP | KEY_ENTER )
#define KEYS_UP_OK       ( KEY_UP | KEY_OK )
#define KEYS_DOWN_LEFT   ( KEY_DOWN | KEY_LEFT )
#define KEYS_DOWN_BACK   ( KEY_DOWN | KEY_BACK )
#define KEYS_DOWN_MENU   ( KEY_DOWN | KEY_MENU )
#define KEYS_DOWN_ENTER  ( KEY_DOWN | KEY_ENTER )
#define KEYS_DOWN_OK     ( KEY_DOWN | KEY_OK )
#define KEYS_LEFT_BACK   ( KEY_LEFT | KEY_BACK )
#define KEYS_LEFT_MENU   ( KEY_LEFT | KEY_MENU )
#define KEYS_LEFT_ENTER  ( KEY_LEFT | KEY_ENTER )
#define KEYS_LEFT_OK     ( KEY_LEFT | KEY_OK )
#define KEYS_BACK_MENU   ( KEY_BACK | KEY_MENU )
#define KEYS_BACK_ENTER  ( KEY_BACK | KEY_ENTER )
#define KEYS_BACK_OK     ( KEY_BACK | KEY_OK )
#define KEYS_MENU_ENTER  ( KEY_MENU | KEY_ENTER )
#define KEYS_MENU_OK     ( KEY_MENU | KEY_OK )
#define KEYS_ENTER_OK    ( KEY_ENTER | KEY_OK )

// Masti apasare 3 butoane simultan
#define KEYS_RIGHT_UP_DOWN    ( KEY_RIGHT | KEY_UP | KEY_DOWN )
#define KEYS_RIGHT_UP_LEFT    ( KEY_RIGHT | KEY_UP | KEY_LEFT )
#define KEYS_RIGHT_UP_BACK    ( KEY_RIGHT | KEY_UP | KEY_DOWN )
#define KEYS_RIGHT_UP_MENU    ( KEY_RIGHT | KEY_UP | KEY_DOWN )

/******************************************************************************/
// Masti stari butoane
// Masti buton LEFT
#define KEY_LEFT_PRESSED         ( ( KEY_LEFT  == KEYS_Pressed ) )
#define KEY_LEFT_NOT_PRESSED     ( !( KEY_LEFT_PRESSED) )

#define KEY_LEFT_RELEASED        ( ( KEY_LEFT  == KEYS_Released ) )
#define KEY_LEFT_NOT_RELEASED    ( !( KEY_LEFT_RELEASED) )

// Masti buton RIGHT
#define KEY_RIGHT_PRESSED        ( ( KEY_RIGHT  == KEYS_Pressed ) )
#define KEY_RIGHT_NOT_PRESSED    ( !( KEY_RIGHT_PRESSED) )

#define KEY_RIGHT_RELEASED       ( ( KEY_RIGHT  == KEYS_Released ) )
#define KEY_RIGHT_NOT_RELEASED   ( !( KEY_RIGHT_RELEASED) )

// Masti buton UP
#define KEY_UP_PRESSED           ( ( KEY_UP  == KEYS_Pressed ) )
#define KEY_UP_NOT_PRESSED       ( !( KEY_UP_PRESSED) )

#define KEY_UP_RELEASED          ( ( KEY_UP  == KEYS_Released ) )
#define KEY_UP_NOT_RELEASED      ( !( KEY_UP_RELEASED) )

// Masti buton DOWN
#define KEY_DOWN_PRESSED         ( ( KEY_DOWN  == KEYS_Pressed ) )
#define KEY_DOWN_NOT_PRESSED     ( !( KEY_DOWN_PRESSED) )

#define KEY_DOWN_RELEASED        ( ( KEY_DOWN  == KEYS_Released ) )
#define KEY_DOWN_NOT_RELEASED    ( !( KEY_DOWN_RELEASED) )

// Masti buton OK
#define KEY_OK_PRESSED           ( ( KEY_OK  == KEYS_Pressed ) )
#define KEY_OK_NOT_PRESSED       ( !( KEY_OK_PRESSED) )

#define KEY_OK_RELEASED          ( ( KEY_OK  == KEYS_Released ) )
#define KEY_OK_NOT_RELEASED      ( !( KEY_OK_RELEASED) )

// Masti buton ENTER
#define KEY_ENTER_PRESSED        ( ( KEY_ENTER  == KEYS_Pressed ) )
#define KEY_ENTER_NOT_PRESSED    ( !( KEY_ENTER_PRESSED) )

#define KEY_ENTER_RELEASED       ( ( KEY_ENTER  == KEYS_Released ) )
#define KEY_ENTER_NOT_RELEASED   ( !( KEY_ENTER_RELEASED) )

// Masti buton MENU
#define KEY_MENU_PRESSED        ( ( KEY_MENU  == KEYS_Pressed ) )
#define KEY_MENU_NOT_PRESSED    ( !( KEY_MENU_PRESSED) )

#define KEY_MENU_RELEASED       ( ( KEY_MENU  == KEYS_Released ) )
#define KEY_MENU_NOT_RELEASED   ( !( KEY_MENU_RELEASED) )

// Masti buton BACK
#define KEY_BACK_PRESSED        ( ( KEY_BACK  == KEYS_Pressed ) )
#define KEY_BACK_NOT_PRESSED    ( !( KEY_BACK_PRESSED) )

#define KEY_BACK_RELEASED       ( ( KEY_BACK  == KEYS_Released ) )
#define KEY_BACK_NOT_RELEASED   ( !( KEY_BACK_RELEASED) )


// Masti 2 taste apasate simultan
/******************************************************************************/
// Masti buton UP_DOWN
#define KEY_RIGHT_UP_PRESSED       ( ( KEYS_RIGHT_UP  == KEYS_Pressed ) )
#define KEY_RIGHT_UP_NOT_PRESSED   ( !( KEY_RIGHT_UP_PRESSED ) )

#define KEY_RIGHT_UP_RELEASED      ( ( KEYS_RIGHT_UP  == KEYS_Released ) )
#define KEY_RIGHT_UP_NOT_RELEASED  ( !( KEY_RIGHT_UP_RELEASED ) )


// Masti buton LEFT_RIGHT
#define KEY_LEFT_RIGHT_PRESSED     ( ( KEYS_LEFT_RIGHT  == KEYS_Pressed ) )
#define KEY_LEFT_RIGHT_NOT_PRESSED ( !( KEY_LEFT_RIGHT_PRESSED ) )

#define KEY_LEFT_RIGHT_RELEASED    ( ( KEYS_LEFT_RIGHT  == KEYS_Released ) )
#define KEY_LEFT_RIGHT_NOT_RELEASED   ( !( KEY_LEFT_RIGHT_RELEASED) )


/******************************************************************************/

// Masti 3 taste apasate simultan
#define KEY_RIGHT_UP_DOWN_PRESSED       ( ( KEYS_RIGHT_UP_DOWN  == KEYS_Pressed ) )
#define KEY_RIGHT_UP_DOWN_NOT_PRESSED   ( !( KEY_RIGHT_UP_DOWN_PRESSED ) )

#define KEY_RIGHT_UP_DOWN_RELEASED      ( ( KEYS_RIGHT_UP_DOWN  == KEYS_Released ) )
#define KEY_RIGHT_UP_DOWN_NOT_RELEASED  ( !( KEY_RIGHT_UP_DOWN_RELEASED ) )











/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
* Public (exported) variables
*----------------------------------------------------------------------------*/
extern unsigned char KEYS_UpdateEnabled;
extern unsigned char KEYS_Pressed;
extern unsigned char KEYS_Released;
/*******************************************************************************
*
* Functions (algorithms)
*
*******************************************************************************/
void KEYS_Update(void);
/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/


#endif //_keys_h
