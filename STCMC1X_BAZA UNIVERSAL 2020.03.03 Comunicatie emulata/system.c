/*******************************************************************************
* Description : Fisier sursa pentru functiile de sistem
* File name   : system.c
* Author      : Ing. Mihai Nechita
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compilator */
#include <ioavr.h>      
#include <inavr.h>      

/* System */
#include "system.h"
#include "types.h"         
#include "swtimers.h"
#include "io_drv.h"
#include "enables.h"
#include "stdmacros.h"
#include "adc.h"
#include "twi.h"
#include "isr.h"
#include "usart.h"
#include "sensors_prot.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char SYS_UserIntfError;        					// Eroare de user interface
unsigned char SYS_SensorError;							// Eroare de senzor
unsigned char SYS_BoilerErrors;
unsigned char SYS_State = IDLE;							// Variabila care determina starea standului; porneste setata cu IDLE
unsigned char SYS_ManualState = WAITING;					// Variabila care determina starea centralei in modul de testare manuala; setata initial cu WAITING
unsigned char SYS_AutoState = WAITING;						// Variabila care determina starea centralei in modul de testare automata; setata initial cu WAITING
unsigned char SYS_EnduranceState = WAITING;					// Variabila care determina starea centralei in modul de testare automata; setata initial cu WAITING					
unsigned char SYS_AutoTest=0;							// Variabila folosita pentru testarea automata, in functie de valoarea ei se selecteaza ce eroare va fi generata 
unsigned char SYS_StartAutoTest=0;                                              // Variabila ce indica automatului de testare automata faptul ca se poate incepe testul
unsigned char SYS_EnduranceTest=0;						// Variabila folosita pentru anduranta, in functie de valoarea ei se selecteaza ce eroare va fi generata 
unsigned char SYS_StartEnduranceTest=0;                                         // Variabila ce indica automatului de anduranta faptul ca se poate incepe testul

/* Numarul de secvente complete executate in meniul de anduranta */
unsigned int SYS_EnduranceTestsNumber=0;

/* Variabila principala ce determina daca se genereaza PWM pt simumlare debit pe PD4; */
/* valoarea sa determina numarul de treceri prin intreruperea ISR_TIMER1_Overflow*/
unsigned int SYS_Flow_Tick_Nr=((unsigned int)(TIMER1_INTERRUPT_FREQUENCY / SYS_FLOW_FREQUENCY)); 

/* Variabila ce contine request-urile de CH si DHW   0|0|0|0|0|0|CH_REQ|DHW_REQ
					             7|6|5|4|3|2|   1  |   0    */

/* State machine-urile de testare manuala sau automata vor folosi aceasta variabila
  pentru a simula semnalele de request CH sau DHW; pentru request CH se inchide 
  contactul de termostat ambient, iar pentru request DHW se simuleaza semnal PWM 
  cu ajutorul modulului Buzzer, echivalentul in debit a 7L/min */ 
unsigned char SYS_Requests=0;							

/* Variabila ce contine modelul centralei ce trebuie testata; In functie de model
se executa o anumita secventa de text, diferita de la model la model*/
__eeprom unsigned char SYS_BoilerType;

#if(STAND_MODEL==STAND_FORTATE)
/* Vector ce contine secventele de erori ce trebuiesc testate pe cele 4 tipuri de CT tiraj fortat*/
unsigned char TEST_SEQUENCES[MENU_BOILER_PARAMETERS_NR][TESTS_NR]={
    {E10, E11_SENSOR, E20, E26_TS, DHW},                                        // Erorile testate pe naturale
    {E10, E11_SENSOR, E20, E26_TS, E26_PA, DHW},                                // Erorile testate pe fixe
    {E10, E11_SENSOR, E20, E26_TS, E26_PA, E52, DHW},                           // Erorile testate pe variabile
    {E10, E11_SENSOR, E20, E26_TS, E26_PA, E40, E52, DHW}                       // Erorile testate pe semicondensate
};

#else
/* Vector ce contine secventele de erori ce trebuiesc testate pe CT condensate*/
unsigned char TEST_SEQUENCES[MENU_BOILER_PARAMETERS_NR][TESTS_NR]={
    {E10, E11_SENSOR, E20, E26_TS, E40, E51, DHW},                              // Erorile testate pe C38-25/29/35/BA
    {E10, E11_SENSOR, E20, E26_TS, E40, E51}                               // Erorile testate pe C38-CH1/CH2
};
#endif   



/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/* Functia SystemInit() configureaza controllerul Atmega32A functie de specificatiile
hardware ale standului, cum ar fi setarea porturilor A,B,C,D ca I/O, setarea timerelor,
setarea intreruperilor, modulului ADC etc.
Functia se executa o singura data inainte de bucla main*/

void SystemInit(void)
{
/* Dezactivare Watchdog pentru a astepta timpul de stabilizare tensiune de alimentare */
    WDTCR = 	 (1U << WDTOE ) | (1 << WDE);   //setare WDTOE si WDE
    WDTCR = 	 (0 << WDE);                    // resetare WDE in maxim 4 clock cycles pentru oprire watchdog
/* Delay de 2 secunde folosit pt stabilizare tensiune de alimentare la Power-Up */
    __delay_cycles(DELAY_1SEC);
    __delay_cycles(DELAY_1SEC);

/*[1] WATCH DOG */
 
    WDTCR = 	 (0U << WDP2 )        						// Setare WDT timeout
    		| (0U << WDP1 )        						// la
    		| (1U << WDP0 )        						// 32.5ms
    		| (1U << WDE  );       						// Enable WDT (start WDT)

    /* [2] Initializare PORTA */
    /***********************************************************************************************************************************/
    //PA0 - SYS_TEMP                                                        --- Temperature sensor
    //PA1 - C_LCD_/RD                                                       --- Read data LCD Command
    //PA2 - C_LCD_/WR                                                       --- Write data LCD Command
    //PA3 - C_LCD_C/D                                                       --- Command / Data LCD Signal
    //PA4 - C_LCD_RES                                                       --- Command LCD Reset
    //PA5 - C_LCD_/CE                                                       --- LCD Chip Enable
    //PA6 - EXT_RES_IO                                                      --- Reset PCB
    //PA7 - C_SCLK                                                          --- Serial Clock
    
    DDRA |=//   (1<<PA0) |
		(1<<PA1) | 
	    	(1<<PA2) |
		(1<<PA3) | 
		(1<<PA4) | 
		(1<<PA5) | 
		(1<<PA6) | 
		(1<<PA7) ;
    
    /***********************************************************************************************************************************/
    
    /* [3] Initializare PORTB */
    /***********************************************************************************************************************************/
    //PB0 - SDATA_CMD                                                       --- Output serial data
    //PB1 - SDATA_LCD                                                       --- Output serial data LCD
    //PB2 - RxPB                                                            --- Receive serial data from sensor board
    //PB3 - REG_CMD                                                         --- Register command
    //PB4 - REG_LCD                                                         --- Register command LCD
    //PB5 - MOSI                                                            --- Read data from ISP
    //PB6 - MISO                                                            --- Write data to ISP 
    //PB7 - SCK/SCLK                                                        --- Serial clock
    
    DDRB |=	(1<<PB0) |
	   	(1<<PB1) | 
	/*	(1<<PB2) | */
		(1<<PB3) | 
		(1<<PB4) | 
		(1<<PB5) | 
		(1<<PB6) |
		(1<<PB7) ;  
    
    
    /***********************************************************************************************************************************/      
    
    /* [4] Initializare PORTC */
    /***********************************************************************************************************************************/
    //PC0 - SCL                                                             --- Two Wire Interface serial clock 
    //PC1 - SDA                                                             --- Two Wire Interface serial data
    //PC2 - TCK                                                             --- OUTPUT JTAG TEST CLOCK
    //PC3 - TMS                                                             --- OUTPUT JTAG TEST MODE SELECT
    //PC4 - TDO                                                             --- OUTPUT JTAG TEST DATA OUT
    //PC5 - TDI                                                             --- OUTPUT JTAG TEST DATA IN
    //PC6 - TAST_IN                                                         --- INPUT keyboard
    //PC7 - EN_IN                                                           --- INPUT common feedback from vent,v3c,vana
    
    DDRC |=	(1<<PC0) | 
//	        (1<<PC1) | 
	    	(1<<PC2) | 
//              (1<<PC3) | 
//	        (1<<PC4) | 
		(1<<PC5) ;
//              (1<<PC6) | 
//              (1<<PC7) ;          
    /***********************************************************************************************************************************/
    
    /* [5] Initializare PORTD */
    /***********************************************************************************************************************************/
    //PD0 - RX                                                                       --- RECEIVE USART
    //PD1 - TX                                                                       --- TRANSMIT USART
    //PD2 - SYNCRO_50HZ                                                              --- Input for syncronisation 50 Hz
    //PD3 - SCANTEIE                                                                 --- Spark feedback
    //PD4 - BUZZER ON/OFF                                                            --- Buzzer command
    //PD5 - BKL ON/OFF                                                               --- LCD Backlight command ON/OFF
    //PD6 - CENTRALA ON/OFF                                                          --- Boiler command ON/OFF
    //PD7 - TxPB                                                                     --- Transmit serial data from motherboard to sensor board.
    
    DDRD |=
//         	(1<<PD0) |
		(1<<PD1) | 
//              (1<<PD2) | 
//              (1<<PD3) | 
		(1<<PD4) | 
		(1<<PD5) | 
		(1<<PD6) | 
		(1<<PD7) ; 
    SET(PORTD,CENTRALA_ONOFF);            //logica de pornire centrala este pe PNP

/* Setare linie de transmisie in HIGH (IDLE) */    
    SET(PORTD,TxPB);
    
    
    /***********************************************************************************************************************************/
    
    ///* [6] Setari ADC  */
    ////		// REFS1 REFS0	ADLAR MUX4 MUX3 MUX2 MUX1 MUX0
    ////		//   0     1      1    0    0    0    0    0
    ////		// AVCC este referinta de tensiune, cu valoare ajustata de stanga in ADCH si ADCL, cu canal 0 selectat
    //	ADMUX |=  ( ( 1<<REFS0 ) | ( 1<<ADLAR ) );
    ////		// ADEN ADSC ADATE ADIF ADIE ADPS2 ADPS1 ADPS0
    ////		//    1    0     0    1    1     1     1     1
    ////		// Activare ADC, fara mod autotrigger, resetare flag intrerupere, activare intrerupere ADC, setare prescaler la 128
    ////		// fAD = 16 MHz / 128 = 125000 Hz, tAD = 1 / fAD = 8 us
    ////		// normal coversion cycle is 13 * tAD = 104 us
    ////		// first coversion cycle is 25 * tAD = 200 us
    //	ADCSRA |= ( (1<<ADEN) | (1<<ADIF) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0) );   
    
    /* **************************** [5] INTRERUPERI EXTERNE **************************** */
    // ! ATENTIE ! MCUCR, MCUCSR, GICR si GIFR sunt registri globali/shared !
    //             De aceea, se foloseste REGISTER |= (...) not REGISTER = (...) !
    
///* Initializare semnale LCD (setare in mod inactiv) */    
    LCD_InitHW();
///* Initializare software LCD; setare font, numar linii etc; */       
    LCD_InitSW();
    
/* Timer 0: folosit pentru a genera intrerupere la perioada de 2 ms pentru comunicatie cu Shift Registers pentru comenzi centrala si LCD */         
    TCCR0 |= ( 1<< WGM01) | ( 1<< WGM00) | ( 1<< CS01) | ( 1<< CS00);
    TCNT0= 0x00;
    TIFR = 0x00;
    TIMSK |=  (1U << TOIE0 );
    
    
/* Timer 1: folosit pentru update swtimers si generare PWM pentru simulare debit */
    TCCR1A =   	( 0U << COM1A1)    						// Functionare normala port:OC1A/OC1B deconectat
		| ( 0U << COM1A0)    						
	    	| ( 1U << WGM10 )    						// Selectare mode 15 Fast PWM:
		| ( 1U << WGM11 );   						// Fast PWM with OCR1A = TOP

    TCCR1B =   	( 1U << WGM13 )    						// Fast PWM with OCR1A = TOP
		| ( 1U << WGM12 )    						// Fast PWM with OCR1A = TOP
	   	| ( 0U << CS12  )    						// Select timer clock: clkIO/1 (no prescaling)
		| ( 0U << CS11  )
		| ( 1U << CS10  );
    
    TIMSK |= (1U << TOIE1);
    __out_word( OCR1A , OCR1_VALUE );   					// Setat pt a genera o frecventa de 4000Hz 

/* Timer 2: folosit pentru comunicatia emulata cu placa de senzori
- functioneaza in mod CTC: clear timer on compare match ; TOP=OCR2
- output wave generation pin - dezactivat 
- timer2 prescaler is set to 8 ( fclktimer2 = 8Mhz / 8 = 1000KHz --> Ttimer2 = 1 / f = 1us )*/
    OCR2 = SENSORS_PROT_SAMPLE_BIT_DELAY;
    TCNT2 = 0x00;
    TCCR2 |= ( 1<<WGM21);
    START_TIMER2;	

    
/* Activare intreruperi externe */        
/* Activeaza intreruperea INT0 pentru sicronizarea cu linia de alimentare; ISR pentru INT0 va fi apelata la fiecare 10 ms */
    MCUCR |=  	(1U << ISC01)      // INT0 Sense Control: detecteaza fronturile POZITIVE pe INT0 (ISC0=11b)
	    	| (1U << ISC00);     //
/* Activare intrerupere INT0 */   
    GICR |= (1<<INT0);
/* Resetare flag intrerupere INT0 */
    GIFR |= (1U << INTF0);  
    
/* Activare intrerupere INT2; folosita pentru comunicatia emulata cu placa de senzori */   
    SET( GIFR, INTF2 ); 
    
/* Resetare flag intrerupere INT2 */    
    SET( GICR, INT2 );
    
/* Activare modul USART */     
    USART_Initialize();
    

    
/* Initializare timere software */
    SWTIMER_Initialize();                     
    
/* Timer pornit pentru reinitializare periodica LCD */
    SWTIMER_Start(SWTIMER_1SEC_LCD_REINIT,SWTIMER_1SEC_LCD_REINIT_LOAD);
/* Timer pornit pentru incarcarea continului temperaturii in LCD_DATA o data la 1 secunda */    
    SWTIMER_Start(SWTIMER_100MS_MENU_LOAD_TEMPERATURE,SWTIMER_100MS_MENU_LOAD_TEMPERATURE_LOAD);
    
/* Initializare modul ADC */    
    ADC_Initialize();
    
/* Functie de initializare cu offset-uri de la pozitia de capat LCD_API_DATA[0]*/
    LCD_API_VOFFSET_Init();
    
/* Initializare modul TWI*/    
    TWI_Init();
    
/* Pornire timer principal pt MAIN*/
    SWTIMER_Start(SWTIMER_10MS_TESTMAIN,SWTIMER_10MS_TESTMAIN_LOAD);
    
/* Pornire timer principal pt citire periodica la 1 secunda a timpului de la PCF8523*/
    SWTIMER_Start(SWTIMER_100MS_GET_RTC_TIME, SWTIMER_100MS_GET_RTC_TIME_LOAD);
    
/* Resetare WDT pentru a nu reseta controllerul inainte de a ajunge in main */
    __watchdog_reset();      
    __enable_interrupt();
    
/* Oprire comanda alimentare centrala; tranzistoarele de comanda alimentare CT 
  Faza si Nul sunt de tip PNP, deci vor porni alimentarea centralei in momentul
  in care procesorul porneste iar liniile de comanda tranzistor alimentare CT sunt
  in LOW; de aceea se apeleaza acest macro in zona de SistemInit(), pentru a opri
  alimentarea CT si pentru a nu electrocuta testerul care poate inca conecteaza 
  conductoarele ce duc 230VAC de la stand la centrala */    
    CENTRALA_OFF;
/* Acelasi principiu ca la alimentare centrala, tranzistor PNP pe comanda ionizare,
  de aceea linia de comanda modul de ionizare trebuie dusa in HIGH (dezactivata) pt a evita 
  E23 din partea centralei (ionizare in afara ciclului de ardere) */ 
    FLACARA_OFF;
    
}


/*******************************************************************************
* Description : Functie de testare a placii de centrala
* In functie de feedback-ul de la elementele de executie a centralei (pompa, ventilator,
vana de gaz, vana 3 cai) se comuta de la o stare la alta, actionand totodata si alte
elemente (termostat siguranta, presostat)
* time exec   : TBD us min; TBD ms max Optimizari NONE
* time exec   : TBD ms us min; TBD ms max Optimizari HIGH SPEED
******************************************************************************/

/* SYS_State_Update pentru fortate */
void SYS_State_Update(void)
{
/* Daca exista request de CH atunci se inchide contactul Termostat Ambient de pe
  modulul AUX0 */
    if(HI(SYS_Requests , CH_REQ ))
    {
	CMD_TA_ON;              
    }
    else
    {
	CMD_TA_OFF;  
    }
    
/* Daca exista request de DHW atunci se genereaza PWM cu ajutorul modulului 
  buzzer  */    
    if( HI ( SYS_Requests , DHW_REQ ) )
    {
	SYS_Flow_Tick_Nr = ((unsigned int)(TIMER1_INTERRUPT_FREQUENCY / SYS_FLOW_FREQUENCY));
    }
    else
    {
	SYS_Flow_Tick_Nr = 0;  
    }
  
    if(SYS_BoilerErrors!=0)
    {
        IO_DRV_Backlight_Toggle();
    } 
    
}
















