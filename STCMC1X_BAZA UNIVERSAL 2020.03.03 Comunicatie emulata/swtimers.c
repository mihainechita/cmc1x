
/*******************************************************************************
 * File name   : SWTIMERS.C
 * Author      : Birou Proiectare Automatizari-Soft   
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Includes
 *----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>

// General
//#include "stdmacros.h"  // Standard macros

// Services
#include "swtimers.h"    // Software timers

// Userinterface
#include "userintf.h"	// User interface


// System
#include "isr.h"        // Interrupt service routines (Must be after include of swtimer.h !) 
#include "stdmacros.h"
#include "system.h"


/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private defines
 *----------------------------------------------------------------------------*/
// Values used for update of SW-timers
#define SWTIMER_250US_PASSED    (unsigned char)1        // Value for 250us measured in 250us ticks
#define SWTIMER_10MS_AT250US    (unsigned char)40       // Value for 10ms measured in 250us ticks (50 for 200us ticks)
#define SWTIMER_100MS_PASSED	(unsigned char)10	// Value of 100ms counter: signals elapse of 100 ms (in  10 ms ticks)
#define SWTIMER_1SEC_PASSED	(unsigned char)10 	// Value of 1 sec counter: signals elapse of 1 sec  (in 100 ms ticks)
#ifdef __DEBUG_VERSION
#define SWTIMER_500MS_PASSED	(unsigned char)50	// Value of 500ms counter: signals elapse of 500 ms (in  10 ms ticks)
#endif

// Macros used in clock & ISRs periodic test
#define SWTIMER_1S_AT10MS       (unsigned char)100U    // Value for 1 second measured in 10ms ticks
#define SWTIMER_TIMEBASE_TOL    (unsigned char)7U      // Allowed tollerance for any timebase frequency/period (system clock, voltage zero-crossing)

// Main loop: execution time limits
#define SWTIMER_EXECTIME_MIN    (unsigned char)(1)     // 1.5ms measured in 250us ticks (err=+/-0.25ms)
#define SWTIMER_EXECTIME_MAX    (unsigned char)(32+1)  // 8.0ms measured in 250us ticks (err=+/-0.25ms)

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public variables
 *----------------------------------------------------------------------------*/
// Counter for testing system clock frequency and related interrupts (INT0 and TMR1_OVF)
// Counter measuring 1s in 10ms ticks based on external source (inc each zero cross of mains voltage - via INT0 ISR)
volatile unsigned char SWTIMER_ZCrossCnter1s;

/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
static SWTIMER_TYPE  TimersTable[ SWTIMER_NO ]; // Table that stores all timers
static unsigned char SWTIMER_Counter10ms;       // Counter that measures 10ms
static unsigned char SWTIMER_Counter100ms;      // Counter that measures 100ms
static unsigned char SWTIMER_Counter1sec;       // Counter that measures 1second
#ifdef __DEBUG_VERSION
static unsigned char SWTIMER_Counter500ms;      // Counter that measures 500ms (debug only)
#endif
static unsigned char SWTIMER_PrevTicksCnterNSM; // Value of ISR_TicksCounterNSM @ previous call of func SWTIMER_Update

// Counter for testing system clock frequency
// and related interrupts (INT0 and TMR1_OVF)
static unsigned char SWTIMER_Timer1Cnter1s;     // Counter measuring 1s in 10ms ticks based on internal source (inc each 40 overflows of timer1 - via TIMER1_OVF ISR)

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private functions
 *----------------------------------------------------------------------------*/
/*****************************************************************************
 * Name       : SWTIMER_UpdateClass
 * Parameters :-lowIndex = index in timers table where class of timers begins
 *            :-highIndex= index in timers table where class of timers ends
 * Return     : none
 * Description: Updates software timers from timers table
 *            : located on positions [lowIndex...highIndex]
 *****************************************************************************/
static void SWTIMER_UpdateClass( unsigned char lowIndex, unsigned char highIndex)
{
    SWTIMER_TYPE *l_TimerAddr;

    for( l_TimerAddr = &TimersTable[lowIndex]; l_TimerAddr <= &TimersTable[highIndex]; l_TimerAddr++ )
    {
        // Update timer only if it is enabled and still running
        if(  ( SWTIMER_DISABLED != (*l_TimerAddr) )
           &&( SWTIMER_STOPPED  != (*l_TimerAddr) )  )
        {
            (*l_TimerAddr)--;
        }
        else// Timer elapsed or disabled
        {
            // Nothing !
        }
    }
}

/*------------------------------------------------------------------------------
 * Public functions
 *----------------------------------------------------------------------------*/

/**************************************************************************
 * Name:        SWTIMER_Initialize
 * Parameters : none
 * Return     : none
 * Description: Initialization of TIMERS module: disables all timers
 * Warnings   :-This function should be called only once: after reset
 *            :-To be called before all other initialization functions
 *            : because there are initialization functions that start timers
 *            : and this function disables all timers !
 ***************************************************************************/
void SWTIMER_Initialize( void )
{
	SWTIMER_TYPE *l_TimerAddr;

	// Initialize timers table (disable all timers)
	for ( l_TimerAddr = &TimersTable[0]; l_TimerAddr <= &TimersTable[SWTIMER_NO-1U]; l_TimerAddr++ )
	{
		(*l_TimerAddr) = SWTIMER_DISABLED;
	}

    // Init/Clear these vars to eliminate linter infos !
  
    SWTIMER_Counter100ms = 0U;
    SWTIMER_Counter1sec  = 0U;
    #ifdef __DEBUG_VERSION
    SWTIMER_Counter500ms = 0U;
    #endif

    // Write such a value in this var to ensure that after reset
    // main-loop execution-time check at beginning of SWTIMER_Update()
    // will be OK to avoid triggering a false error after resets !
    ISR_TicksCounterNSM = ( SWTIMER_EXECTIME_MIN + 1 );
}

/**************************************************************************
 * Name       : SWTIMER_Update
 * Parameters : none
 * Return     : none
 * Description: Updates software timers at each 10 ms and performs
 *            : time-related checks.
 * Note       : To be called from main loop
 * Warning    :-Assumes main loop is executed in LESS THAN 10ms !
 *            : If main loop execution takes more than 10ms,
 *            : than SW timers will not be updated properly !
 *            : (replace flag with counter in that case)
 *            :-TMR1_OVF ISR assumes ticks-counter is cleared each 40 ticks !
 *            : If the *40* ticks are changed to a number not dividable
 *            : with 8, bug will be generated !
 ***************************************************************************/
void SWTIMER_Update( void )
{
    //unsigned char l_execTime;

    // If main loop time is outside valid range, then signal error
    // Note: This check is useful when TMR1_OVF ISR is not called (normally, called each 250us)
    //       OR when main-loop execution-time has an abnormal value
    //       OR when some other internal failure is present
    //l_execTime = ( ISR_TicksCounterNSM - SWTIMER_PrevTicksCnterNSM ); // Time between two consecutive calls of this func !
    if(0==1)//( ( l_execTime < SWTIMER_EXECTIME_MIN )
      // ||( l_execTime > SWTIMER_EXECTIME_MAX )  )
    {
        //SYS_IntrClockError = TRUE;
    }
    else
    {
        // Note: Do not clear error flag: critical error (once entered, never exit)!
        //SYS_IntrClockError = UNCHANGED;
     
   // Check if 250us elapsed (time to update timers)
   if(ISR_TicksCounterNSM >=SWTIMER_250US_PASSED)
    {
//        SET(PORTA,EXT_RES_IO); 
          // Substract "10ms" from counter: see warning (counter inc-ed by ISR - see ISR module)
          // Note: Previous ticks counter update is put immediately after update of ticks counter
          //       in order to minimize measurement error (ticks counter volatile !)      
          ISR_TicksCounterNSM-=SWTIMER_250US_PASSED;
          SWTIMER_PrevTicksCnterNSM = ISR_TicksCounterNSM;
          
          // Update counter used to check system clock and INT0 & TIMER1_OVF interrupts          
          SWTIMER_Timer1Cnter1s++;
          
          // Update 250us timers (SW timers with tick @ 250us)          
          SWTIMER_UpdateClass( 0U, (SWTIMER_10MS_CLASS_OFS - 1U) );
          SWTIMER_Counter10ms++;
        // Check if 10ms elapsed (time to update timers)  
         if( SWTIMER_Counter10ms >= SWTIMER_10MS_AT250US )
        {

          
//              SET(PORTA,EXT_RES_IO);  

            SWTIMER_Counter10ms -= SWTIMER_10MS_AT250US;     // Substract 10ms from counter
            // Update counter that measures 10ms and related timers if 10ms passed
            // variabila principala care triggereaza update citire tastatura/scriere LCD odata la 20 ms fiecare
            USERINTF_Refresh ^=0x01;
            
//          CLR(PORTA,EXT_RES_IO);  

            // Update 10ms timers (SW timers with tick @ 10ms)
            SWTIMER_UpdateClass( SWTIMER_10MS_CLASS_OFS, (SWTIMER_100MS_CLASS_OFS - 1U) );

            // Update counter that measures 100ms and related timers if 100ms passed
            SWTIMER_Counter100ms++;
            if( SWTIMER_Counter100ms >= SWTIMER_100MS_PASSED )
            {
                SWTIMER_Counter100ms -= SWTIMER_100MS_PASSED;   // Substract 100ms from counter
                //Update user-interface timing flag
                
                // Update 100ms timers (SW timers with tick @ 100ms)
                SWTIMER_UpdateClass( SWTIMER_100MS_CLASS_OFS, (SWTIMER_1SEC_CLASS_OFS - 1U) );
    
                // Update counter that measures 1second and related timers if 1second passed
                SWTIMER_Counter1sec++;
                if( SWTIMER_Counter1sec >= SWTIMER_1SEC_PASSED )
                {
                    SWTIMER_Counter1sec -= SWTIMER_1SEC_PASSED; // Substract 1second from counter if 1s passed
    
                    // Update 1sec timers (SW timers with tick @ 1second)
                    SWTIMER_UpdateClass( SWTIMER_1SEC_CLASS_OFS, (SWTIMER_1MIN_CLASS_OFS - 1U) );
                }
                else
                {
                    // Nothing: Wait for [at least] 1second to pass !
                }
            }
            else
            {
                // Nothing: Wait for [at least] 100ms to pass !
            }
        }   
    else
    {
      // Nothing: Wait for [at least] 10ms to pass !
    }  
              

            // In DEBUG version, timebase signal is simulated, because user interface is not connected !
            // Timebase signal is updated at each 500ms (500ms high, 500ms low)
      //      #ifdef __DEBUG_VERSION
       //     SWTIMER_Counter500ms++;                             // Increment counter
       //     if( SWTIMER_Counter500ms >= SWTIMER_500MS_PASSED )  // 500ms passed
       //     {
      //          SWTIMER_Counter500ms -= SWTIMER_500MS_PASSED;   // Clear counter
      //          RTC_TimeBaseSignal   ^= 0x01;                   // Toggle signal status
      //      }
   //         else
   //         {
                // Nothing: Wait for [at least] 500ms to pass !
   //         }
  //          #endif
//    CLR(PORTA,EXT_RES_IO); 
        }
        
        else
        {
            // Wait for [at least] 10ms to pass !
            //ISR_TicksCounterNSM = UNCHANGED;
            SWTIMER_PrevTicksCnterNSM = ISR_TicksCounterNSM;
        }

 
    }
}

/**********************************************************************
 * Name       : SWTIMER_TestIntrClock
 * Description:-Performs monitoring of system clock frequency
 *            : (frequency monitoring, IEC-60730-1, H.2.18.10.1)
 *            : and periodic testing of ISRs (INT0, TMR1_OVF) used
 *            : to measure time for this test.
 * Note       :-
 * Warning    :-Call in main loop ONLY after call of SWTIMER_Update().
 *            :-Test counters are volatile ! Load them locally to avoid 
 *            : problems !
 *********************************************************************/
void SWTIMER_TestIntrClock( void )
{
    unsigned char l_ZCrossCnter;               // tick @ 10ms (alias: zc)
    unsigned char l_Timer1Cnter;               // tick @ 10ms (alias: tc)

    // Load test counters (Remember: Test counters are volatile !)
    l_ZCrossCnter = SWTIMER_ZCrossCnter1s;
    l_Timer1Cnter = SWTIMER_Timer1Cnter1s;

    // Test 1: Assuming frequency of voltage zero-crossing & related ISR is OK,
    //         test if frequency of system clock or timer1 overflow ISR is in range

    // If 1 sec passed according to external reference (mains frequency)...
    if( SWTIMER_1S_AT10MS == l_ZCrossCnter )
    {
        // If 1-second measured based on system clock & Timer1 OVF ISR varies wih more than 5%
        // [of 1-second measured based on mains zero-crossing], signal interrupt/clock failure
        if(  ( l_Timer1Cnter < (SWTIMER_1S_AT10MS - SWTIMER_TIMEBASE_TOL) )
           ||( l_Timer1Cnter > (SWTIMER_1S_AT10MS + SWTIMER_TIMEBASE_TOL) )  )  // Note: This is somehow redundant: caught by leaf case 3 !
        {
            // Leaf case 1 (i.e. zc = 100, tc = 90)
            //SYS_IntrClockError = TRUE;
            //l_ZCrossCnter = UNCHANGED;
            //l_Timer1Cnter = UNCHANGED;
        }
        else// Otherwise, clear both 1-second counters (1 second passed)
        {
            // Note: Do not clear error flag: critical error (once entered, never exit)!

            // Leaf case 2 (i.e. zc = 100, tc = 98)
            //SYS_IntrClockError = UNCHANGED;
            l_ZCrossCnter = 0U;
            l_Timer1Cnter = 0U;
        }
    }

    // Test 2: Assuming frequency of system clock and timer1 overflow ISR is OK,
    //         test if frequency of voltage zero-crossing & related ISR is in range

    // If 1 sec passed according to internal reference (counter based on timer1 overflow & sys clock)...
    if( SWTIMER_1S_AT10MS == l_Timer1Cnter )
    {
        // If 1-second measured based on system clock & Timer1 OVF ISR varies wih more than 5%
        // [of 1-second measured based on mains zero-crossing], signal interrupt/clock failure


        // If 1-second measured based on mains zero-crossing & INT0 ISR varies wih more than 5%
        // [of 1-second measured based on system clock & Timer1 OVF ISR], signal interrupt/clock failure
        if(  ( l_ZCrossCnter < (SWTIMER_1S_AT10MS - SWTIMER_TIMEBASE_TOL) )
           ||( l_ZCrossCnter > (SWTIMER_1S_AT10MS + SWTIMER_TIMEBASE_TOL) )  ) // Note: This is somehow redundant: caught by leaf case 1 !
        {
            // Leaf case 3 (i.e. tc = 100, zc = 90)
            // SYS_IntrClockError = TRUE;
            // l_ZCrossCnter = UNCHANGED;
            // l_Timer1Cnter = UNCHANGED;
        }
        else// Otherwise, clear both 1-second counters (1 second passed)
        {
            // Note: Do not clear error flag: critical error (once entered, never exit)!

            // Leaf case 4 (i.e. tc = 100, zc = 98)
            // SYS_IntrClockError = UNCHANGED;
            l_ZCrossCnter = 0U;
            l_Timer1Cnter = 0U;
        }
    }

    // Save test counters
    SWTIMER_ZCrossCnter1s = l_ZCrossCnter;
    SWTIMER_Timer1Cnter1s = l_Timer1Cnter;
}

/**************************************************************************
 * Name       : SWTIMER_Start
 * Parameters :-timerIndex = index of timer in timers table
 *            :-loadValue  = value to be loaded in timer
 * Return     : none
 * Description: Loads the timer specified by timerIndex with loadValue
 ***************************************************************************/
void SWTIMER_Start( unsigned char timerIndex, SWTIMER_TYPE loadValue )
{
	TimersTable[ timerIndex ] = loadValue;
}

/**************************************************************************
 * Name       : SWTIMER_Stop
 * Parameters :-timerIndex = index of timer in timers table
 * Return     : none
 * Description: Stops (clears!) the timer specified by timerIndex
 ***************************************************************************/
void SWTIMER_Stop( unsigned char timerIndex)
{
    TimersTable[ timerIndex ] = SWTIMER_STOPPED;
}

/**************************************************************************
 * Name       : SWTIMER_Disable
 * Parameters :-timerIndex = index of timer in timers table
 * Return     : none
 * Description: Disables the timer specified by timerIndex
 ***************************************************************************/
void SWTIMER_Disable( unsigned char timerIndex )
{
	TimersTable[ timerIndex ] = SWTIMER_DISABLED;
}

/**************************************************************************
 * Name       : SWTIMER_GetStatus
 * Parameters : timerIndex = index of timer in timers table
 * Return     : Status/value of timer specified by timerIndex
 *            : -SWTIMER_DISABLED if timer is disabled
 *            : -SWTIMER_STOPPED  if timer finished (stopped) counting
 *            : -other value (timer's value) if timer still counting
 * Description: Return the status/value of the specified timer 
 ***************************************************************************/
SWTIMER_TYPE SWTIMER_GetStatus( unsigned char timerIndex )
{
    return TimersTable[ timerIndex ];
}




