/*******************************************************************************
* File name   : adc.h
* Author      : Ing. Mihai Nechita
* Description : Fisier header pentru achizitie adc
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef _ADC_H_
#define _ADC_H_


/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
/* Number of ADC-channels that are sampled  */
#define ADC_NO_CHANNELS			(uint8_t)1U

/* Size of ADC-samples buffer (measured in uint) */
#define ADC_BUFFER_SIZE			(uint8_t)1U

/* Mask for selection of MUX bits from ADMUX register (ADC register) */
#define ADC_MUX_MASK			(uint8_t)0x00U

/* Index of ADC-channel for acquisition of system temperature feedback */
#define ADC_SYS_TEMP            	(uint8_t)0U

/* Index of last channel to be sampled */
#define ADC_LAST_CHANNEL	        (uint8_t)1U

/* Index of first ADC-channel to be sampled  */
#define ADC_FIRST_CHANNEL		(uint8_t)0U

/* Number of samples to be acquired from an ADC-channel before computing the average value  */
#define ADC_NO_SAMPLES2AVG		(uint8_t)50		        

/* ADC-unit characteristics */

 /* Reference voltage: 5V */
#define ADC_REF_VOLTAGE                 5U     

/* Maximum value for 10-bit resolution */
#define ADC_MAXVALUE_10BIT              (uint16_t)1023U                        


/* ADC sampling control */
#define ADC_START_CONVERSION()          SET( ADCSRA, ADSC )

/* ADC average characteristics: number of samples */
#define ADC_NOSAMPLES_SYS_TEMP          50U

/* CH flow temperature sensor */
#define ADC_CHANNEL_SYS_TEMP            0U                   


/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
/* Samples counters */
extern volatile uint8_t ADC_SamplesCounters[ ADC_BUFFER_SIZE ]; 

/* Samples sums buffer written by ISR */
extern volatile uint16_t  ADC_SamplesSumsISR [ ADC_BUFFER_SIZE ]; 

/* Current ADC averages */
extern          uint16_t  ADC_CurrentAverages[ ADC_BUFFER_SIZE ]; 

/* Previous ADC averages */
extern          uint16_t  ADC_PreviousAverages[ ADC_BUFFER_SIZE ];
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
extern void ADC_Initialize( void );  
extern void ADC_UpdateData( void ); 
extern uint8_t ADC_IsAverageReady( uint8_t channel );
/* _adc_H_ */
 #endif 
 