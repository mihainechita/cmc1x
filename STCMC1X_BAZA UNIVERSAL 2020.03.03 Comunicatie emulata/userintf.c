/*******************************************************************************
* Description : Fisier sursa pentru interfata utilizator
* File name   : userintf.c
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>     
#include <inavr.h>      

/* System */
#include "stdmacros.h"
#include "swtimers.h"
#include "userintf.h"
#include "keys.h"
#include "enables.h"
#include "io_drv.h"
#include "lcdapi.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
uint8_t USERINTF_Refresh = 0U;	
/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
uint8_t USERINTF_OldRefr=0U;
volatile uint8_t USERINTF_NewRefr=0U;

uint8_t USERNITF_ActivePageUpdate=0U;

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/*********************************************************************
* Name		: USERINTF_Update()
* Parameters	: 
* Returns	: 
* Description	: Actualizeaza semafoare de executie update butoane si 
                 semnale de enable 
 *********************************************************************/

void USERINTF_Update( void ) 
{
	USERINTF_OldRefr = USERINTF_NewRefr;
	USERINTF_NewRefr = USERINTF_Refresh;
	
	if( USERINTF_OldRefr != USERINTF_NewRefr )
	{
            /* executa update de interfata sau de semnale de intrare */
            if( USERINTF_Refresh == 0U )
            {
                /* update butoane */
                KEYS_UpdateEnabled = TRUE;               
            }
            else
            {
                /* update semnale de feedback de pe blocurile de simulare (vezi hardware)  */
                ENABLES_UpdateEnabled = TRUE;            
            }
            
	}
}
