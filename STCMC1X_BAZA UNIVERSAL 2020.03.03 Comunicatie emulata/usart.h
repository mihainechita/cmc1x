/*******************************************************************************
* Description : Fisier header
* File name   : usart.h
* Author      : Ing. Mihai Nechita   
* Date	      : 05.12.2018           
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef _USART_H_
#define _USART_H_

#include "types.h"
#include "system.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/

// USART baudrate
// ! Warning ! USART_BAUDU1X_19200_BPS are valoarea doar pentru cuartz de 8 MHz ! 
#if (F_CPU == 8000000)
    #define USART_BAUDU1X_19200_BPS	   25U             // err = 0.2 % ( U2X = 0 Async mode)
#else
    #error "USART: F_CPU != 8MHz; trebuie modificata valoarea USART_BAUDU1X_19200_BPS  !!!"
#endif

// USART buffer sizes
// ! Warning ! Buffer size MUST be at least equal to message size !
#define USART_TX_BUFFER_SIZE    (uint8_t)52U
#define USART_RX_BUFFER_SIZE	(uint8_t)52U

// USART status flags masks
/* -----------------------------------------
 * | 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
 *  ---------------------------------------
 * | X  | X  |TBE | FE |DOR | X  |MOE |MRC |
 * ----------------------------------------- */
#define USART_STATUS_TBE         (1U << 5U)           // Transmission Buffer Empty (used as return value)
#define USART_STATUS_BL_FE       (1U << FE)           // Byte-Level errors: Frame error
#define USART_STATUS_BL_DOR      (1U << DOR)          // Byte-Level errors: Data OverRun error
#define USART_STATUS_ML_OE       (1U << 1U  )         // Message-Level errors: message/buffer overflow error: message longer than buffer size or more than one message received
#define USART_STATUS_ML_RC       (1U << 0U  )         // Message-Level: Message Reception Complete
#define USART_RX_STATUS_MASK     (uint8_t)0x1F          // Mask for reception status flags
#define USART_RX_ERRORS_MASK     (uint8_t)0x1E          // Mask for reception error  flags

// USART functions return codes
#define USART_STATUS_RX_OK       USART_STATUS_ML_RC // Message received successfully
#define USART_STATUS_TX_OK       USART_STATUS_TBE   // Message written in TX buffer successfully
#define USART_STATUS_TX_NOK      0x00               // Message not written in TX buffer 

// Message special characters
#define USART_SOM_CHAR	         '>'	            // Start Of Message character
#define USART_EOM_CHAR	         '<'                // End Of Message character

// Defines for getting USART TX status
// Warning: UDRIE is a bit position. Consequently use macro HI, instead of HI_BITS!
#define USART_TX_ISREADY()       LO( UCSRB, UDRIE )
#define USART_TX_ISBUSY()        HI( UCSRB, UDRIE )




/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/




/* Structuri de receptie date USART */
/* Buffer de receptie USART */
extern volatile uint8_t USART_RX_Buffer[ USART_RX_BUFFER_SIZE ];	

/* Buffer de receptie USART */
extern volatile uint8_t USART_RX_Head;          

/* Buffer de receptie USART */
extern volatile uint8_t USART_RX_Tail;

/* Buffer de receptie USART */
extern volatile uint8_t USART_RX_Buffer_Length;


/* Structuri de transmisie date USART */
/* Buffer de transmisie USART */
extern volatile uint8_t USART_TX_Buffer[ USART_TX_BUFFER_SIZE ];	

/* Buffer de transmisie USART */
extern volatile uint8_t USART_TX_Head;          

/* Buffer de transmisie USART */
extern volatile uint8_t USART_TX_Tail;

/* Buffer de transmisie USART */
extern volatile uint8_t USART_TX_Buffer_Length;




/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
extern void USART_Initialize( void );
extern void USART_Transmit( const unsigned char *srcString   );
extern void USART_Receive ( void );


#endif



