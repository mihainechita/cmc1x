/*******************************************************************************
* File name   : lcd.h
* Author      : Ing. Mihai Nechita
* Description : Header pentru functii si alocari LCD
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __LCD_h
#define __LCD_h

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/







/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/

     
     
     
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/



/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/










#endif //_LCD_H_
 