/*******************************************************************************
* Description : Fisier sursa pentru achizitia ADC
* File name   : adc.c
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>      
#include <inavr.h>      

/* System */
#include "types.h"
#include "adc.h"       
#include "isr.h"
#include "temp.h"
#include "stdmacros.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
/* Numaratori de sample-uri */
volatile uint8_t ADC_SamplesCounters[ ADC_BUFFER_SIZE ]; 

/* Buffer ce contine suma sample-urilor scris de catre ISR */
volatile uint16_t  ADC_SamplesSumsISR [ ADC_BUFFER_SIZE ]; 

/* Media curenta a sample-urilor */
         uint16_t  ADC_CurrentAverages[ ADC_BUFFER_SIZE ]; 
         
/* Media precedenta a sample-urilor */         
         uint16_t  ADC_PreviousAverages[ ADC_BUFFER_SIZE ];
         
/* Flag ce indica daca exista sample-uri */
volatile uint8_t ADC_SamplesAvailable = FALSE; 		


/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/


static uint16_t  ADC_SamplesSums    [ ADC_BUFFER_SIZE ];   			// Buffer ce contine suma sample-urilor folosit pentru a calcula media lor
static uint16_t  ADC_SumsReady;                            			// Flag ce semnalizeaza daca suma sample-urilor este ready; fiecare bit din acest byte semnalizeaza ready pt fiecare canal ADC
static uint16_t  ADC_AveragesReady;                        			// Flag ce semnalizeaza daca media sample-urilor este ready; fiecare bit din acest byte semnalizeaza ready pt fiecare canal ADC
static uint8_t ADC_AverageChannel;                       			// Index pentru urmatorul canal ce trebuie mediat

// Numarul de sample-uri ce trebuiesc citite pentru fiecare canal al moduluilui ADC; se foloseste doar ADC0 pentru senzorul NTC de pe placa de baza
static __flash const uint8_t ADC_SamplesNumbersTable[ ADC_BUFFER_SIZE ] =
{
    ADC_NOSAMPLES_SYS_TEMP
//    1U,
//    1U,
//    1U,
//    1U,
//    1U,
//    1U,
//    1U
   
};

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/*******************************************************************************
* Nume       	: ADC_Initialize
* Parametri	: None
* Descriere	: Initializeaza modulul ADC
* Atentie	: Se apeleaza o singura data, in cadrul functie SystemInit(), inainte
* 	       	de a incepe bucla main
 ******************************************************************************/
void ADC_Initialize( void )
{

    ADMUX  =  	   ( 0U << REFS1 ) 						// Select ADC channel: channel 0 (by default)
		 | ( 0U << REFS0 );						// Select voltage reference for ADC: AREF, internal Vref disabled

	ADCSRA =   (1U << ADEN )    						// Enable ADC (ADEN)
	         | (1U << ADIE )    						// with ADC End-Of-Conversion Interrupt triggering (ADIE)
	         | (1U << ADPS2)    						// with ADC clock: Fosc/64 = 4000 KHz/64 = 62.50Khz (ADPS) => T = 16us (14 * 16us = 224us per sample)
	         | (1U << ADPS1)
	         | (0U << ADPS0);

    // Clear/Init ready flags [to eliminate linter infos] !
    ADC_SumsReady     = 0U;
    ADC_AveragesReady = 0U;
    
}


/****************************************************************************
 * Name       : ADC_UpdateData 
 * Parameters : None  
 * Description:-Computes average values for ADC-samples
 *            : and updates related data 
 * Note       : Periodic function (called at each 10ms) 
 ****************************************************************************/
void ADC_UpdateData( void )
{
    uint8_t l_schannel;
    uint8_t l_achannel;
    uint8_t l_number;
    uint16_t  l_sum;

    //[1] Update for each channel the samples sum, if all samples were acquired and summed for that channel
    // Search all channels for data to be updated (including the two virtual channels)
    for( l_schannel = 0U; l_schannel < ADC_BUFFER_SIZE; l_schannel++ )
    {
        // If channel samples were acquired and summed, copy sum written by ISR to sum for application
        if( 0U == ADC_SamplesCounters[ l_schannel ] )
        {
            // Update function sum and clear ISR sum
            ADC_SamplesSums   [ l_schannel ] = ADC_SamplesSumsISR[ l_schannel ];
            ADC_SamplesSumsISR[ l_schannel ] = 0U;
            // Signal channel sum is ready to be used for averaging
            SET( ADC_SumsReady, l_schannel );
            // Last: Load channel samples-counter (allow ISR to add samples to channel sum) !
            ADC_SamplesCounters[ l_schannel ] = ADC_SamplesNumbersTable[ l_schannel ];
        }
        else
        {
            // Nothing ! 
        }
    }

    //[2] Update previuos and current averages for next channel to be averaged
    l_achannel = ADC_AverageChannel;      // Load index of channel to average
    if( HI( ADC_SumsReady, l_achannel ) )
    {
        // Compute average
        l_number = ADC_SamplesNumbersTable[ l_achannel ];
        l_sum    = ADC_SamplesSums[ l_achannel ];
        ADC_PreviousAverages[ l_achannel ] = ADC_CurrentAverages[ l_achannel ];
        if(l_number != 0)
        ADC_CurrentAverages [ l_achannel ] = ( ( l_sum + (l_number >> 1U) ) / l_number );
        // Clear sum-ready flag and signal average is ready to outside functions
        CLR( ADC_SumsReady    , l_achannel );
        SET( ADC_AveragesReady, l_achannel );
    }
    else
    {
        // Nothing !
    }

    // Move to next channel to average (ONLY one average per main loop !)
//    if( l_achannel < (ADC_BUFFER_SIZE - 1U) )
//    {
//        l_achannel++;
//    }
//    else
//    {
//        l_achannel = 0U;
//    }
    ADC_AverageChannel = l_achannel;      // Store/Save index of channel to average
}
/**************************************************************************
 * Name       : ADC_IsAverageReady
 * Parameters : channel = index of channel for which the average status
 *            : is retrieved.
 * Return     : Status of the specified channel average:
 *            : -TRUE  if average is ready
 *            : -FALSE if average is not ready
 * Description: Retrives the status of the average for the specified channel
 *            : and clears the channel average-ready flag.
 ***************************************************************************/
uint8_t ADC_IsAverageReady( uint8_t channel )
{
    uint8_t l_ready;

    l_ready = HI( ADC_AveragesReady, channel );
    CLR( ADC_AveragesReady, channel );

    return l_ready;
}
