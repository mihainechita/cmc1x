/*******************************************************************************
* Description : Fisier header pentru 
* File name   : crc.h
* Author      : Ing. Mihai Nechita   
* Date	      : 22.11.2019 
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef _CRC_H_
#define _CRC_H_

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
// Initial value for CRCs
#define CRC_INIT_VAL    (unsigned int)0xFFFF


/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
extern __flash const unsigned int CRC_Table[ 256U ]; 


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions
 *----------------------------------------------------------------------------*/
extern unsigned int CRC_AddByteToCRC( unsigned int CRC, unsigned char byte );
extern unsigned int CRC_ComputeAreaCRC( unsigned char *startAddr, unsigned char nrBytes );

#endif
 