/*******************************************************************************
* File name   : isr.h
* Author      : Ing. Mihai Nechita 
* Description : Header for Interrupt Service Routine
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __ISR_h
#define __ISR_h

#include "types.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
#if( SYS_LCDMODEL == RG24064 )       //daca LCD este RG24064
//Define-uri pentru RG24064A-BIW-V

#define	LCD_PORT	PORTA

#define	LCD_PIN_CE      C_LCD_CE
#define	LCD_PIN_RES	C_LCD_RES
#define	LCD_PIN_CD	C_LCD_CD
#define	LCD_PIN_WR	C_LCD_WR
#define LCD_PIN_RD      C_LCD_RD
     
// Macro-uri folosite la inversarea nivelurilor logice ale semnalelor de LCD
#define LCD_CLR_RES		SET ( LCD_PORT, LCD_PIN_RES );  	 
#define LCD_CLR_RD		SET ( LCD_PORT, LCD_PIN_RD );	 
#define LCD_CLR_WR		SET ( LCD_PORT, LCD_PIN_WR );	 
#define LCD_CLR_CD		SET ( LCD_PORT, LCD_PIN_CD );	 	
#define LCD_CLR_CE       	SET ( LCD_PORT, LCD_PIN_CE );     
     
#define LCD_SET_RES		CLR ( LCD_PORT, LCD_PIN_RES );  	 
#define LCD_SET_RD		CLR ( LCD_PORT, LCD_PIN_RD );	 
#define LCD_SET_WR		CLR ( LCD_PORT, LCD_PIN_WR );	 
#define LCD_SET_CD		CLR ( LCD_PORT, LCD_PIN_CD );	 	
#define LCD_SET_CE       	CLR ( LCD_PORT, LCD_PIN_CE );  

// Comenzi LCD - consulta datasheet RAIO RA6963 pentru detalii
// Register Setting
#define LCD_SETCURSOR_POINTER           0x21
#define LCD_SET_OFFSET_REG              0x22
#define LCD_SET_ADDR_POINTER            0x24

// Set Control Word
#define LCD_SET_TEXT_HOME_ADDR          0x40
#define LCD_SET_TEXT_AREA               0x41
#define LCD_SET_GRAPHIC_HOME_ADDR       0x42
#define LCD_SET_GRAPHIC_AREA            0x43

// Mode Set
#define LCD_OR_MODE                     0x80
#define LCD_EXOR_MODE                   0x81
#define LCD_AND_MODE                    0x83
#define LCD_TEXT_ATTR_MODE              0x84
#define LCD_INTERN_CGROM                0x87
#define LCD_EXTERN_CGRAM                0x8F

// Display Mode
#define LCD_DISPLAY_OFF                 0x90
#define LCD_CURSOR_ON_BLINK_OFF         0x92
#define LCD_CURSOR_ON_BLINK_ON          0x93
#define LCD_TEXT_ON_GRAPHIC_OFF         0x94
#define LCD_TEXT_OFF_GRAPHIC_ON         0x98
#define LCD_TEXT_ON_GRAPHIC_ON          0x9C

// Cursor Pattern Select
#define LCD_CURSOR_1LINE                0xA0
#define LCD_CURSOR_2LINE                0xA1
#define LCD_CURSOR_3LINE                0xA2
#define LCD_CURSOR_4LINE                0xA3
#define LCD_CURSOR_5LINE                0xA4
#define LCD_CURSOR_6LINE                0xA5
#define LCD_CURSOR_7LINE                0xA6
#define LCD_CURSOR_8LINE                0xA7

// Data Read/Write
#define LCD_DATA_WRITE_INC_ADP          0xC0
#define LCD_DATA_READ_INC_ADP           0xC1
#define LCD_DATA_WRITE_DEC_ADP          0xC2
#define LCD_DATA_READ_DEC_ADP           0xC3
#define LCD_DATA_WRITE_NONVARIABLE_ADP  0xC4
#define LCD_DATA_READ_NONVARIABLE_ADP   0xC5

// Data auto Read/Write
#define LCD_DATA_AUTO_WRITE             0xB0
#define LCD_DATA_AUTO_READ              0xB1
#define LCD_DATA_AUTO_RESET             0xB2

// Screen Peek
#define LCD_SCREEN_PEEK                 0xE0

// Screen Copy
#define LCD_SCREEN_COPY                 0xE8

// Bit Set/Reset
#define LCD_BIT_RESET                   0xF7
#define LCD_BIT_SET                     0xFF

#define LCD_BIT0                        0xF0
#define LCD_BIT1                        0xF1
#define LCD_BIT2                        0xF2
#define LCD_BIT3                        0xF3
#define LCD_BIT4                        0xF4
#define LCD_BIT5                        0xF5
#define LCD_BIT6                        0xF6
#define LCD_BIT7                        0xF7

// Screen Reverse
#define LCD_SCREEN_REVERSE              0xD0

// Blink Time
#define LCD_BLINK_TIME                  0x50

// Cursor Auto Moving           
#define LCD_AUTO_MOVING                 0x60

// CGROM Font Select
#define LCD_FONT_SELECT                 0x70

#endif

/* Define-uri hardware LCD */       
#define LCD_LINES_NR                    (8U)
#define LCD_COLUMNS_NR                  (40U)       
     
/* Dimensiunea memoriei pentru LCD */  
#define LCD_API_CHAR_MAXNR              (320U)

/* Numar de segmente memorie */  
#define LCD_API_SEG_NUM                 (40)

/* Lungime segment */  
#define LCD_API_SEG_SIZE                ( LCD_API_CHAR_MAXNR / ( (unsigned int )LCD_API_SEG_NUM ) )

/* Numar de caractere ramase la diferenta dintre numarul maxim de caractere
   si produsul numarului de segmente cu dimensiunea segmentului */      
#define LCD_API_DIFF_CHAR                ( LCD_API_CHAR_MAXNR - ( LCD_API_SEG_NUM * LCD_API_SEG_SIZE )   ) 

/* Numar de segmente adaugate la lungimea finala a vectorului de offset 
 in functie de numarul de caractere ramase si dimensiunea unui segment */      
#define LCD_API_VOFF_DIFF_LENGTH        (  LCD_API_DIFF_CHAR / LCD_API_SEG_SIZE )
 
/*  */  
#define LCD_API_DIFF_SEG_CHAR           ( LCD_API_CHAR_MAXNR - ( (  LCD_API_VOFF_DIFF_LENGTH + LCD_API_SEG_NUM ) *  LCD_API_SEG_SIZE ) )
 
/*  */  
#define LCD_API_DIFF_SEG                ( ( LCD_API_DIFF_SEG_CHAR * LCD_API_SEG_SIZE ) / LCD_API_SEG_SIZE )

/* Lungime vector de offset */     
#define LCD_VOFF_IDX_LENGTH             ( LCD_API_SEG_NUM + LCD_API_VOFF_DIFF_LENGTH + LCD_API_DIFF_SEG + 1 )      

//#if ( LCD_API_SEG_NUM >50 )               
//#error "LCD_API_SEG_NUM prea mare; maxim 32 !!!!"
//#endif

///* Numar de treceri prin ISR pana la schimbarea offsetului */       
//#define ISR_TIMER0_PASSES	        (LCD_API_CHARNR_TO_SEND/ISR_CHARNR_TO_SEND)  

///* Numar de caractere trimise la o trecere prin intrerupere */       
//#define ISR_CHARNR_TO_SEND              (LCD_API_SEG_SIZE)
     
#define MENU_READY              0x1000          
#define MENU_BUSY               0xEFFF
#define MENU_READY_BIT          (12)     


#define INC_TO_2_PWR( A, B ) A = ( (A)+1 ) & ( (1<<(B)) - 1 )
#define DEC_TO_2_PWR( A, B ) A = ( (A)-1 ) & ( (1<<(B)) - 1 )
         
//#define ISR_SENT                0x2000            
//#define ISR_NOT_SENT            0xDFFF
//#define ISR_SENT_BIT            (13)

/* Macro pentru trimitere comanda de setare address pointer*/	
#define LCD_API_SETPOS_CMD \
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
STROBE_LCD;\
LCD_CONTROL_SET_COMMAND;\
LCD_CONTROL_CE_ENABLE;\
LCD_CONTROL_WR_ENABLE;\
asm("nop");\
LCD_CONTROL_WR_DISABLE;\
LCD_CONTROL_CE_DISABLE;
     
/* Macro pentru activare autoscriere (trimitere doar date, fara 0xC0 dupa)*/	
#define LCD_API_ACTIVATE_AUTOWRITE \
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
STROBE_LCD;\
LCD_CONTROL_SET_COMMAND;\
LCD_CONTROL_CE_ENABLE;\
LCD_CONTROL_WR_ENABLE;\
asm("nop");\
LCD_CONTROL_WR_DISABLE;\
LCD_CONTROL_CE_DISABLE;

/* Macro pentru dezactivare autoscriere */
#define LCD_API_DEACTIVATE_AUTOWRITE \
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
PORTB=0x02;\
RISING_EDGE_CLOCK;\
PORTB=0x00;\
RISING_EDGE_CLOCK;\
STROBE_LCD;\
LCD_CONTROL_SET_COMMAND;\
LCD_CONTROL_CE_ENABLE;\
LCD_CONTROL_WR_ENABLE;\
asm("nop");\
LCD_CONTROL_WR_DISABLE;\
LCD_CONTROL_CE_DISABLE;

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
*----------------------------------------------------------------------------*/
extern volatile unsigned char ISR_TicksCounterNSM;                              // Counter with tick @200us Not-Synchronized with Mains: for  SW-timers

extern unsigned char SYS_LCD_Data;

extern unsigned char LCD_API_ReadyToChangePage;

extern unsigned int SYS_VOFF_IDX_Length;

extern volatile unsigned int LCD_API_Segment_Select;

extern unsigned char LCD_API_DATA[ LCD_API_CHAR_MAXNR + LCD_API_SEG_SIZE + LCD_API_SEG_SIZE];

extern unsigned int VOFFSET[];

extern unsigned char ISR_LCD_API_ChangeOffset;

extern unsigned char SYS_FBK_Spark;
//extern unsigned int SYS_ISR_LCD_VOFF_Idx;

extern volatile uint16_t ISR_INT0_Refresh;


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
void LCD_API_Update(void);
void LCD_API_VOFFSET_Init( void );
 
#endif //_ISR_H_
 


















