/*******************************************************************************
* Description : Fisier header care include toate celelalte fisiere header din proiect
* File name   : headers.h
* Author      : Ing. Mihai Nechita            
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/



#ifndef __HEADERS_H
#define __HEADERS_H

#include <ioavr.h>
#include <inavr.h>

#include "aaa_sys_config.h"
#include "types.h"
#include "swtimers.h"
#include "system.h"
#include "isr.h"
#include "stdmacros.h"
#include "adc.h"
#include "twi.h"
#include "rtc.h"
#include "temp.h"
#include "userintf.h"
#include "menu.h"
#include "keys.h"
#include "enables.h"
#include "io_drv.h"
#include "lcdapi.h"
#include "Comm595.h"
#include "usart.h"
#include "pc_prot.h"
#include "crc.h"
#include "sensors_prot.h"

/* __HEADERS_H */
#endif