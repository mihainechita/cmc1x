/*******************************************************************************
* Description : Fisier header pentru citire si scriere registre de shiftare
* File name   : io_drv.h
* Author      : Ing. Mihai Nechita   
* Date	      : 05.12.2018           
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __io_drv_h
#define __io_drv_h

#include "types.h"
#include "lcdapi.h"
/*******************************************************************************
*
* Data structures
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public defines
*----------------------------------------------------------------------------*/
#define PSINPUT          C_LCD_RD
#define PSINPUT_PORT     PORTA
#define TAST_IN_PORT     PINC
#define EN_IN_PORT       PINC


// Iesiri shift register U2_2
#define CMD_TS                  7          // 0b 1000 0000 
#define CMD_FM                  6         // 0b 0100 0000 
#define CMD_AUX2                5          // 0b 0010 0000 
#define CMD_AUX1                4          // 0b 0001 0000 
#define CMD_AUX0                3          // 0b 0000 1000 
#define CMD_PH                  2          // 0b 0000 0100 
#define CMD_PA                  1          // 0b 0000 0010 
#define CMD_TL                  0          // 0b 0000 0001 

// Iesiri shift register U1_2
#define FS_LCD                  7          // 0b 1000 0000 
#define FLACARA_ONOFF           6          // 0b 0100 0000 
#define AUXILIAR_ONOFF          5          // 0b 0010 0000 
#define VANAGAZ_ONOFF           4          // 0b 0001 0000  
#define V3C_ONOFF               3          // 0b 0000 1000  
#define VENT_ONOFF              2          // 0b 0000 0100 
#define PMPBOILER_ONOFF         1          // 0b 0000 0010 
#define PMPCENTRALA_ONOFF       0          // 0b 0000 0001 

#define RISING_EDGE_CLOCK               { SET(PORTA,C_SCLK); asm ("nop"); CLR(PORTA,C_SCLK); }
//#define FALLING_EDGE_CLOCK              { CLR(PORTA,C_SCLK); SET(PORTA,C_SCLK); }
// liniile care contin C_ in fata numelui trebuiesc inversate ca nivel logic deoarece sunt trecute prin registrul inversor trigger Schmitt
#define SHIFT_CLOCK    			{ CLR(PORTA,C_SCLK);SET(PORTA,C_SCLK); }        // tranzitie HIGH->LOW a liniei C_SCLK, care va deveni 
                                                                                        // tranzitie LOW->HIGH dupa ce linia trece prin registrul inversor trigger Schmitt
// Atentie la logica de nivel:  CLR(PSINPUT_PORT,PSINPUT) duce linia PSINPUT in HIGH (5V) !!!!
//                              SET(PSINPUT_PORT,PSINPUT) duce linia PSINPUT in LOW ( 0V) !!!!
#define SET_PARALLEL_MODE_MC14014       CLR(PSINPUT_PORT,PSINPUT);              // atentie la logica de nivel ( inversata )
#define SET_SERIAL_MODE_MC14014         SET(PSINPUT_PORT,PSINPUT);              // atentie la logica de nivel ( inversata )

#define STROBE_CMD 			{ SET(PORTB,REG_CMD); CLR(PORTB,REG_CMD); }





/* Define-uri pentru iesirile de pe registrul SIPO U1_2 */
/*---------------------------------------------------------------------------*/
/* Q7 */
#define CMD_PH_ON        SET(SYS_CMD_Relay_High,CMD_PA)
#define CMD_PH_OFF       CLR(SYS_CMD_Relay_High,CMD_PA)
#define CMD_PH_XOR       XOR(SYS_CMD_Relay_High,CMD_PA)
/* Q6 */
#define CMD_FM_ON        SET(SYS_CMD_Relay_High,CMD_FM)
#define CMD_FM_OFF       CLR(SYS_CMD_Relay_High,CMD_FM)
#define CMD_FM_XOR       XOR(SYS_CMD_Relay_High,CMD_FM)
/* Q5 */
#define CMD_AUX2_ON      SET(SYS_CMD_Relay_High,CMD_AUX2)
#define CMD_AUX2_OFF     CLR(SYS_CMD_Relay_High,CMD_AUX2)
#define CMD_AUX2_XOR     XOR(SYS_CMD_Relay_High,CMD_AUX2)
/* Q4 */
#define CMD_AUX1_ON      SET(SYS_CMD_Relay_High,CMD_AUX1)
#define CMD_AUX1_OFF     CLR(SYS_CMD_Relay_High,CMD_AUX1)
#define CMD_AUX1_XOR     XOR(SYS_CMD_Relay_High,CMD_AUX1)
/* Q3 */
#define CMD_AUX0_ON      SET(SYS_CMD_Relay_High,CMD_AUX0)
#define CMD_AUX0_OFF     CLR(SYS_CMD_Relay_High,CMD_AUX0)
#define CMD_AUX0_XOR     XOR(SYS_CMD_Relay_High,CMD_AUX0)
/* Q2 */
#define CMD_TS_ON        SET(SYS_CMD_Relay_High,CMD_PH)
#define CMD_TS_OFF       CLR(SYS_CMD_Relay_High,CMD_PH)
#define CMD_TS_XOR       XOR(SYS_CMD_Relay_High,CMD_PH)
/* Q1 */
#define CMD_PA_ON        SET(SYS_CMD_Relay_High,CMD_TS)
#define CMD_PA_OFF       CLR(SYS_CMD_Relay_High,CMD_TS)
#define CMD_PA_XOR       XOR(SYS_CMD_Relay_High,CMD_TS)
/* Q0 */
#define CMD_TL_ON        SET(SYS_CMD_Relay_High,CMD_TL)
#define CMD_TL_OFF       CLR(SYS_CMD_Relay_High,CMD_TL)
#define CMD_TL_XOR       XOR(SYS_CMD_Relay_High,CMD_TL)
/*---------------------------------------------------------------------------*/




/* Define-uri pentru iesirile de pe registrul SIPO U2_2 */
/*---------------------------------------------------------------------------*/
/* Q7 */
#define CMD_FS_LCD_ON    SET(SYS_CMD_Relay_Low,CMD_TS)
#define CMD_FS_LCD_OFF   CLR(SYS_CMD_Relay_Low,CMD_TS)
#define CMD_FS_LCD_XOR   XOR(SYS_CMD_Relay_Low,CMD_TS)
/* Q6 */
#define FLACARA_ON       CLR(SYS_CMD_Relay_Low,FLACARA_ONOFF)
#define FLACARA_OFF      SET(SYS_CMD_Relay_Low,FLACARA_ONOFF)
#define FLACARA_XOR      XOR(SYS_CMD_Relay_Low,FLACARA_ONOFF)
/* Q5 */
#define VANAGAZ_ON       SET(SYS_CMD_Relay_Low,VANAGAZ_ONOFF)
#define VANAGAZ_OFF      CLR(SYS_CMD_Relay_Low,VANAGAZ_ONOFF)
#define VANAGAZ_XOR      XOR(SYS_CMD_Relay_Low,VANAGAZ_ONOFF)
/* Q4 */
#define AUXILIAR_ON      SET(SYS_CMD_Relay_Low,AUXILIAR_ONOFF)
#define AUXILIAR_OFF     CLR(SYS_CMD_Relay_Low,AUXILIAR_ONOFF)
#define AUXILIAR_XOR     XOR(SYS_CMD_Relay_Low,AUXILIAR_ONOFF)
/* Q3 */
#define VANA3C_ON        SET(SYS_CMD_Relay_Low,V3C_ONOFF)
#define VANA3C_OFF       CLR(SYS_CMD_Relay_Low,V3C_ONOFF)
#define VANA3C_XOR       XOR(SYS_CMD_Relay_Low,V3C_ONOFF)
/* Q2 */
#define VENT_ON          SET(SYS_CMD_Relay_Low,VENT_ONOFF)
#define VENT_OFF         CLR(SYS_CMD_Relay_Low,VENT_ONOFF)
#define VENT_XOR         XOR(SYS_CMD_Relay_Low,VENT_ONOFF)    
/* Q1 */
#define PMPBOILER_ON     SET(SYS_CMD_Relay_Low,PMPBOILER_ONOFF)
#define PMPBOILER_OFF    CLR(SYS_CMD_Relay_Low,PMPBOILER_ONOFF)
#define PMPBOILER_XOR    XOR(SYS_CMD_Relay_Low,PMPBOILER_ONOFF)
/* Q0 */
#define PMPCENTRALA_ON   SET(SYS_CMD_Relay_Low,PMPCENTRALA_ONOFF)
#define PMPCENTRALA_OFF  CLR(SYS_CMD_Relay_Low,PMPCENTRALA_ONOFF)
#define PMPCENTRALA_XOR  XOR(SYS_CMD_Relay_Low,PMPCENTRALA_ONOFF)
/*---------------------------------------------------------------------------*/

#define EXT_RES_IO_ON    SET(PORTA_Buffer,EXT_RES_IO);
#define EXT_RES_IO_OFF   CLR(PORTA_Buffer,EXT_RES_IO);
#define EXT_RES_IO_XOR   XOR(PORTA_Buffer,EXT_RES_IO);

#define CENTRALA_ON      CLR(PORTD_Buffer,CENTRALA_ONOFF);
#define CENTRALA_OFF     SET(PORTD_Buffer,CENTRALA_ONOFF);
#define CENTRALA_XOR     XOR(PORTD_Buffer,CENTRALA_ONOFF);

#define BKL_ON                  SET(PORTD_Buffer,BKL_ONOFF)
#define BKL_OFF                 CLR(PORTD_Buffer,BKL_ONOFF)

#define BUZZ_ON                 SET(PORTD,BUZZER_ONOFF)
#define BUZZ_OFF                CLR(PORTD,BUZZER_ONOFF)
#define BUZZ_XOR                XOR(PORTD,BUZZER_ONOFF)

/* Redefinire pentru utilizare nativa (termostatul de ambient este simulat cu ajutorul iesirii AUX0 */
#define CMD_TA_ON     	        CMD_AUX0_ON
#define CMD_TA_OFF    	        CMD_AUX0_OFF 
#define CMD_TA_XOR    	        CMD_AUX0_XOR 

/* Redefinire pentru utilizare nativa (modulul AUX este folosit pentru citirea comenzii de ignitie) */
#define IGNITIE_ON	        AUXILIAR_ON
#define IGNITIE_OFF	        AUXILIAR_OFF
#define IGNITIE_XOR	        AUXILIAR_XOR    

#define CMD_E13_ON              CMD_FM_ON      
#define CMD_E13_OFF             CMD_FM_OFF     
#define CMD_E13_XOR             CMD_FM_XOR   

#define CMD_E10_ON              CMD_AUX2_ON      
#define CMD_E10_OFF             CMD_AUX2_OFF     
#define CMD_E10_XOR             CMD_AUX2_XOR   

#define CMD_E11_SENSOR_ON       CMD_TL_ON       
#define CMD_E11_SENSOR_OFF      CMD_TL_OFF       
#define CMD_E11_SENSOR_XOR      CMD_TL_XOR       


#define CMD_E40_ON              CMD_AUX1_ON      
#define CMD_E40_OFF             CMD_AUX1_OFF     
#define CMD_E40_XOR             CMD_AUX1_XOR     

#define CMD_BUZZ_ON             CMD_PH_ON       
#define CMD_BUZZ_OFF            CMD_PH_OFF      
#define CMD_BUZZ_XOR            CMD_PH_XOR    



/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
* Public (exported) variables
*----------------------------------------------------------------------------*/

extern volatile uint8_t SYS_CMD_Relay_High;                  // Variabila principala cu care se activeaza/dezactiveaza releele
extern volatile uint8_t SYS_CMD_Relay_Low;                  // Variabila principala cu care se activeaza/dezactiveaza releele

extern volatile uint8_t PORTA_Buffer;
extern volatile uint8_t PORTB_Buffer;
extern volatile uint8_t PORTC_Buffer;
extern volatile uint8_t PORTD_Buffer;

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/
void IO_DRV_Backlight_Toggle( void );
void IO_DRV_Update(void);

/* __io_drv_h */
#endif 














