/*******************************************************************************
* Description : Fisier sursa pentru meniu
* File name   : menu.c
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>      
#include <inavr.h>      
#include <string.h>     
/* System */
#include "types.h"
#include "lcdapi.h"
#include "system.h"
#include "swtimers.h"
#include "temp.h"
#include "io_drv.h"
#include "menu.h"
#include "isr.h"
#include "keys.h"
#include "stdmacros.h"
#include "enables.h"
#include "twi.h"
#include "rtc.h"
#include "usart.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Private variables
*----------------------------------------------------------------------------*/
/* MENU_Index este indexul principal in care selecteaza ce functie de incarcare
meniu se apeleaza; acesta este modificat de functia MENU_KEYS_Process*/
uint8_t MENU_Index=0;
/* Buffer auxiliar in care se incarca continut din RAM sau Flash, folosit pentru
a incarca continutul de pe o singura linie a LCD-ului*/
uint8_t MENU_LINE_Buffer[LCD_COLUMNS_NR];
/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
/* Semafor principal de comunicatie intre MENU_Process, functie ce incarca 
 continut in LCD_API_DATA, si ISR_TIMER0_OVF_vect, intreruperea de Timer0, care
 trimite date catre LCD in LCD_API_DATA */
unsigned long int SYS_LCD_UpdateEnabled=0;



/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
/* MENU_Cursor_pos contine pozitia cursorului ">" din MENU_HOME*/
uint8_t MENU_Cursor_pos=0;

/* uint8_t MENU_Normal_Cursor=0 */
/* MENU_Cursor_pos contine pozitia cursorului ">" din meniul de selectie centrala*/
uint8_t MENU_Boiler_Cursor=0;

/* MENU_Cursor_pos contine pozitia cursorului (parametrul care palpaie) din 
meniul de setare data/ora*/
uint8_t MENU_Setup_Time_Cursor=0;

/* Variabila folosita de automatul de blink pentru a face toggle intre afisarea
textului sau a spatiilor libere*/
uint8_t MENU_Blink_State=0;

/* Vector unde este incarcat textul ce reprezinta temperatura masurata pe senzorul
de pe placa de baza a standului */
uint8_t temperature[3];

/* Buffere folosite pentru a stoca textul de afisat, ce reprezinta data/ora */ 
uint8_t TEMP_seconds[3], TEMP_minutes[3], TEMP_hours[3], TEMP_days[3], TEMP_wk_days[3], TEMP_months[3], TEMP_years[3];
uint8_t SYS_seconds[3], SYS_minutes[3], SYS_hours[3], SYS_days[3], SYS_wk_days[3], SYS_months[3], SYS_years[3];

/* Buffere folosite pentru a stoca textul de afisat, ce reprezinta numarul de teste finalizate in cadrul meniului de anduranta*/
uint8_t MENU_EnduranceTestsNr_Buffer[3];
//--------------------------------------------"                                        "



#if(STAND_MODEL==STAND_FORTATE)
__flash uint8_t MENU_MAIN_STATIC_00[] = "STAND CMC1X CT TIRAJ FORTAT             ";
__flash uint8_t MENU_MAIN_STATIC_02[] = "0.                                      ";    
#else
__flash uint8_t MENU_MAIN_STATIC_00[] = "STAND CMC1X CT CONDENSATIE              ";
__flash uint8_t MENU_MAIN_STATIC_02[] = "0.                                      ";    
#endif
__flash uint8_t MENU_MAIN_STATIC_03[] = "1. Test manual                          ";  
__flash uint8_t MENU_MAIN_STATIC_04[] = "2. Test automat                         ";   
__flash uint8_t MENU_MAIN_STATIC_06[] = "3. Anduranta                            ";  
__flash uint8_t MENU_MAIN_STATIC_07[] = "4. Setari                               ";
__flash uint8_t MENU_BOILER_40SPACE[] = "                                        ";
__flash uint8_t       MENU_MAIN_TBD[] = "To be defined...                        ";

__flash uint8_t MENU_TITLE_00[]       = "Selectie tip centrala                   ";
__flash uint8_t MENU_TITLE_01[]       = "Test manual                             ";
__flash uint8_t MENU_TITLE_02[]       = "Test automat                            ";
__flash uint8_t MENU_TITLE_04[] 	    = "Anduranta                               "; 
__flash uint8_t MENU_TITLE_05[] 	    = "Setari                                  ";

/*---------------------------------------"                                        "*/
__flash uint8_t MENU_STATIC_01[] = "Vana Gaz                                ";
__flash uint8_t MENU_STATIC_02[] = "Pompa                                   ";
__flash uint8_t MENU_STATIC_03[] = "Vana cu 3 cai                           ";
__flash uint8_t MENU_STATIC_04[] = "Stare CT                                ";
__flash uint8_t MENU_STATIC_05[] = "Urmariti aparitia erorii                ";
__flash uint8_t MENU_STATIC_06[] = "Data/ora 20  /  /     :  :              ";
__flash uint8_t MENU_STATIC_07[] = "Urmariti comutarea vanei cu 3 cai       ";
__flash uint8_t MENU_STATIC_08[] = "Problema modul termostat ambient !!!!   ";
__flash uint8_t MENU_STATIC_09[] = "Problema modul ionizare !!!!            ";
__flash uint8_t MENU_STATIC_10[] = "Alimentare CT      Cerere CH            ";
__flash uint8_t MENU_STATIC_11[] = "Ventilator         Cerere DHW           ";
__flash uint8_t MENU_STATIC_16[] = "Vana Gaz           Teste finalizate     ";
#if(STAND_MODEL==STAND_FORTATE)
__flash uint8_t MENU_STATIC_12[] = "Tiraj natural                           ";
__flash uint8_t MENU_STATIC_13[] = "Tiraj fortat turatie fixa               ";
__flash uint8_t MENU_STATIC_14[] = "Tiraj fortat turatie variabila          ";
__flash uint8_t MENU_STATIC_15[] = "Semicondensatie                         ";
__flash uint8_t MENU_STATIC_17[] = "Fixa                                    ";
__flash uint8_t MENU_STATIC_18[] = "Variabila                               ";
#else
__flash uint8_t MENU_STATIC_12[] = "C38-25/29/35/BA                         ";
__flash uint8_t MENU_STATIC_13[] = "C38-CH1/CH2                             ";
#endif

__flash uint8_t MENU_CURSOR[] = ">";
__flash uint8_t MENU_1SPACE[] = " ";
__flash uint8_t MENU_2SPACE[] = "  ";
__flash uint8_t CELSIUS[]     = " C";

uint8_t *MENU_EES_STATUS[2]={"OFF","ON  "};
uint8_t *MENU_BOILER_STATUS[9]={"ASTEPTARE     ","PREVENTILARE  ","IGNITIE       ","ARDERE        ","EROARE        ","EROARE        ","RESTART       ","EROARE        ","EROARE        "};

#if(STAND_MODEL==STAND_FORTATE)
uint8_t *MENU_ERRORS_STATUS_TEST[TESTS_NR-1]={"E10","E11","E20","E26","E26","E40","E52"};
#else
uint8_t *MENU_ERRORS_STATUS_TEST[TESTS_NR-1]={"E10","E11","E20","E26","E40","E51","DHW"};
#endif

uint8_t *MENU_V3C_STATUS[2]={"DHW","CH "};
uint8_t *MENU_WEEK_DAYS[7]={"Duminica","Luni    ","Marti   ","Miercuri","Joi     ","Vineri  ","Sambata "};
/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/
/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/*******************************************************************************
* Functie ce transforma un numar pe 16 biti intr-un sir de maxim 5 caractere 
* ce poate fi afisat pe LCD
*
******************************************************************************/
void NRtoString(uint16_t number, uint8_t *text)
{
	text[2]=0x00;
        text[1]=(number%10)+0x30;
        text[0]=(number/10)+0x30;
}

/*******************************************************************************
* Functie ce incarca  LCD_API_DATA cu continut din flash la offsetul 
* corespunzator pozitionarii fizice pe LCD 
******************************************************************************/
void MENU_Load_Flash(__flash uint8_t v[], uint16_t LCD_Line, uint16_t LCD_Column, uint8_t length)
{
    uint8_t i=0;
    uint16_t l_VOFF_Index_Start=0;
    uint16_t l_VOFF_Index_End=0;
    
    uint16_t l_Index_Off;
    uint16_t l_Index_Seg;
    unsigned long l_LCD_Off;
    
    uint16_t l_VOFF_Index_Start_Off;
    uint16_t l_VOFF_Index_End_Off;
    
    /* Calcul offset fata de pozitia 0 a memoriei LCD_API_DATA */ 
    l_LCD_Off = ( ( (unsigned long)(LCD_Line * LCD_COLUMNS_NR) + ((unsigned long)LCD_Column) ) );
    /* Calcul index segment de start */
    l_VOFF_Index_Start = (l_LCD_Off / LCD_API_SEG_SIZE ) ;
    /* Calcul offset segment in cadrul segmentului de start */
    l_VOFF_Index_Start_Off = (uint16_t)( l_LCD_Off - (l_VOFF_Index_Start * LCD_API_SEG_SIZE));
    
    /* Calcul offset fata de pozitia 0 a memoriei LCD_API_DATA */ 
    l_LCD_Off = ( ( (unsigned long)(LCD_Line * LCD_COLUMNS_NR) ) ) + ((unsigned long)LCD_Column) + ((unsigned long)length);
    /* Calcul index segment de stop */
    l_VOFF_Index_End = (l_LCD_Off / LCD_API_SEG_SIZE ) ;
    /* Calcul offset segment in cadrul segmentului de stop */
    l_VOFF_Index_End_Off = (uint16_t)( l_LCD_Off - (l_VOFF_Index_End * LCD_API_SEG_SIZE));
    
    /* Daca scrierea se face pe acelasi segment.. */
    if( l_VOFF_Index_Start == l_VOFF_Index_End )
    {
        /* Parcurgere de la offsetul de start in cadrul segmentului pana la  */
        /* offsetul de stop din cadrul aceluiasi offset  */
        for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
        {
            LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
            i = i + 1;
        }
    }
    else
    {
        /* Daca scrierea se face pe 2 segmente consecutive.. */
        if( l_VOFF_Index_Start == (l_VOFF_Index_End - 1) )
        {
            /* Parcurgere de la offsetul in cadrul segmentului de start pana la  */
            /* offsetul din cadrul segmentului de stop  */
            for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
            
            for(l_Index_Seg = 0 ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_End  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
        }
        else
        {
            /* Daca scrierea se face pe mai mult de 2 segmente.. */
            /* Parcurgere de la offset in cadrul segmentului de start*/
            for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
            /* Parcurgere segmente cu scriere completa segment */
            for( l_Index_Off = (l_VOFF_Index_Start + 1); l_Index_Off <= (l_VOFF_Index_End - 1) ; l_Index_Off++ )
            {
                for(l_Index_Seg = 0 ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
                {
                    LCD_API_DATA[ VOFFSET [ l_Index_Off  ] + l_Index_Seg ] = v[ i ];
                    i = i + 1;
                }
            }
            /* Parcurgere pana la offsetul in cadrul segmentului de stop */
            for(l_Index_Seg = 0 ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_End  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
        }
    }
}


/*******************************************************************************
* Functie ce incarca  LCD_API_DATA cu continut din RAM la offsetul 
* corespunzator pozitionarii fizice pe LCD 
******************************************************************************/
void MENU_Load_RAM( uint8_t v[], uint16_t LCD_Line, uint16_t LCD_Column,  uint8_t length)
{
    uint8_t i=0;
    uint16_t l_VOFF_Index_Start=0;
    uint16_t l_VOFF_Index_End=0;
    
    uint16_t l_Index_Off;
    uint16_t l_Index_Seg;
    unsigned long l_LCD_Off;
    
    uint16_t l_VOFF_Index_Start_Off;
    uint16_t l_VOFF_Index_End_Off;
    
    /* Calcul offset fata de pozitia 0 a memoriei LCD_API_DATA */ 
    l_LCD_Off = ( ( (unsigned long)(LCD_Line * LCD_COLUMNS_NR) + ((unsigned long)LCD_Column) ) );
    /* Calcul index segment de start */
    l_VOFF_Index_Start = (l_LCD_Off / LCD_API_SEG_SIZE ) ;
    /* Calcul offset segment in cadrul segmentului de start */
    l_VOFF_Index_Start_Off = (uint16_t)( l_LCD_Off - (l_VOFF_Index_Start * LCD_API_SEG_SIZE));
    
    /* Calcul offset fata de pozitia 0 a memoriei LCD_API_DATA */ 
    l_LCD_Off = ( ( (unsigned long)(LCD_Line * LCD_COLUMNS_NR) ) ) + ((unsigned long)LCD_Column) + ((unsigned long)length);
    /* Calcul index segment de stop */
    l_VOFF_Index_End = (l_LCD_Off / LCD_API_SEG_SIZE ) ;
    /* Calcul offset segment in cadrul segmentului de stop */
    l_VOFF_Index_End_Off = (uint16_t)( l_LCD_Off - (l_VOFF_Index_End * LCD_API_SEG_SIZE));
    
    /* Daca scrierea se face pe acelasi segment.. */
    if( l_VOFF_Index_Start == l_VOFF_Index_End )
    {
        /* Parcurgere de la offsetul de start in cadrul segmentului pana la  */
        /* offsetul de stop din cadrul aceluiasi offset  */
        for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
        {
            LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
            i = i + 1;
        }
    }
    else
    {
        /* Daca scrierea se face pe 2 segmente consecutive.. */
        if( l_VOFF_Index_Start == (l_VOFF_Index_End - 1) )
        {
            /* Parcurgere de la offsetul in cadrul segmentului de start pana la  */
            /* offsetul din cadrul segmentului de stop  */
            for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
            
            for(l_Index_Seg = 0 ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_End  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
        }
        else
        {
            /* Daca scrierea se face pe mai mult de 2 segmente.. */
            /* Parcurgere de la offset in cadrul segmentului de start*/
            for(l_Index_Seg = l_VOFF_Index_Start_Off ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_Start  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
            /* Parcurgere segmente cu scriere completa segment */
            for( l_Index_Off = (l_VOFF_Index_Start + 1); l_Index_Off <= (l_VOFF_Index_End - 1) ; l_Index_Off++ )
            {
                for(l_Index_Seg = 0 ; l_Index_Seg < LCD_API_SEG_SIZE  ; l_Index_Seg++ )
                {
                    LCD_API_DATA[ VOFFSET [ l_Index_Off  ] + l_Index_Seg ] = v[ i ];
                    i = i + 1;
                }
            }
            /* Parcurgere pana la offsetul in cadrul segmentului de stop */
            for(l_Index_Seg = 0 ; l_Index_Seg < l_VOFF_Index_End_Off  ; l_Index_Seg++ )
            {
                LCD_API_DATA[ VOFFSET [ l_VOFF_Index_End  ] + l_Index_Seg ] = v[ i ];
                i = i + 1;
            }
        }
    }
}

/*******************************************************************************
* Conversie temperatura afisata pe LCD cu informatii de la TEMP_ComputeTemperature()
******************************************************************************/
void MENU_Update_Temperature(void)
{
//    MENU_Load_RAM(temperature,0,35,5);
    if(SWTIMER_ELAPSED==SWTIMER_GetStatus(SWTIMER_100MS_MENU_LOAD_TEMPERATURE))
    {
    /* Conversie SYS_SMQuantitiesValx1(temperatura de pe placa de baza) in sir ce poate fi afisat pe LCD*/
    NRtoString(SYS_SMQuantitiesValx1,temperature);
    SWTIMER_Start(SWTIMER_100MS_MENU_LOAD_TEMPERATURE,SWTIMER_100MS_MENU_LOAD_TEMPERATURE_LOAD);
    }
    /* Incarcare temperatura sistem; se afiseaza tot timpul in partea dreapta sus a ecranului */
}

/*******************************************************************************
* Functie ce incarca cu text din FLASH un buffer intermediar de dimensiune 40
* caractere, astfel incat la incarcarea textului static sau dinamic pe LCD, acesta sa nu palpaie
******************************************************************************/
void MENU_Load_Line_Buffer_Flash(__flash uint8_t v[], uint16_t offset, uint16_t length)
{
    uint8_t l_index;
    uint8_t i=0;
    
    for( l_index = offset ; l_index < (length+offset) ; l_index++ )
    {
        MENU_LINE_Buffer[ l_index ] = v [ i ];
        i++;

        
    }  
}


/*******************************************************************************
* Functie ce incarca cu text din FLASH un buffer intermediar de dimensiune 40
* caractere, astfel incat la incarcarea textului static sau dinamic pe LCD, acesta sa nu palpaie
******************************************************************************/
void MENU_Load_Line_Buffer_RAM(uint8_t v[], uint16_t offset, uint16_t length)
{
    uint8_t l_index;
    uint8_t i=0;
    
    for( l_index = offset ; l_index < (length+offset) ; l_index++ )
    {
        MENU_LINE_Buffer[ l_index ] = v [ i ];
        i++;

    }  
}


/*******************************************************************************
* Incarcarea meniului principal
******************************************************************************/
void Menu_Main( void )
{
    
    SYS_State = IDLE;
    
    /* Line 0 */  
    /* testare automata */
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_00,0,40);  
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
    
    /* Line 1 */
    /* Afisare data/ora */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_06,0,40);
    MENU_Load_Line_Buffer_RAM(SYS_years,11,2);
    MENU_Load_Line_Buffer_RAM(SYS_months,14,2);
    MENU_Load_Line_Buffer_RAM(SYS_days,17,2);
    MENU_Load_Line_Buffer_RAM(SYS_hours,20,2);
    MENU_Load_Line_Buffer_RAM(SYS_minutes,23,2);
    MENU_Load_Line_Buffer_RAM(SYS_seconds,26,2);
    MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TWI_ReceiveBuffer[10]],29,8);
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
    
    /* Line 2 */
/*******************************************************************************/    
    /* Afisare meniu selectie centrala  */
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_02,1,40);
    
    switch(SYS_BoilerType)
    {
#if(STAND_MODEL==STAND_FORTATE)
        case NATURALA:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,4,25);  
            break;
        }
        case FIXA:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,4,25);  
            break;
        }
        
        case VARIABILA:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_14,4,30);  
            break;
        }
        
        case SEMICONDENSATIE:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_15,4,25);  
            break;
        }
#else
        case C3825_29_35_BA:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,4,15);  
            break;
        }
        
        case C38GC35_CH1_CH2:
        {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,4,15);  
            break;
        }
        
#endif
        
    }
    if(MENU_Cursor_pos==0)
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);
    else
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1); 
    MENU_Load_RAM( MENU_LINE_Buffer,2,0,40);
/*******************************************************************************/
    /* Line 3 */
    /* Afisare meniu selectie test manual  */   
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_03,1,39);
    if(MENU_Cursor_pos == 1)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,3,0,40);
    
    
    /* Line 4 */
    /* Afisare meniu selectie test automat fast  */
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_04,1,39);
    if(MENU_Cursor_pos == 2)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,4,0,40);
    
    

  
    /* Line 5 */
    /* Afisare meniu selectie meniu anduranta  */
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_06,1,39);
    if(MENU_Cursor_pos == 3)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,5,0,40);
    
    
    /* Line 6 */
    /* Afisare meniu selectie setari  */
    MENU_Load_Line_Buffer_Flash(MENU_MAIN_STATIC_07,1,39);
    if(MENU_Cursor_pos == 4)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,6,0,40);
    
    /*----------------------------------------------------------------------------*/  
    /* Line 7 */  
    MENU_Load_Flash (MENU_BOILER_40SPACE,7,0,40);
    
}
/*******************************************************************************
* Incarcarea meniului de testare manuala
******************************************************************************/
void Menu_Manual( void )
{    
    
    SYS_State=MANUAL;
    /* Line 0 */  
    /* testare manuala */
    MENU_Load_Line_Buffer_Flash(MENU_TITLE_01,0,40);  
    
        switch(SYS_BoilerType)
        {
#if(STAND_MODEL==STAND_FORTATE)
        case NATURALA:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,12,15);  
            break;
          }
        case FIXA:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_17,12,4);  
            break;
          }
          
        case VARIABILA:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_18,12,9);  
            break;
          }
          
        case SEMICONDENSATIE:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_15,12,15);  
            break;
          }
#else
        case C3825_29_35_BA:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,12,15);  
            break;
          }
          
        case C38GC35_CH1_CH2:
          {
            MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,12,15);  
            break;
          }
#endif
          
        }
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
    
    
    /* Line 1 */ 
    /* Alimentare centrala  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_10,0,40);
    /* stare alimentare centrala  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[LO(PORTD_Buffer,CENTRALA_ONOFF)],15,3);
    /* stare cerere CH  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , CH_REQ ))],29,3);
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
    
    
    /* Line 2 */ 
    /* Ventilator  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_11,0,40);
    /* stare ventilator  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VENT)],15,3);
    /* stare cerere DHW  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , DHW_REQ ))],30 ,3);
    MENU_Load_RAM( MENU_LINE_Buffer,2,0,40);
    
    
    /* Line 3 */ 
    /* Vana Gaz  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_01,0,40);
    /* stare vana gaz   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VANAGAZ)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,3,0,40);

    /* Line 4 */ 
    /* Pompa */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_02,0,40);
    /* stare pompa centrala   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_PMPCENTRALA)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,4,0,40);

    /* Line 5 */ 
    /* Vana cu 3 cai  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_03,0,40);
    /* stare vana cu 3 cai  */
    MENU_Load_Line_Buffer_RAM(MENU_V3C_STATUS[HI(SYS_ENABLES,EN_V3C)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,5,0,40);

    /* Line 6 */ 
    /* Stare CT  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_04,0,40);
    MENU_Load_Line_Buffer_RAM(MENU_BOILER_STATUS[SYS_ManualState],9,14);  
    MENU_Load_RAM( MENU_LINE_Buffer,6,0,40);
    
       /* Line 7 */ 
    /*   */
    MENU_Load_Flash(MENU_BOILER_40SPACE,7,0,40);    

    
    


    
}



/*******************************************************************************
* Incarcarea meniului de selectie model centrala
******************************************************************************/
void Menu_Boiler( void )
{    
    
    /* Line 0 */  
    /* testare automata */
    MENU_Load_Line_Buffer_Flash(MENU_TITLE_00,0,40);  
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
/*----------------------------------------------------------------------------*/    
    /* Line 1 */  
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,1,39);
    if(MENU_Boiler_Cursor == 0)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
/*----------------------------------------------------------------------------*/      
    
    /* Line 2 */  
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,1,39);
    if(MENU_Boiler_Cursor == 1)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,2,0,40);
/*----------------------------------------------------------------------------*/  
#if(STAND_MODEL==STAND_FORTATE) 
    /* Line 3 */  
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_14,1,39);
    if(MENU_Boiler_Cursor == 2)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,3,0,40);
#else
    MENU_Load_Flash(MENU_BOILER_40SPACE,3,0,40);
#endif
/*----------------------------------------------------------------------------*/  
    
    /* Line 4 */  
    
#if(STAND_MODEL==STAND_FORTATE)      
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_15,1,39);
    if(MENU_Boiler_Cursor == 3)
    {
        MENU_Load_Line_Buffer_Flash( MENU_CURSOR,0,1);  
    }
    else
    {
        MENU_Load_Line_Buffer_Flash( MENU_1SPACE,0,1);
    }
    MENU_Load_RAM( MENU_LINE_Buffer,4,0,40);
#else
    
    MENU_Load_Flash(MENU_BOILER_40SPACE,4,0,40);
#endif
/*----------------------------------------------------------------------------*/  

    /* Line 5 */  
    MENU_Load_Flash(MENU_BOILER_40SPACE,5,0,40);
    /* Line 6 */  
    MENU_Load_Flash(MENU_BOILER_40SPACE,6,0,40);
    /* Line 7 */  
    MENU_Load_Flash(MENU_BOILER_40SPACE,7,0,40);
}


void Menu_Automatic_Err( void )
{
    SYS_State=AUTOMAT;
                            

    /* Line 0 */  
    /* testare automata */
    MENU_Load_Line_Buffer_Flash(MENU_TITLE_02,0,40); 
    
            switch(SYS_BoilerType)
            {
#if(STAND_MODEL==STAND_FORTATE)
            case NATURALA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,13,15);  
                break;
              }
            case FIXA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_17,13,4);  
                break;
              }
              
            case VARIABILA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_18,13,9);  
                break;
              }
              
            case SEMICONDENSATIE:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_15,13,15);  
                break;
              }
#else
            case C3825_29_35_BA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,13,15);  
                break;
              }
              
            case C38GC35_CH1_CH2:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,13,15);  
                break;
              }
#endif
              
            }
    
    
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
    
    
    /* Line 1 */ 
    /* Alimentare centrala  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_10,0,40);
    /* stare alimentare centrala  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[LO(PORTD_Buffer,CENTRALA_ONOFF)],15,3);
    /* stare cerere CH  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , CH_REQ ))],29,3);
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
    
    
    /* Line 2 */ 
    /* Ventilator  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_11,0,40);
    /* stare ventilator  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VENT)],15,3);
    /* stare cerere DHW  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , DHW_REQ ))],30 ,3);
    MENU_Load_RAM( MENU_LINE_Buffer,2,0,40);
    
    
    /* Line 3 */ 
    /* Vana Gaz  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_01,0,40);
    /* stare vana gaz   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VANAGAZ)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,3,0,40);

    /* Line 4 */ 
    /* Pompa */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_02,0,40);
    /* stare pompa centrala   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_PMPCENTRALA)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,4,0,40);

    /* Line 5 */ 
    /* Vana cu 3 cai  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_03,0,40);
    /* stare vana cu 3 cai  */
    MENU_Load_Line_Buffer_RAM(MENU_V3C_STATUS[HI(SYS_ENABLES,EN_V3C)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,5,0,40);

    /* Line 6 */ 
    /* Stare CT  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_04,0,40);
    MENU_Load_Line_Buffer_RAM(MENU_BOILER_STATUS[SYS_AutoState],9,14);  
    MENU_Load_RAM( MENU_LINE_Buffer,6,0,40);
    
    
    
    if( HI(SYS_BoilerErrors,IONIZARE))
    {
        MENU_Load_Flash( MENU_STATIC_09,7,0,40);
    }
    else if( HI(SYS_BoilerErrors,TERMOSTAT))
    {
        MENU_Load_Flash( MENU_STATIC_08,7,0,40);
    }
    else if( SYS_StartAutoTest == FALSE  )
    {
        MENU_Load_Flash( MENU_BOILER_40SPACE,7,0,40);
    }
    else if ( TEST_SEQUENCES[SYS_BoilerType][SYS_AutoTest] == (TESTS_NR-1))
    {
        MENU_Load_Line_Buffer_Flash(MENU_STATIC_07,0,40);
        MENU_Load_RAM( MENU_LINE_Buffer,7,0,40);
    }
    else 
    {
        MENU_Load_Line_Buffer_Flash(MENU_STATIC_05,0,40);

        MENU_Load_Line_Buffer_RAM(MENU_ERRORS_STATUS_TEST[TEST_SEQUENCES[SYS_BoilerType][SYS_AutoTest]],25,3);  

        MENU_Load_RAM( MENU_LINE_Buffer,7,0,40);
    }

            
          
    
}



void Menu_Endurance( void )
{
    SYS_State=ENDURANCE;
                            

    /* Line 0 */  
    /* testare automata */
    MENU_Load_Line_Buffer_Flash(MENU_TITLE_04,0,40);  
    
            switch(SYS_BoilerType)
            {
#if(STAND_MODEL==STAND_FORTATE)
            case NATURALA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,10,13);  
                break;
              }
            case FIXA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_17,10,4);  
                break;
              }
              
            case VARIABILA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_18,10,9);  
                break;
              }
              
            case SEMICONDENSATIE:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_15,10,15);  
                break;
              }
#else
            case C3825_29_35_BA:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_12,10,15);  
                break;
              }
              
            case C38GC35_CH1_CH2:
              {
                MENU_Load_Line_Buffer_Flash(MENU_STATIC_13,10,15);  
                break;
              }
              
#endif
              
            }
    
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
    
    
    /* Line 1 */ 
    /* Alimentare centrala  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_10,0,40);
    /* stare alimentare centrala  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[LO(PORTD_Buffer,CENTRALA_ONOFF)],15,3);
    /* stare cerere CH  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , CH_REQ ))],29,3);
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
    
    
    /* Line 2 */ 
    /* Ventilator  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_11,0,40);
    /* stare ventilator  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VENT)],15,3);
    /* stare cerere DHW  */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[(HI(SYS_Requests , DHW_REQ ))],30 ,3);
    MENU_Load_RAM( MENU_LINE_Buffer,2,0,40);
    
    
    /* Line 3 */ 
    /* Vana Gaz  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_16,0,40);
    /* stare vana gaz   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_VANAGAZ)],15,3);
    /* afisare numar de teste finalizate  */
    MENU_Load_Line_Buffer_RAM(MENU_EnduranceTestsNr_Buffer,37,2);
    MENU_Load_RAM( MENU_LINE_Buffer,3,0,40);

    /* Line 4 */ 
    /* Pompa */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_02,0,40);
    /* stare pompa centrala   */
    MENU_Load_Line_Buffer_RAM(MENU_EES_STATUS[HI(SYS_ENABLES,EN_PMPCENTRALA)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,4,0,40);

    /* Line 5 */ 
    /* Vana cu 3 cai  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_03,0,40);
    /* stare vana cu 3 cai  */
    MENU_Load_Line_Buffer_RAM(MENU_V3C_STATUS[HI(SYS_ENABLES,EN_V3C)],15,3);
    MENU_Load_RAM( MENU_LINE_Buffer,5,0,40);

    /* Line 6 */ 
    /* Stare CT  */
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_04,0,40);
    MENU_Load_Line_Buffer_RAM(MENU_BOILER_STATUS[SYS_EnduranceState],9,14);  
    MENU_Load_RAM( MENU_LINE_Buffer,6,0,40);
    
    
    if( HI(SYS_BoilerErrors,IONIZARE))
    {
        MENU_Load_Flash( MENU_STATIC_09,7,0,40);
    }
    else if( HI(SYS_BoilerErrors,TERMOSTAT))
    {
        MENU_Load_Flash( MENU_STATIC_08,7,0,40);
    }
    else if( SYS_StartEnduranceTest == FALSE  )
    {
        MENU_Load_Flash( MENU_BOILER_40SPACE,7,0,40);
    }
    else if ( TEST_SEQUENCES[SYS_BoilerType][SYS_EnduranceTest] == (TESTS_NR-1))
    {
        MENU_Load_Line_Buffer_Flash(MENU_STATIC_07,0,40);
        MENU_Load_RAM( MENU_LINE_Buffer,7,0,40);
    }
    else 
    {
        MENU_Load_Line_Buffer_Flash(MENU_STATIC_05,0,40);

        MENU_Load_Line_Buffer_RAM(MENU_ERRORS_STATUS_TEST[TEST_SEQUENCES[SYS_BoilerType][SYS_EnduranceTest]],25,3);  

        MENU_Load_RAM( MENU_LINE_Buffer,7,0,40);
    }
   
}


void Menu_Setup( void )
{
    
    /* Line 0 */  
    /* testare manuala */
    MENU_Load_Line_Buffer_Flash(MENU_TITLE_05,0,40); 
    /* temperatura sistem */
    MENU_Load_Line_Buffer_RAM(temperature,36,2);
    MENU_Load_Line_Buffer_Flash( CELSIUS,38,2);
    MENU_Load_RAM( MENU_LINE_Buffer,0,0,40);
/*----------------------------------------------------------------------------*/    
    /* Line 1 */  
    MENU_Load_Line_Buffer_Flash(MENU_STATIC_06,0,40);
   
    switch(MENU_Setup_Time_Cursor)
    {
        case YEAR:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                       
                    }
                     break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case MONTH:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                       
                    }
                     break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case DAY:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                       
                    }
                     break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case HOUR:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                        
                    }
                    break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case MINUTE:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                        break;
                    }
                    break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case SECONDS:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                        
                    }
                    break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
            break;
        }
        
        case WK_DAY:
        {
            switch(MENU_Blink_State)
            {
                case BLNK_OFF:
                {
                    MENU_Load_Line_Buffer_RAM(MENU_WEEK_DAYS[TEMP_Time[RTC_WK_DAY]],29,8);
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_ON;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                        
                    }
                    break;
                }
                
                case BLNK_ON:
                {
                    if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BLINK ))
                    {
                        MENU_Blink_State=BLNK_OFF;
                        SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
                    }
                    break;
                }
                
            }
            MENU_Load_Line_Buffer_RAM(TEMP_years,11,2);
            MENU_Load_Line_Buffer_RAM(TEMP_months,14,2);
            MENU_Load_Line_Buffer_RAM(TEMP_days,17,2);
            MENU_Load_Line_Buffer_RAM(TEMP_hours,20,2);
            MENU_Load_Line_Buffer_RAM(TEMP_minutes,23,2);
            MENU_Load_Line_Buffer_RAM(TEMP_seconds,26,2);
            break;
        }
        
        
    }
    
    
    
    

    
    
    
   
    MENU_Load_RAM( MENU_LINE_Buffer,1,0,40);
/*----------------------------------------------------------------------------*/      
    
    /* Line 2 */  
    MENU_Load_Flash( MENU_BOILER_40SPACE,2,0,40);
/*----------------------------------------------------------------------------*/  
    /* Line 3 */  
    MENU_Load_Flash( MENU_BOILER_40SPACE,3,0,40);
/*----------------------------------------------------------------------------*/  
    /* Line 4 */  
    MENU_Load_Flash( MENU_BOILER_40SPACE,4,0,40);
/*----------------------------------------------------------------------------*/      
     /* Line 5 */  
    MENU_Load_Flash( MENU_BOILER_40SPACE,5,0,40); 
/*----------------------------------------------------------------------------*/      
    /* Line 6 */  
    MENU_Load_Flash( MENU_BOILER_40SPACE,6,0,40);
/*----------------------------------------------------------------------------*/  
    /* Line 7 */  
    MENU_Load_Flash (MENU_BOILER_40SPACE,7,0,40);

         
}



void MENU_KEYS_Process(void)
{
    switch(KEYS_Pressed)
    {
        /* Daca se apasa tasta UP ..*/               
        case KEY_UP: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                
                switch( MENU_Index )
		{
		case HOME_SCREEN: 
		    {
			if(MENU_Cursor_pos==0)  
			    MENU_Cursor_pos=(MENU_MAIN_PARAMETERS_NR-1);
			else
			    MENU_Cursor_pos--;
			break;
		    }
		    
		    
		case BOILER_SELECT: 
		    {
			if(MENU_Boiler_Cursor==0)   
			    MENU_Boiler_Cursor = (MENU_BOILER_PARAMETERS_NR-1);
			else
			    MENU_Boiler_Cursor--;
			break;
		    }
		    
		   
		    
		case SETUP_TIME: 
		    {
			switch(MENU_Setup_Time_Cursor)
			{
			case YEAR:
			    {
				TEMP_Time[RTC_YEAR]=(TEMP_Time[RTC_YEAR]+1) % 100;
				break;
			    }
			case MONTH:
			    {
				
				TEMP_Time[RTC_MONTH]=(TEMP_Time[RTC_MONTH]+1) % 13;
				if(TEMP_Time[RTC_MONTH]==0) TEMP_Time[RTC_MONTH]=1;
				break;
			    }			    
			case DAY:
			    {
				TEMP_Time[RTC_DAYS]=(TEMP_Time[RTC_DAYS]+1) % 32;
				if(TEMP_Time[RTC_DAYS]==0) TEMP_Time[RTC_DAYS]=1;
				break;
			    }			    
			case HOUR:
			    {
				TEMP_Time[RTC_HOUR]=(TEMP_Time[RTC_HOUR]+1) % 24;
				break;
			    }
			case MINUTE:
			    {
				TEMP_Time[RTC_MIN]=(TEMP_Time[RTC_MIN]+1) % 60;
				break;
			    }
			case SECONDS:
			    {
				TEMP_Time[RTC_SEC]=(TEMP_Time[RTC_SEC]+1) % 60;
				break;
			    }			    
			case WK_DAY:
			    {
				TEMP_Time[RTC_WK_DAY]=(TEMP_Time[RTC_WK_DAY]+1) % 7;
				break;
			    }				    
			}
			break;
		    }		    
		    
		    
		}
                
            }
            
            
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta DOWN ..*/               
        case KEY_DOWN: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                
                switch(	MENU_Index )
                {
                    case HOME_SCREEN: 
                    {
                        MENU_Cursor_pos=(MENU_Cursor_pos+1) % MENU_MAIN_PARAMETERS_NR;
                        break;
                    }
                    
                    case BOILER_SELECT: 
                    {
                        MENU_Boiler_Cursor=(MENU_Boiler_Cursor+1) % MENU_BOILER_PARAMETERS_NR;
                        break;
                    }
                    
                   case SETUP_TIME: 
		    {
			switch(MENU_Setup_Time_Cursor)
			{
			case YEAR:
			    {
				if(TEMP_Time[RTC_YEAR]==0)   
				    TEMP_Time[RTC_YEAR] = 99;
				else
				    TEMP_Time[RTC_YEAR]--;
				break;
			    }
			case MONTH:
			    {
				if(TEMP_Time[RTC_MONTH]==0)   
				    TEMP_Time[RTC_MONTH] = 12;
				else
				    TEMP_Time[RTC_MONTH]--;				
				break;
			    }			    
			case DAY:
			    {
				if(TEMP_Time[RTC_DAYS]==0)   
				    TEMP_Time[RTC_DAYS] = 31;
				else
				    TEMP_Time[RTC_DAYS]--;						
				break;
			    }			    
			case HOUR:
			    {
				if(TEMP_Time[RTC_HOUR]==0)   
				    TEMP_Time[RTC_HOUR] = 23;
				else
				    TEMP_Time[RTC_HOUR]--;
				break;
			    }
			case MINUTE:
			    {
				if(TEMP_Time[RTC_MIN]==0)   
				    TEMP_Time[RTC_MIN] = 59;
				else
				    TEMP_Time[RTC_MIN]--;
				break;
			    }
			case SECONDS:
			    {
				if(TEMP_Time[RTC_SEC]==0)   
				    TEMP_Time[RTC_SEC] = 59;
				else
				    TEMP_Time[RTC_SEC]--;
				break;
			    }			    
			case WK_DAY:
			    {
				if(TEMP_Time[RTC_WK_DAY]==0)   
				    TEMP_Time[RTC_WK_DAY] = 6;
				else
				    TEMP_Time[RTC_WK_DAY]--;
				break;
			    }				    
			}
			break;
		    }		    
                    
                }                        
                
                
            }
            
            
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta LEFT ..*/               
        case KEY_LEFT: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
		{
                    
		case SETUP_TIME: 
		    {
			if(MENU_Setup_Time_Cursor==0)   
			    MENU_Setup_Time_Cursor = (MENU_SETUP_TIME_PARAMETERS_NR-1);
			else
			    MENU_Setup_Time_Cursor--;
			break;
		    }
		}                        
            }
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta RIGHT ..*/               
        case KEY_RIGHT: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
		{
		    
		case SETUP_TIME: 
		    {
			MENU_Setup_Time_Cursor=(MENU_Setup_Time_Cursor+1) % MENU_SETUP_TIME_PARAMETERS_NR;
			break;
		    }		    
		}                       
            }
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta ENTER ..*/               
        case KEY_ENTER: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
		{
		case HOME_SCREEN: 
		    {
			switch (MENU_Cursor_pos)
			{
			case 0:
			    {
				MENU_Index=BOILER_SELECT;
				
				break;  
			    }
			case 1:
                          {
                              MENU_Index=TESTARE_MANUALA;
                              USART_TX_Buffer[USART_TX_Head]='M';
                              USART_TX_Buffer[USART_TX_Head+1]='A';
                              USART_TX_Buffer[USART_TX_Head+2]='N';
                              USART_TX_Buffer[USART_TX_Head+3]='U';
                              USART_TX_Buffer[USART_TX_Head+4]='A';
                              USART_TX_Buffer[USART_TX_Head+5]='L';
                              USART_TX_Head = (USART_TX_Head + 6) % USART_TX_BUFFER_SIZE;
                              break;  
                          }
			case 2:
			    {
                                
                                USART_TX_Buffer[USART_TX_Head]='A';
                              USART_TX_Buffer[USART_TX_Head+1]='U';
                              USART_TX_Buffer[USART_TX_Head+2]='T';
                              USART_TX_Buffer[USART_TX_Head+3]='O';
                              USART_TX_Buffer[USART_TX_Head+4]='M';
                              USART_TX_Buffer[USART_TX_Head+5]='A';
                              USART_TX_Buffer[USART_TX_Head+6]='T';
                              USART_TX_Head = (USART_TX_Head + 7) % USART_TX_BUFFER_SIZE;
				MENU_Index=TESTARE_AUTOMATA;
				break;  
			    }
                            
			case 3:
			    {
				MENU_Index=ANDURANTA;
				SYS_EnduranceState=0U;
                                SYS_StartEnduranceTest=0U;
                                SYS_EnduranceTest=0U;
                                SYS_EnduranceTestsNumber=0U;
				break;  
			    }
			case 4:
			    {
				MENU_Index=SETUP_TIME;
                                TEMP_Time[RTC_SEC]=BCD2DEC(TWI_ReceiveBuffer[6U]);
                                TEMP_Time[RTC_MIN]=BCD2DEC(TWI_ReceiveBuffer[7U]);
                                TEMP_Time[RTC_HOUR]=BCD2DEC(TWI_ReceiveBuffer[8U]);
                                TEMP_Time[RTC_DAYS]=BCD2DEC(TWI_ReceiveBuffer[9U]);
                                TEMP_Time[RTC_WK_DAY]=BCD2DEC(TWI_ReceiveBuffer[10U]);
                                TEMP_Time[RTC_MONTH]=BCD2DEC(TWI_ReceiveBuffer[11U]);
                                TEMP_Time[RTC_YEAR]=BCD2DEC(TWI_ReceiveBuffer[12U]);
                                SWTIMER_Start(SWTIMER_100MS_BLINK , SWTIMER_100MS_BLINK_LOAD);
				break;  
			    }       
			}
			break;
		    }
		    
		case BOILER_SELECT: 
		    {
			switch(MENU_Boiler_Cursor)
			{
#if(STAND_MODEL==STAND_FORTATE)
                        case NATURALA:
                          {
                            SYS_BoilerType=NATURALA;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }
                          
                        case FIXA:
                          {
                            SYS_BoilerType=FIXA;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }
                          
                        case VARIABILA:
                          {
                            SYS_BoilerType=VARIABILA;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }
                          
                        case SEMICONDENSATIE:
                          {
                            SYS_BoilerType=SEMICONDENSATIE;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }	
#else
                        case C3825_29_35_BA:
                          {
                            SYS_BoilerType=C3825_29_35_BA;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }
                          
                        case C38GC35_CH1_CH2:
                          {
                            SYS_BoilerType=C38GC35_CH1_CH2;
                            MENU_Index=HOME_SCREEN;
                            break;
                          }
#endif
                          
			} 
			
			break;
		    }
		    
		case TESTARE_MANUALA: 
		    {
			break;
		    }
		    
		case SETUP_TIME: 
		    {
			TWI_Write(0xD0,TEMP_Time);   // sriere 12 bytes de date in bufferul de transmisie TWI
                        MENU_Index=HOME_SCREEN;         // dupa setarea datei/orei se iese in meniul principal
			break;
		    }
		}                        
            }
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta OK ..*/               
        case KEY_OK: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
                {
                    case HOME_SCREEN: 
                    {
                        
                        break;
                    }
                    
                    case BOILER_SELECT: 
                    {
                       
                        
                        break;
                    }
                    
                    case TESTARE_MANUALA: 
                    {
                        CENTRALA_XOR;
                        break;
                    }
                    
                    case TESTARE_AUTOMATA: 
                        {
                            SYS_AutoState=0;
                            SYS_StartAutoTest^=(1<<0);
                            SYS_AutoTest=0;
                            CENTRALA_XOR;
                            
                            break;
                        }
                    
                    case ANDURANTA: 
                    {
                        SYS_EnduranceState=0;
                        SYS_StartEnduranceTest^=(1<<0);
                        SYS_EnduranceTest=0;
                        SYS_EnduranceTestsNumber=0;
                        CENTRALA_XOR;
                        break;
                    }                    
                    
                }                        
                
            }
            
            
            KEYS_Pressed=0;
            break;
        }
        
        /* Daca se apasa tasta MENU ..*/               
        case KEY_MENU: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
                {
                    case HOME_SCREEN: 
                    {
                        
                        break;
                    }
                    
                    case TESTARE_MANUALA: 
                    {
                        DHW_REQ_XOR;
                        break;
                    }
                    

                    
                    
                }                        
                
            }
            
            
            KEYS_Pressed=0;
            break;
        }		
        
        /* Daca se apasa tasta BACK ..*/               
        case KEY_BACK: 
        {
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ))  
            {
                /* Apasare lunga (> 1 sec) */ 
                
                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            else
            {
                /* Apasare scurta (< 1 sec) */ 
                switch(	MENU_Index )
                {
                    case HOME_SCREEN: 
                    {
                        
                        break;
                    }
                    
                     case BOILER_SELECT: 
                    {
                        MENU_Index=HOME_SCREEN;
                        break;
                    }
                    
                    case TESTARE_MANUALA: 
                    {
                        MENU_Index=HOME_SCREEN;
                        break;
                    }
                    
                    case TESTARE_AUTOMATA: 
                    {
                        MENU_Index=HOME_SCREEN;
                        SYS_AutoTest=0;
                        SYS_StartAutoTest=FALSE;
                        EXT_RES_IO_OFF;
                        CENTRALA_OFF;
                        SYS_AutoState=0;
                        SYS_BoilerErrors=0;
                        
                        break;
                    }

                    case ANDURANTA: 
                    {
                        MENU_Index=HOME_SCREEN;
                        SYS_EnduranceState=0;
                        SYS_EnduranceTest=FALSE;
                        EXT_RES_IO_OFF;
                        CENTRALA_OFF;
                        SYS_EnduranceTestsNumber=0;
                        SYS_BoilerErrors=0;
                        break;
                    }

                    case SETUP_TIME: 
                    {
                        MENU_Index=HOME_SCREEN;
                        break;
                    }
                    
                }                        
                
            }
            
            
            KEYS_Pressed=0;
            break;
        }				
        
    }
    
    
}

/* Functie ce pregateste textul ce trebuie afisat pe LCD pentru valoarea secundelor
minutelor etc. ce trebuiesc afisare in meniul principal si cel de setari ceas
Texec Optimizari NONE 1 ms*/

void MENU_Update_Date(void)
{
    switch(MENU_Index)
    {
        case SETUP_TIME:
        {
            /* Update text data afisata in meniul de setup data */
            /* Update text secunde */
            NRtoString(TEMP_Time[RTC_SEC],TEMP_seconds);
            
            /* Update text minute */
            NRtoString(TEMP_Time[RTC_MIN],TEMP_minutes);
            
            /* Update text ore */
            NRtoString(TEMP_Time[RTC_HOUR],TEMP_hours);
            
            /* Update text zile */
            NRtoString(TEMP_Time[RTC_DAYS],TEMP_days);
            
            /* Update text zile ale saptamanii */
            NRtoString(TEMP_Time[RTC_WK_DAY],TEMP_wk_days);
            
            /* Update text luni */
            NRtoString(TEMP_Time[RTC_MONTH],TEMP_months);
            
            /* Update text ani */
            NRtoString(TEMP_Time[RTC_YEAR],TEMP_years);
            break;
        }
    
        case HOME_SCREEN:
        {
            /* Update text data afisata in meniul principal */
            /* Update text secunde */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[6]),SYS_seconds);
            
            /* Update text minute */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[7]),SYS_minutes);
            
            /* Update text ore */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[8]),SYS_hours);
            
            /* Update text zile */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[9]),SYS_days);
            
            /* Update text zile ale saptamanii */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[10]),SYS_wk_days);
            
            /* Update text luni */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[11]),SYS_months);
            
            /* Update text ani */
            NRtoString(BCD2DEC(TWI_ReceiveBuffer[12]),SYS_years);
            break;
        }
        default: break;
    }   
}



/* MENU_Process   */ 

void MENU_Process ( void )
{
    if(SWTIMER_ELAPSED == SWTIMER_GetStatus(SWTIMER_100MS_GET_RTC_TIME))
    {
        TWI_Read(0xD0,13);
        SWTIMER_Start(SWTIMER_100MS_GET_RTC_TIME, SWTIMER_100MS_GET_RTC_TIME_LOAD);
    }
    
    MENU_KEYS_Process();
      
    MENU_Update_Temperature();
    MENU_Update_Date();
    
    switch(MENU_Index)
    {
        case HOME_SCREEN:
        {
            Menu_Main();
            break;
        }
        
        case BOILER_SELECT:
        {
            Menu_Boiler();
            break;
        }
        
        case TESTARE_MANUALA:
        {
            Menu_Manual();
            break;
        }
        
        case TESTARE_AUTOMATA:
        {
            Menu_Automatic_Err();
            break;
        }
        
        case ANDURANTA:
        {
            NRtoString(SYS_EnduranceTestsNumber, MENU_EnduranceTestsNr_Buffer);
            Menu_Endurance();
            break;
        }
        
        case SETUP_TIME:
        {
            Menu_Setup();
            break;
        }

    }

}
