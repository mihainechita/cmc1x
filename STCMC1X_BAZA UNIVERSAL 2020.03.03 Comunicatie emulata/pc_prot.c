/*******************************************************************************
* Description : Fisier sursa pentru comunicatia cu PC
* File name   : pc_prot.c
* Author      : Ing. Mihai Nechita   
* Date	      : 2020.02.24          
* Copyright   : (C) 2020 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>
#include <inavr.h>

/* System */
#include "pc_prot.h"
#include "types.h"
#include "usart.h"
#include "stdmacros.h"
#include "io_drv.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
uint16_t PC_Command_ID=0;
uint8_t PC_Command_DATA[2];
/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

void PC_Protocol( void )
{
    /* Receptionare, verificare sintaxa si CRC pentru comenzile primite de ala PC */
    USART_Receive();
}


/* Functie ce decodifica si executa comenzile primite de la PC */
void PC_Prot_Decode_CMD(void)
{
    switch(PC_Command_ID)
    {
        /*--------------------------------------------------------------------*/         
      case CMD_ID_TT: 
        {
            switch(PC_Command_DATA[0])
            {
                
              case 0: 
                {
                    MENU_Index= HOME_SCREEN;
                    SYS_State = IDLE;
                    break;
                }  
                
                
              case 1: 
                {
                    MENU_Index=TESTARE_MANUALA;
                    SYS_State = MANUAL;
                    break;
                }
              case 2: 
                {
                    MENU_Index=TESTARE_AUTOMATA;
                    SYS_State = MANUAL;
                    break;
                }  
                
                
            }
            break;
        }
        
        /*--------------------------------------------------------------------*/         
      case CMD_ID_BP: 
        {
            switch(PC_Command_DATA[0])
            {
                
              case 0: 
                {
                    CENTRALA_OFF;
                    break;
                }  
                
                
              case 1: 
                {
                    CENTRALA_ON;
                    break;
                }
                
            }
            break;
        }
        
        /*--------------------------------------------------------------------*/         
      case CMD_ID_BT: 
        {
            
            
            switch(PC_Command_DATA[0])
            {
                
#if(STAND_MODEL==STAND_FORTATE)
              case 0: 
                {
                    SYS_BoilerType = NATURALA;
                    break;
                }  
                
                
              case 1: 
                {
                    SYS_BoilerType = FIXA;
                    break;
                }
                
              case 2: 
                {
                    SYS_BoilerType = VARIABILA;
                    break;
                }
                
              case 3: 
                {
                    SYS_BoilerType = SEMICONDENSATIE;
                    break;
                }
#else
              case 0: 
                {
                    SYS_BoilerType = C3825_29_35_BA;
                    break;
                }  
                
                
              case 1: 
                {
                    SYS_BoilerType = C38GC35_CH1_CH2;
                    break;
                }
#endif
                
            }
            break;
        }
        
        /*--------------------------------------------------------------------*/        
        
      case CMD_ID_EE: 
        {
            SYS_CMD_Relay_High = PC_Command_DATA[0];
            SYS_CMD_Relay_Low = PC_Command_DATA[1];
            break;
        }
     
      case CMD_ID_RQ: 
        {
           SYS_Requests = PC_Command_DATA[0];
            break;
        }
        
        
        
        
        
    }                   
        
        
}
