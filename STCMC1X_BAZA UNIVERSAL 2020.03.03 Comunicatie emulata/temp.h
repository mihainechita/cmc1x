/*******************************************************************************
* File name   : temp.h
* Author      : Birou Proiectare Automatizari-Soft 
* Description : Fisier header pentru calcul temperatura
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __temp_h
#define __temp_h

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
// Minimum and maximum measurable temperatures
// Note: Sensors measurable range is actually much larger,
//       but, except for the exterior sensor, firmware
//       only needs to measure temps in range [0...+108]C
#define TEMP_SENSOR_MINTEMP         0                   // Minimum measured temp (signed char!)
#define TEMP_SENSOR_MAXTEMP         (signed char)(+108) // Maximum measured temp (signed char!)

// Min and max allowed ADC values from sensors (used for detection of broken sensors)
#define TEMP_SENSOR_MINVAL          0x000B              // ~300_000 ohm ( <  -43 degrees )
#define TEMP_SENSOR_MAXVAL          0x0360              //     ~500 ohm ( > +125 degress )

#define TEMP_SENSOR_MAXDIF            85U        //OK      // for 100U ~ (2.5 * 200mV) 500mV =2.5 * (5-8 C) degrees difference (5C @40C, 8C @90C) // 0x28 ( 40 old value ) AlexB



/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
extern void TEMP_Update( void );
 
  #endif //_temp_h_
 