/*******************************************************************************
* Description : Fisier sursa pentru two wire interface
* File name   : twi.c
* Author      : Ing. Mihai Nechita            
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "twi.h"   
#include "rtc.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char TWI_TransmitBuffer[TWI_BUFFER_SIZE];
unsigned char TWI_ReceiveBuffer[TWI_BUFFER_SIZE];
unsigned char TWI_ReceiveBuffer_Index, TWI_TransmitBuffer_Index;
unsigned char TWI_TransmitBufferLength, TWI_ReceiveBufferLength=0;  
unsigned char TWI_Transmit_Receive_Mode; 
/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

void TWI_Init(void)
{
	// SCL freq = (CPU Clock) / (16 + 2(TWBR) * (prescale value))
	// 100 kHz = (8MHz) / (16 + 2(32) * (1))
	TWBR = 32;
	TWSR = 0;   // TWPS = 0 => prescale value = 1 
	TWCR = (1 << TWEN) |                               // Enable TWI interface and release TWI pins.
	       (1 << TWIE) | (0 << TWINT) |                // Disable TWI interupt.
	       (0 << TWEA) | (0 << TWSTA) | (0 << TWSTO) | // No signal requests.
	       (0 << TWWC);
}
/******************************************************************************
* Name      	: TWI_Write
* Descriere  	: Functie folosita pentru a incarca datele ce trebuiesc transmise
catre ceasul de timp real PCF8523T, date ce sunt apoi preluate de catre intreruperea
de TWI si incarcate in TWDR, apoi transmise
TWI_Write va fi apelata dupa ce data, ora, minutul etc au fost setate si se apasa 
ENTER, si se iese in meniul principal.
	La intrarea in meniul de setare data, de vazut daca fac apel de citire 
date noi pt a popula ecranul cu date fresh sau daca afisez ultimele date deja 
salvate. Functia populeaza bufferul de trimitere inainte de a da start trimiterii
 si implicit copierii de date din bufferul de trimitere in TWDR. 
Conform spec-urilor din datasheetul PCF8523T legate de secventa de scriere date 
in PCF, softul standului ar trebui sa execute urmatoarele:
	1. Setare comanda de START � data de catre functia TWI_Write, executata 
in zona de menu.c
	2. Automatul din intreruperea de TWI ar trebui sa detecteze daca comanda
 de START s-a dat cu succes, daca TWSR(TWI Status Register) (mascat cu 0xF8 pt a
 ignora bitii de prescaler), contine valoarea 0x08; Daca se gaseste valoare 0x08,
 automatul va incarca in TWDR TWI_TransmitBuffer[0], unde se gaseste adresa PCF (0xD0)
 plus 0 pe LSB pentru a semnaliza ca vreau sa fac o scriere (date incarcate deja
 de catre TWI_Write in TWI_TransmitBuffer, ISR doar va copia cand trebuie date
 din TWI_TransmitBuffer in TWDR). Din acest moment automatul poate functiona singur,
 nemaiavand nevoie de interventie din partea MAIN. Bitul TWINT este 0 cand TWI 
si-a terminat treaba, iar pt a incepe sa transmita din nou trebuie pus manual pe 1,
 deci starea TWI_Start va copia adresa PCF + 0 pe LSB si va scrie TWINT pe 1, 
pt a transmite acest byte.
	3.  Dupa ce TWI transmite SLA+W, bitul TWINT se va pune pe 0, si se va 
genera intreruperea de TWI din nou; de aceasta data automatul ar trebui sa intre 
in starea 2, si anume SLA+W_ACK; daca s-a transmis adresa si PCF a raspuns cu 
Acknowledge, TWSR ar trebui sa aiba valoarea 0x18; Daca TWST=0x18 atunci incarc 
in TWSR TWI_TransmitBuffer[1], unde TWI_Write a incarcat inainte adresa registrului
 de secunde, si anume 0x03; Setez TWINT pt a transmite adresa
	4. Daca se triggereaza ISR si TWSR = 0x28, incarc in TWDR valoarea 
registrului de secunde.
	5. Daca TWSR = 0x28, TWDR=valoarea registrului de minute
	6. Daca TWSR = 0x28, TWDR=valoarea registrului de ore
	7. Daca TWSR = 0x28, TWDR=valoarea registrului de zile ale lunii
	8. Daca TWSR = 0x28, TWDR=valoarea registrului de zile ale saptamanii
	9. Daca TWSR = 0x28, TWDR=valoarea registrului de luni
	10. Daca TWSR = 0x28, TWDR=valoarea registrului de ani
	11. STOP


Forma bufferelor TWI
TWI_TransmitBuffer[0]=0xD0; adresa PCF + 0 pt scriere
TWI_TransmitBuffer[1]=0x00; adresa registrului control 1 0x00
TWI_TransmitBuffer[2]=0xXX; valoarea registrului de control 1 
TWI_TransmitBuffer[3]=0xXX; valoarea registrului de control 2
TWI_TransmitBuffer[4]=0xXX; valoarea registrului de control 3
TWI_TransmitBuffer[5]=0xXX; valoarea registrului de secunde
TWI_TransmitBuffer[6]=0xXX; valoarea registrului de minute
TWI_TransmitBuffer[7]=0xXX; valoarea registrului de ore
TWI_TransmitBuffer[8]=0xXX; valoarea registrului de zile
TWI_TransmitBuffer[9]=0xXX; valoarea registrului de zile ale saptamanii
TWI_TransmitBuffer[10]=0xXX; valoarea registrului de luni
TWI_TransmitBuffer[11]=0xXX; valoarea registrului de ani
TWI_TransmitBuffer[12]=0xXX; valoarea registrului de alarma minut
TWI_TransmitBuffer[13]=0xXX; valoarea registrului de alarma ora
TWI_TransmitBuffer[14]=0xXX; valoarea registrului de alarma zi
TWI_TransmitBuffer[15]=0xXX; valoarea registrului de alarma zi a saptamanii 
TWI_TransmitBuffer[16]=0xXX; valoarea registrului de offset pentru corectie deviatie ceas

TWI_ReceiveBuffer[0]=0xD0; adresa PCF + 0 pt scriere
TWI_ReceiveBuffer[1]=0x00; adresa registrului 0 de control
TWI_ReceiveBuffer[2]=0xD1; adresa PCF + 1 pt citire 
TWI_ReceiveBuffer[3]=0xXX; valoarea registrului 0 de control 
TWI_ReceiveBuffer[4]=0xXX; valoarea registrului 1 de control 
TWI_ReceiveBuffer[5]=0xXX; valoarea registrului 2 de control 
TWI_ReceiveBuffer[6]=0xXX; valoarea registrului de secunde 
TWI_ReceiveBuffer[7]=0xXX; valoarea registrului de minute 
TWI_ReceiveBuffer[8]=0xXX; valoarea registrului de ore
TWI_ReceiveBuffer[9]=0xXX; valoarea registrului de zile
TWI_ReceiveBuffer[10]=0xXX; valoarea registrului de zile ale saptamanii
TWI_ReceiveBuffer[11]=0xXX; valoarea registrului de luni
TWI_ReceiveBuffer[12]=0xXX; valoarea registrului de ani
******************************************************************************/  
void TWI_Write(unsigned char slave_addr, unsigned char* data)
{
	// Copiere adresa PCF8523T in TWI_TransmitBuffer
	TWI_TransmitBuffer[0] = slave_addr ; 

	// Copiere adresa registrul de control 1 
	TWI_TransmitBuffer[1] = PCF8523T_CTRL_1_ADDRESS ; 
        
        // Copiere valoare registru de control 1
	TWI_TransmitBuffer[2] = 0x00 ;
        
        // Copiere valoare registru de control 2
	TWI_TransmitBuffer[3] = 0x00 ; 
        
        // Copiere valoare registru de control 3
	TWI_TransmitBuffer[4] = 0x00 ; 
	
	// Copiere date ce trebuiesc transmise in registrul TWI_TransmitBuffer
	for(unsigned char i = 0; i <= 6; i++)
		TWI_TransmitBuffer[i+5] = DEC2BCD(data[i]);	
	
        // Copiere valoare registru de alarma minute
	TWI_TransmitBuffer[12] = 0x80 ;
        
        // Copiere valoare registru de alarma ore
	TWI_TransmitBuffer[13] = 0x80 ;
        
        // Copiere valoare registru de alarma ore
	TWI_TransmitBuffer[14] = 0x80 ;
        
        // Copiere valoare registru de alarma ore
	TWI_TransmitBuffer[15] = 0x80 ;
        
        // Copiere valoare registru de offset setat pentru offset de -4.340 o data la 2 ore
	TWI_TransmitBuffer[16] = 0x80 ;  // 

        
	// Salvare lungime buffer transmisie pentru a fi folosita in intrerupere
	TWI_TransmitBufferLength = 17 ;
	
        /* Indicare operatie pentru automatul din ISR 0-Scriere / 1 - Citire */
        TWI_Transmit_Receive_Mode=0;
        
	// Initiere transmisie prin setarea conditiei de START
	TWI_Start;
}

void TWI_Read(unsigned char slave_addr, unsigned char length)
{
	// Copy slave address into the tx buffer.
	TWI_ReceiveBuffer[0] = slave_addr;
	
        TWI_ReceiveBuffer[1] = 0x00;  
        
        TWI_ReceiveBuffer[2] = slave_addr+1; 
	TWI_ReceiveBufferLength = length;
	
        /* Indicare operatie pentru automatul din ISR 0-Scriere / 1 - Citire */
        TWI_Transmit_Receive_Mode=1;
        
        
//        TWI_ReceiveBuffer_Index=0;
	// Initiere transmisie prin setarea conditiei de START
	TWI_Start;
	


}

