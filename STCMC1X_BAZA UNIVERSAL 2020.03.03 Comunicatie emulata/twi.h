/*******************************************************************************
* File name   : twi.h
* Author      : ing. Mihai Nechita
* Description : Fisier header pentru comunicatia TWI
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __twi_h
#define __twi_h

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
#define TWI_FREQUENCY           100000 
#define TWI_BUFFER_SIZE         20
     
     
#define TWI_START               0x08
#define TWI_REPEATED_START      0x10

#define TWI_MTX_SLA_W_ACK       0x18
#define TWI_MTX_SLA_W_NACK      0x20  
#define TWI_MTX_DATA_ACK        0x28
#define TWI_MTX_DATA_NACK       0x30
#define TWI_ARBITRATION_LOST    0x38
  
#define TWI_MRX_SLA_R_ACK       0x40
#define TWI_MRX_SLA_R_NACK      0x48
#define TWI_MRX_DATA_ACK        0x50
#define TWI_MRX_DATA_NACK       0x58



#define TWI_Start		(TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN)|(1<<TWIE))
#define TWI_Stop	       	(TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN)|(1<<TWIE)) 
#define TWI_ResetTWINT	        (TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE)) 
#define TWI_SendACK		(TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE)|(1<<TWEA)) 
#define TWI_SendNACK		(TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE))      
/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
extern unsigned char TWI_TransmitBuffer[TWI_BUFFER_SIZE];
extern unsigned char TWI_ReceiveBuffer[TWI_BUFFER_SIZE];
extern unsigned char TWI_ReceiveBuffer_Index, TWI_TransmitBuffer_Index;
extern unsigned char TWI_TransmitBufferLength, TWI_ReceiveBufferLength; 
extern unsigned char TWI_Transmit_Receive_Mode;     
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
void TWI_Init(void);
void TWI_Write(unsigned char slave_addr, unsigned char* data);
void TWI_Read(unsigned char slave_addr, unsigned char length);
#endif //_twi_h_
 