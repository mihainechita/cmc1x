/*******************************************************************************
* Description : Fisier sursa pentru citire intrari
* File name   : inputs.c
* Author      : Ing. Mihai Nechita             
* Date        : 08/11/2018         
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

/* System */
/* Includ header specific */
#include "enables.h"    

/* Includ macro-uri precum SET/CLR specific */
#include "stdmacros.h"  

/* Includ define-uri si variabile din io_drv deoarece acolo se pregatesc date de 
    la hardware ce sunt decodificate in acest fisier sursa */
#include "io_drv.h"
        
#include "isr.h"       
#include "msg.h"
#include "pc_prot.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char SYS_ENABLES=0;
unsigned char ENABLES_UpdateEnabled;  // Signals enables update event
/*------------------------------------------------------------------------------
* Private (static) variables
*----------------------------------------------------------------------------*/
static unsigned char ENABLES_PrevSample;
static unsigned char ENABLES_CrtSample;

static unsigned char ENABLES_PrevStatus;
static unsigned char ENABLES_CrtStatus;



unsigned char ENABLES_SamplesCounter[8];
unsigned char ENABLES_SamplesReady;
/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/


/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/
void ENABLES_Update(void)
{
    // variabile pentru feedback semnale de enable
    unsigned int l_ENABLES_Buffer=0;
    unsigned int l_TAST_EN_Buffer=0;
    unsigned char l_ENABLES=0;

    
    
    if( ENABLES_UpdateEnabled == TRUE )
    {
//        SET(PORTA,EXT_RES_IO);
        // resetare flag; informatie consumata 
        ENABLES_UpdateEnabled = FALSE;
        
        l_TAST_EN_Buffer=SYS_TAST_EN_Data;              // stocare in variabila locala
        
      
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
           
           l_ENABLES_Buffer=l_ENABLES_Buffer<<1;
           l_ENABLES_Buffer|= (l_TAST_EN_Buffer & 0x8000)>>15;
//           l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
      
        
        l_ENABLES=(unsigned char)l_ENABLES_Buffer;

        l_ENABLES=~l_ENABLES;
           
        
        /* incarcare variabile cu informatii filtrate */
        ENABLES_PrevSample = ENABLES_CrtSample;
        
        /* incarcare variabile cu informatii filtrate */
        
        /* incarcare variabile cu informatii filtrate */            
        ENABLES_CrtSample  = l_ENABLES;
           
        
        /* verificare daca semnalele de feedback sunt stabile/ nu au bounce */
        if( ENABLES_PrevSample == ENABLES_CrtSample)
        {
            /* Update stare taste */
            ENABLES_PrevStatus = ENABLES_CrtStatus;
            ENABLES_CrtStatus = ENABLES_CrtSample;
            if( ENABLES_PrevStatus != ENABLES_CrtStatus )
            {
                SYS_ENABLES = ENABLES_CrtStatus;
                
                MSG_Put( FBK_ID_SS, (unsigned int)SYS_ENABLES );

            }
            
            
        }
        
 
       
        
        
    }
        

    
    
    
}



























