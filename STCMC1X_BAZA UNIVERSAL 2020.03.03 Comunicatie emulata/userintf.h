/*******************************************************************************
* File name   : userintf.h
* Author      : Ing. Mihai Nechita
* Description : Fisier header pentru 
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __userintf_h
#define __userintf_h

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/







/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
extern unsigned char USERINTF_Refresh;
extern unsigned char USERNITF_ActivePageUpdate;
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
void USERINTF_Initialize( void );
void USERINTF_Update( void );
void USERINTF_Keys(void);
void USERINTF_Enables(void);


 
 
  #endif //__userintf_h
 

