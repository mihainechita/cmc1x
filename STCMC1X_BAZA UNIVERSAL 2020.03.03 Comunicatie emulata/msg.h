/*******************************************************************************
* Description : Fisier header pentru comunicatia cu PC
* File name   : pcmsg.h
* Author      : Ing. Mihai Nechita   
* Date	      : 22.11.2019          
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __msg_h
#define __msg_h

#include "usart.h"

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/

// Message structure
/* ------------------------------------------------------------------
 * | 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 |
 *  -----------------------------------------------------------------
 * |SOM |  ID(2)  |       DATA(4)     |DEN |        CRC(4)     |EOM |
 * ------------------------------------------------------------------- */
#define MSG_FRAME_LEN         (uint8_t)3U    // Frame length is sum of bytes from message frame (SOM, DEN, EOM)
#define MSG_ID_FIELD_LEN      (uint8_t)2U    // ID field length (in bytes)
#define MSG_DATA_FIELD_LEN    (uint8_t)4U    // Data field length (in bytes)
#define MSG_CRC_FIELD_LEN     (uint8_t)4U    // CRC field length (in bytes)
#define MSG_DATA_OFFSET       (uint8_t)3U    // Offset of data field
#define MSG_DEN_OFFSET        (uint8_t)7U    // Offset of DEN char
#define MSG_CRC_OFFSET        (MSG_DEN_OFFSET+1U)// Offset of CRC field
#define MSG_LEN           (MSG_FRAME_LEN + MSG_ID_FIELD_LEN + MSG_DATA_FIELD_LEN + MSG_CRC_FIELD_LEN)


// Message special chars
#define MSG_SOM_CHAR          USART_SOM_CHAR      // Start-Of-Message char: first char in message
#define MSG_EOM_CHAR          USART_EOM_CHAR      // End-Of-Message char: last char in message
#define MSG_DEN_CHAR          '#'                 // Data ENd char: first char after Data field

/* Status flags for message receptions
 * -----------------------------------------
 * | 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
 *  ---------------------------------------
 * |MCE | X  | X  | FE |DOR |MSE |MOE |MRC |
 * ----------------------------------------- */
// Message reception: status flags masks
#define MSG_STATUS_MSG_CE     (1U << 7U)          // Message CRC Error    (MCE)
#define MSG_STATUS_MSG_SE     (1U << 2U)          // Message Syntax Error (MSE)
#define MSG_STATUS_MSG_OK     USART_STATUS_RX_OK  // Message OK

// MSG functions: return codes
#define MSG_STATUS_GET_OK     USART_STATUS_RX_OK  // Message read and unpacked successfully
#define MSG_STATUS_PUT_OK     USART_STATUS_TX_OK  // Message packed and put in TX buffer




/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions
 *----------------------------------------------------------------------------*/
extern void MSG_Put( const unsigned int  msgID, const unsigned int  msgData );
extern void MSG_ASCII2Hex( uint8_t inbase, const uint8_t *inString, uint8_t* outString );




#endif //_pcmsg_h_
 