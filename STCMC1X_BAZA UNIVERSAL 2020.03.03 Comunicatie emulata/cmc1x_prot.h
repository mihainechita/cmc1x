/*******************************************************************************
* Description : Fisier header pentru 
* File name   : cmc1x_prot.h
* Author      : Ing. Mihai Nechita   
* Date	      : 26.11.2019        
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef _CMC1X_PROT_H_
#define _CMC1X_PROT_H_

/*------------------------------------------------------------------------------
 * Includes
 *----------------------------------------------------------------------------*/


/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
// Communication pass-key 
#define PROT_COMM_PASSKEYLO  (unsigned int)37U               // Lo byte
#define PROT_COMM_PASSKEYHI  (unsigned int)71U               // Hi byte

// Number of status messages to be sent to PC in each communication session
// Note: This refers to the number of status messages with *different* IDs !
//       The actual number of messages is greater because of indexed messages!
#define PROT_STATUSMSGSNO    (unsigned char)12U

// IDs for status messages sent by CMC1X to PC
#define PROT_MSGID_ID     (((unsigned int)'I' << 8U) | 'D') // 0 Last: Hi:CFG_BoilerModel; Lo: CFG_BoilerRev
#define PROT_MSGID_NM     (((unsigned int)'N' << 8U) | 'M') // 1 Number of processed Messages
#define PROT_MSGID_RQ     (((unsigned int)'R' << 8U) | 'Q') // 2 Request data
#define PROT_MSGID_SS     (((unsigned int)'S' << 8U) | 'S') // 3 System Status
#define PROT_MSGID_EE     (((unsigned int)'E' << 8U) | 'E') // 4 Execution Elements: Control & Status (status as received from P2)
#define PROT_MSGID_QV     (((unsigned int)'Q' << 8U) | 'V') // 5 sensors measured Quantities: Values
#define PROT_MSGID_CE     (((unsigned int)'C' << 8U) | 'E') // 6 CH Enable inputs
#define PROT_MSGID_MC     (((unsigned int)'M' << 8U) | 'C') // 7 Modulation Commands
#define PROT_MSGID_MF     (((unsigned int)'M' << 8U) | 'F') // 8 Modulation Feedbacks
#define PROT_MSGID_US     (((unsigned int)'U' << 8U) | 'S') // 9 User Settings
#define PROT_MSGID_TI     (((unsigned int)'T' << 8U) | 'I') //10-Not supported: system TIme (for C13/C14/C15/C17)
#define PROT_MSGID_CS     (((unsigned int)'C' << 8U) | 'S') //1x-Not supported: Chrono Settings (C13/C14/C15/C17)
#define PROT_MSGID_CK     (((unsigned int)'C' << 8U) | 'K') //11-ChecKsum: Flash/EEPROM memory Actual CRC (used to extract CRC when not updated)

// IDs for command messages sent by PC to CMC1X
// Note: IDs RQ, US, CS, TI are shared by status & command msgs !
#define PROT_MSGID_CC     (((unsigned int)'C' << 8U) | 'C') //1 Communication Control

#if ( CXX_BOILER_RESET_FROM_PC == 1U )
#warning ONLY LAB - Reset from PC activated !!! - LAB ONLY
#define PROT_MSGID_RR     (((unsigned int)'R' << 8U) | 'R') //2 Reset Request ! Not supported !
#endif

#define PROT_MSGID_ER     (((unsigned int)'E' << 8U) | 'R') //3 EEPROM Read & system Reset  C1X: !!!!: uchar -> uint !!!
#define PROT_MSGID_MT     (((unsigned int)'M' << 8U) | 'T') //5 Modulation Test commands - when FW in TEST (parameter FWARE_TYPE=TEST)
#define PROT_MSGID_IP     (((unsigned int)'I' << 8U) | 'P') //6 Installer params
#ifdef __DEBUG_VERSION
#define PROT_MSGID_OP     (((unsigned int)'O' << 8U) | 'P') //7 Operation params - debug only: replaces PP, II, DD, FF,
#endif

// IDs for response messages sent by CMC1X to PC
// Note: For each received message, CMC1X will send a response!
#define PROT_MSGID_ME     (((unsigned int)'M' << 8U) | 'E') // Message Error (msg response)
#define PROT_MSGID_EA     (((unsigned int)'E' << 8U) | 'A') // EEPROM Address
#define PROT_MSGID_EV     (((unsigned int)'E' << 8U) | 'V') // EEPROM Value

// Responses to messages received from PC (response is included in data field of message)
// ! WARNING ! Response codes must preserve compatibility with old CMC1112 boards !
#define PROT_RESP_BUF_OVERRUN   (unsigned char)0x0A    // USART error: more than one message received
#define PROT_RESP_CHAR_ERROR    (unsigned char)0x0B    // USART error: Frame error, Data-overrun error
#define PROT_RESP_CRC_ERROR     (unsigned char)0x0C    // Message error: CRC error (message corrupted)
#define PROT_RESP_SYNTAX_ERR	  (unsigned char)0x13    // Message error: invalid syntax
#define PROT_RESP_ID_INVALID    (unsigned char)0x0F    // Protocol error: invalid command ID
#define PROT_RESP_IDX_INVALID   (unsigned char)0x0D    // Protocol error: invalid data index
#define PROT_RESP_VAL_INVALID   (unsigned char)0x12    // Protocol error: invalid data value
#define PROT_RESP_CMD_OK        (unsigned char)0x11    // Command successfully processed
#define PROT_RESP_NONE          (unsigned char)0x00    // No message received (no response to send)

/* Communication with PC: status flags masks
 * (flags set by lower layers: USART and PCMSG)
 * -----------------------------------------
 * | 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
 *  ---------------------------------------
 * |MCE | X  | X  | FE |DOR |MSE |MOE |MRC |
 * ----------------------------------------- */
#define PROT_STATUS_ML_CE         PCMSG_STATUS_MSG_CE     // Message-Level errors: CRC Error (MCE)
#define PROT_STATUS_ML_SE         PCMSG_STATUS_MSG_SE     // Message-Level errors: Message Syntax Error (MSE)
#define PROT_STATUS_BL_FE         USART_STATUS_BL_FE      // Byte-Level errors: Frame error
#define PROT_STATUS_BL_DOR        USART_STATUS_BL_DOR     // Byte-Level errors: Data OverRun error
#define PROT_STATUS_ML_OE         USART_STATUS_ML_OE      // Message-Level errors: message/buffer overflow error: message longer than buffer size or more than one message received
#define PROT_STATUS_NONE          (unsigned char)0x00     // No status flag set

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
extern unsigned char PCPROT_CommunicationActive; 

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions
 *----------------------------------------------------------------------------*/
extern void PCPROT_Initialize( void );
extern void PCPROT_WriteStatusToPC( void );
extern void PCPROT_ReadCommandFromPC( void );
extern void PCPROT_SendEEPROMtoPC( void ); 


#endif



