/*******************************************************************************
* Description : Fisier sursa pentru comunicatia cu registrele de shiftare
* File name   : Comm595.C
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

#include "Comm595.h"          


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/


void Comm595_Send1B_LCD(unsigned char data)
{	
	unsigned char l_data=data;
	
        if( l_data & MASK_1BYTE ) DATA_SET_LCD    // daca MSB==1 atunci linia de date -> HIGH
        else DATA_CLR_LCD                           // else linia de date -> LOW
            SHIFT_CLOCK                               // clock: HIGH->LOW->HIGH
		l_data = l_data<<1;             // shiftare date cu 1 stanga
        
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
		l_data = l_data<<1;
            
       if( l_data & MASK_1BYTE ) DATA_SET_LCD
        else DATA_CLR_LCD
            SHIFT_CLOCK
	
        
            
        STROBE_LCD             // puls pe linia de enable 
                    
}











