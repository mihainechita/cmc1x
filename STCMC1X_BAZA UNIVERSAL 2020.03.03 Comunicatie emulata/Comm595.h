/*******************************************************************************
* File name   : Comm595.h
* Author      : Ing. Mihai Nechita 
* Description : Header pentru comunicatia cu registrele de shiftare
*             :
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __Comm595_h
#define __Comm595_h

#include "types.h"
#include "system.h"
#include "lcdapi.h"
#include "stdmacros.h"
#include "isr.h"
#include "io_drv.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
/* Iesiri shift register U3_2 */
#define LCD_DB7                 0x80U                      
#define LCD_DB6                 0x40U            
#define LCD_DB5                 0x20U            
#define LCD_DB4                 0x10U            
#define LCD_DB3                 0x08U            
#define LCD_DB2                 0x04U            
#define LCD_DB1                 0x02U            
#define LCD_DB0                 0x01U            

/*-----------------------------------------------------------------------------
 Define strobe pins for both 74hc595, data and clock line
------------------------------------------------------------------------------*/
#define MASK_2BYTES 		        0x8000U                                 
#define MASK_1BYTE 		        0x80U                                  

#define DATA_SET_CMD 		        SET(PORTB,SDATA_CMD);
#define DATA_CLR_CMD 		        CLR(PORTB,SDATA_CMD);

#define DATA_SET_LCD 		        SET(PORTB,SDATA_LCD);
#define DATA_CLR_LCD 		        CLR(PORTB,SDATA_LCD);

#define STROBE_CMD 			{ SET(PORTB,REG_CMD); CLR(PORTB,REG_CMD); }
#define STROBE_LCD 			{ SET(PORTB,REG_LCD); CLR(PORTB,REG_LCD); }
/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
 
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
 

/*------------------------------------------------------------------------------
 Routines used to transmitt data through 74hc595
------------------------------------------------------------------------------*/

/* folosita pentru trimitere comenzi spre U3(LCD) */
void Comm595_Send1B_LCD(unsigned char data);                    

/* _Comm595_h */
#endif  