/*******************************************************************************
* Description : Fisier header pentru protocolul de comunicatie cu placa de senzori
* File name   : sensors_prot.h
* Author      : Ing. Mihai Nechita   
* Date	      : 2020.02.27
* Copyright   : (C) 2020 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __SENSORS_PROT_H
#define __SENSORS_PROT_H

#include "types.h"
/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*******************************************************************************
* Public defines
******************************************************************************/
// for details see timer2 initialization in main (2us period for clk source) 
// for 2400 baud --> bit period is 416us; 416us / 0.52us = 208 - loading constant for timer2
#define SENSORS_PROT_DATA_BIT_DELAY		208
#define SENSORS_PROT_SAMPLES_PER_BIT		8

#define SENSORS_PROT_STOP_BIT_THRESHOLD		2
#define SENSORS_PROT_DATA_BIT_THRESHOLD		4
#define SENSORS_PROT_SAMPLE_BIT_DELAY		( SENSORS_PROT_DATA_BIT_DELAY>>2 )
#define SENSORS_PROT_STOP_BIT_SAMPLES		4

#define SENSORS_PROT_RX_PORT			PINB
#define SENSORS_PROT_TX_PORT			PORTD

#define SENSORS_PROT_RX_PIN			RxPB
#define SENSORS_PROT_TX_PIN			TxPB

#define SENSORS_PROT_READ_RX_PIN		( SENSORS_PROT_RX_PORT & ( 1<<SENSORS_PROT_RX_PIN ) )
#define SENSORS_PROT_WRITE_TX_PIN( a )	        SENSORS_PROT_TX_PORT = ( ( ( a ) == 0 ) ? ( SENSORS_PROT_TX_PORT & ~( 1<<SENSORS_PROT_TX_PIN ) ) : ( SENSORS_PROT_TX_PORT | ( 1<<SENSORS_PROT_TX_PIN ) ) )
#define SENSORS_PROT_WRITE_TX_PIN_0		SENSORS_PROT_TX_PORT & ~( 1<<SENSORS_PROT_TX_PIN )
#define SENSORS_PROT_WRITE_TX_PIN_1		SENSORS_PROT_TX_PORT | ( 1<<SENSORS_PROT_TX_PIN )

#define SENSORS_PROT_RX_BUFFER_LEN		32
#define SENSORS_PROT_TX_BUFFER_LEN		32

#define SENSORS_PROT_RX_BUFFER_READY		0x00
#define SENSORS_PROT_RX_BUFFER_EMPTY		0x01

#define SENSORS_PROT_TX_BUFFER_READY		0x00
#define SENSORS_PROT_TX_BUFFER_FULL		0x01

/*******************************************************************************
* Type definitions
******************************************************************************/

/*******************************************************************************
* Public variables
******************************************************************************/

/*******************************************************************************
 * Private (static) variables
******************************************************************************/

/* circular buffer for receiver process */
extern uint8_t SENSORS_PROT_RX_Buffer[ SENSORS_PROT_RX_BUFFER_LEN ];
/* control variables of circular buffer for receiver process */
extern uint8_t SENSORS_PROT_RX_Buffer_Head, SENSORS_PROT_RX_Buffer_Tail;
	
/* circular buffer for transmitter process */
extern uint8_t SENSORS_PROT_TXBuffer[ SENSORS_PROT_TX_BUFFER_LEN ];
/* control variables of circular buffer for transmitter process */
extern uint8_t SENSORS_PROT_TX_Buffer_Head, SENSORS_PROT_TX_Buffer_Tail;






/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*******************************************************************************
* Public (exported) functions 
******************************************************************************/
/*__SENSORS_PROT_H */
#endif 
 