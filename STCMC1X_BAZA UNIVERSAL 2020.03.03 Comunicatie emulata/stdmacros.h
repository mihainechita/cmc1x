/*******************************************************************************
 * File name   : STDMACROS.H
 * Author      : Birou Proiectare Automatizari-Soft
 * Description : Macro-uri standard pentru toate modulele.
 *             : Majoritate sunt folosite pentru acces de date sau operatii atomice
 * Note        :-Unused macros where commented in order to eliminate linter
 *             : info's.
 *             :-Used "DU" not "D" (i.e 1U, not 1) where macros assume unsigned
 *             : operations in order to avoid linter warnings when macro is used!
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
 ******************************************************************************/

#ifndef _STDMACROS_H_
#define _STDMACROS_H_

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/

// Binary constants (0/1)
// Note: Value 0 was chosen as default-value because
//       it is the normal/default RAM value after a reset !
#define ON            1U
#define OFF	      0U

#define NOT_OK        1U // unusual case is 1
#define OK            0U // normal  case is 0

#define PRESENT       1U
#define NOT_PRESENT   0U

#define TRUE          1U
#define FALSE         0U

//#define NULL          0U
#define NONE          0U

#if (NOT_OK != 1U)
      #error "stdmacros.h: NOT_OK macro must be equal to 1U (TRUE) because in some modules \
              the result of an evaluated condition is store directly in a variable of OK/NOT_OK 'type' (see temp.c) !"
#endif


#define Status_LED  PORTD, 7                
                  
                  

// Bit manipulation defines: bits reading & testing
#define HI(__var, __bit_pos)          ( ((__var) & (1U<<(__bit_pos)))?1U:0U)
#define LO(__var, __bit_pos)          (!((__var) & (1U<<(__bit_pos))))
#define HI_BITS(__var, __bits_mask)   ( ((__var) & (__bits_mask))?1U:0U)
#define LO_BITS(__var, __bits_mask)   (!((__var) & (__bits_mask)))

// Bit manipulation defines: bit writing: SET, CLEAR, XOR
// single-bit operation (bit specified by position)
#define SET(__var, __bit_pos)         (__var) |= (1U<<(__bit_pos))
#define CLR(__var, __bit_pos)         (__var) &=~(1U<<(__bit_pos))
#define XOR(__var, __bit_pos)         (__var) ^= (1U<<(__bit_pos)) 

// multi-bit operation (bits specified by mask)
#define SET_BITS(__var, __bits_mask)  (__var) |= (__bits_mask)
#define CLR_BITS(__var, __bits_mask)  (__var) &=~(__bits_mask)
//#define XOR_BITS(__var, __bits_mask)(__var) ^= (__bits_mask)

// Nibble (4-bit) manipulation defines: nibble reading
#define HI_NIBBLE(__var)        (((unsigned char)(__var))>>4U)
#define LO_NIBBLE(__var)        (((unsigned char)(__var))&0x0F)

// Byte manipulation defines: access for byte reading
#define HI_BYTE(__var) 	        (unsigned char)(*(((unsigned char *)&(__var))+1U))
#define LO_BYTE(__var) 	        (unsigned char)(*(((unsigned char *)&(__var))+0U))
//#define GETBYTE(__var,__idx)  (unsigned char)(*(((unsigned char *)&(__var))+(__idx)))

// Byte manipulation defines: access for byte writing
#define HI_BYTE2SET(__var) 	    (*(((unsigned char *)&(__var))+1U))
#define LO_BYTE2SET(__var) 	    (*(((unsigned char *)&(__var))+0U))
//#define BYTE2SET(__var,__idx) (*(((unsigned char *)&(__var))+(__idx)))

// Word (integer) manipulation defines: access for byte reading
//#define HI_WORD(__var) 	    (unsigned int)(*(((unsigned int*)&(__var))+1U))
//#define LO_WORD(__var) 	    (unsigned int)(*(((unsigned int*)&(__var))+0U))

// Word (integer) manipulation defines: access for byte writing
//#define HI_WORD2SET(__var) 	(*(((unsigned int *)&(__var))+1U))
//#define LO_WORD2SET(__var) 	(*(((unsigned int *)&(__var))+0U))

// BCD manipulation defines
//#define BCD_HI(__var)         (((unsigned char)(__var)) / 10)
//#define BCD_LO(__var)         (((unsigned char)(__var)) % 10)
//#define BCD2HEX(__var)        (((__var)&0x40) ? : ((__var)-'A'+10) : ((__var)&0x0F))

// Maximum retrieval
#define MAX( __A, __B)          ( ( (__A) > (__B) ) ? (__A) : (__B) )

// Absolute value retrieval
#define ABS_WORD( A )           ( ( ( A )  & 0x8000 ) ? ( ~( ( A ) + 0xFFFF ) ) : ( A ) ) // Max 17 cycles
//#define ABS_BYTE( A )         ( ( ( A )  &   0x80 ) ? ( ~( ( A ) +   0xFF ) ) : ( A ) )

// converts a value from format [0-1000] to percent [100%] (+ cast uint to uchar)
//#define VAL1000_TO_PERCENT( VAL )	(unsigned char)( ( VAL ) / (unsigned char)10 )
// converts a value from percent [100%] to format [0-1000] (+ cast uchar to uint)
//#define PERCENT_TO_VAL1000( PER )	(unsigned int)( ( PER ) * (unsigned char)10 )

/*****************************************************************************
 * Name       : Atomic operations: read, write, read-then-write
 * Parameters :-ISRvar = multibyte data shared between ISR and normal funcs
 *             -SRCvar = multibyte data to be written to ISRvar
 *             -DESTvar= multibyte variable to be written with ISRvar
 * Description: Useful when writing/reading variables that:
 *            : -require a multiple access to memory
 *            :  (i.e. size is larger than data-bus)
 *            : -and are shared between ISRs and normal funcs
 *            : Used to avoid cases where ISR interrupts normal function
 *            : exactly when ISRvar is written:
 *            : i.e. byte 0 of ISRvar has been written with new value,
 *            : but byte 1 of ISRvar has not been written yet with new value !
 * Warning    :-These macros assume that interrupts are enabled,
 *            : otherwise it would not make sense to use these macros !!!
 *            : If interrupts are not enabled, then DO NOT USE these macros,
 *            : because after the atomic operation is executed, interrupts
 *            : are enabled by any of these macros !!!
 *            :-Do atomic operations ONLY if necessary, as it implies
 *            : disabling ints for a few cycles !
 *            : Atomic operations should be performed only on transitions,
 *            : not at every main call as this would have a bad impact
 *            : on ISRs execution !
 *****************************************************************************/
#define WRITE_WORD_ATOMIC( ISRvar, SRCvar)\
    __disable_interrupt();\
    (ISRvar) = (SRCvar);\
    __enable_interrupt()

#define READ_WORD_ATOMIC( ISRvar, DESTvar)\
    __disable_interrupt();\
    (DESTvar) = ISRvar;\
    __enable_interrupt()

#define READWRITE_WORD_ATOMIC( ISRvar, DESTvar, SRCvar)\
    __disable_interrupt();\
    (DESTvar) = (ISRvar);\
    (ISRvar)  = (SRCvar);\
    __enable_interrupt()


#endif



