/*******************************************************************************
* File name   : MAIN.C
* Author      : Ing. Mihai Nechita
* Description : Firmware microcontroller P1 de pe placa de baza a standului CMC1X
* Data        : 2020.02.14
* time exec   : 5 ms min; 5.2 ms max Optimizari NONE
* time exec   : 2.6 ms us min; 2.7 ms max Optimizari HIGH SPEED
* perioada    : 10 ms @ clock 8 MHz; setare clock fuse la Ext crystal resonator medium freq startup time 16K CK + 64 ms
* Copyright   : (C) 2020 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
*
* Version     : 
2020.03.03      Comunicatie emulata pt comunicatie cu placa de senzori


Valoare CSTACK: 0x64 = 100 bytes
Valoare RSTACK: 50
******************************************************************************/
#include "headers.h"

__C_task main( void )
{
    /* Dezactivare intreruperi inainte de SystemInit pt initializare fara intrerupere */
     __disable_interrupt();
    
    /* Initializare sistem; setari porturi, timere, module ADC, USART, watchdog */ 
    SystemInit();
    /* Bucla principala executata cu perioada de 10 ms */
    for( ; ; )
    {
	/* Actualizare timere software; functia SWTIMER_Update se apeleaza prima pentru a nu fi intarziata de alte procese */ 
	SWTIMER_Update();
        
	/* Executie bucla main cu perioada de 10ms  */
	if(SWTIMER_ELAPSED == SWTIMER_GetStatus(SWTIMER_10MS_TESTMAIN))
	{   
#ifdef __RELEASE_VERSION
            /* Verificarea memoriei Flash (doar in RELEASE MODE) */
            FLASH_CheckMemory();            
#endif            
	    /* Achizitie semnal de la senzorul de temperatura de pe placa de baza pentru supervizare temperatura sistem */
	    ADC_UpdateData();   
	    
	    /* Procesare date de la modulul ADC pentru conversie din interval 0-1024 in temperatura*/
	    TEMP_Update();
	    
	    /* Achizitie semnale de feedback privind starea butoanelor de meniu ale standului, de la registrul U1_2 Parallel Input Serial Output (PISO) */
	    KEYS_Update();
	    
	    /* Achizitie semnale de feedback privind elementele centralei (pompa, fan, vana gaza, vana cu 3 cai), de la registrul U2_2 Parallel Input Serial Output (PISO) */
	    ENABLES_Update();
	    
	    /* Functie ce actualizeaza semafoarele care permit citirea alternativa a butoanelor standului sau a semnalelor de feedback ale elementelor centralei (pompa, fan, vana gaza, vana cu 3 cai) */
	    USERINTF_Update();
	 
	    /* Functie ce proceseaza informatia ce trebuie afisata pe LCD-ul standului */
	    MENU_Process();
	   
	    /* Automatul de stare al standului ce cuprinde si automatele de testare manuala si automata ale centralei  */
	    SYS_State_Update();
            
            /* Update stare relee sau tastatura/semnale de feedback elemente centrala */
            IO_DRV_Update();

            /* Decodificare mesaje primite de la PC si trimitere feedback de la elementele centralei */ 
            PC_Protocol();

	    /* Resetare timer de bucla main @10ms perioada*/
	    SWTIMER_Start(SWTIMER_10MS_TESTMAIN,SWTIMER_10MS_TESTMAIN_LOAD);
            
           /* Resetare watchdog */ 
            __watchdog_reset();
	}
    }
}



