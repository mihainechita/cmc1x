/*******************************************************************************
* Description : Fisier header pentru menu
* File name   : menu.h
* Author      : Ing. Mihai Nechita   
* Date	      : 05.12.2018           
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __menu_h
#define __menu_h

#include "aaa_sys_config.h"
#include "types.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/


#define HOME_SCREEN                     0
#define BOILER_SELECT                   1
#define TESTARE_MANUALA                 2
#define TESTARE_AUTOMATA                3 
#define ANDURANTA                     	4
#define SETUP_TIME                    	5

#define MENU_READY     0x1000
#define MENU_BUSY      0xEFFF
     


/* Define-uri ce definesc asezarea in ecran a coloanelor pe care pozitionez cursorul,
precum si textele dinamice (ON,OFF,etc) */
#if(STAND_MODEL==STAND_FORTATE)
#define MENU_MAIN_PARAMETERS_NR        5
#define MENU_BOILER_PARAMETERS_NR      4
#define MENU_SETUP_TIME_PARAMETERS_NR  7
#elif(STAND_MODEL==STAND_CONDENSATIE)
#define MENU_MAIN_PARAMETERS_NR        5
#define MENU_BOILER_PARAMETERS_NR      2
#define MENU_SETUP_TIME_PARAMETERS_NR  7
#endif


#define YEAR	0
#define MONTH	1     
#define DAY	2
#define HOUR	3
#define MINUTE	4
#define SECONDS	5
#define WK_DAY	6     
     
#define BLNK_OFF       0
#define BLNK_ON        1     
     
/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/

extern unsigned long int SYS_LCD_UpdateEnabled;

extern uint8_t MENU_Index;
    
/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
void MENU_Process ( void );     
     
void Menu_Update(void);
void NRtoString(unsigned int number, unsigned char *text);


void Menu_Main( void );
void Menu_Boiler( void );
void Menu_Manual( void );
void Menu_Automat( void );
void Menu_Endurance( void );
void Menu_Setup( void );




	
  #endif //__menu_h
 