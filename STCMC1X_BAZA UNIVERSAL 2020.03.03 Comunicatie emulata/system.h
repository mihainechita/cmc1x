/*******************************************************************************
* Description : Fisier header pentru functiile de sistem
* File name   : system.h
* Author      : Ing. Mihai Nechita   
* Date	      : 29.07.2019           
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/
#ifndef __SYSTEM_H
#define __SYSTEM_H

#include "aaa_sys_config.h"
#include "menu.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/

#define SYS_TEMP        PA0
#define C_LCD_RD        PA1
#define C_LCD_WR        PA2
#define C_LCD_CD        PA3
#define C_LCD_RES       PA4
#define C_LCD_CE        PA5
#define EXT_RES_IO      PA6
#define C_SCLK          PA7

#define SDATA_CMD       PB0
#define SDATA_LCD       PB1
#define RxPB            PB2
#define REG_CMD         PB3
#define REG_LCD         PB4
#define MOSI            PB5
#define MISO            PB6
#define SCK             PB7

#define SCL             PC0 
#define SDA             PC1
#define TCK             PC2
#define TMS             PC3
#define TDO             PC4
#define TDI             PC5
#define TAST_IN         PC6
#define EN_IN           PC7

#define RX              PD0
#define TX              PD1
#define SINCRO_50Hz     PD2
#define SCANTEIE        PD3
#define BUZZER_ONOFF    PD4
#define BKL_ONOFF       PD5
#define CENTRALA_ONOFF  PD6
#define TxPB            PD7
 
/* Cate treceri prin ISR_TIMER0 pentru a executa zona de scriere CMD si citire
tastatura si feedback */
#define SYS_ISR_CMD_KEYS_MS         5    

/* Definire flow echivalent a 7 L/m */
#define SYS_FLOW_FREQUENCY         100          

/*--------------------------------------------------------------------------------*/
#define DELAY_1SEC      8000000
     
     
#define SYS_SenTemp       SYS_SensorModels           		                // Water temp sensor model (CH-flow)
// Sensor models
#define SYS_SENSORMODELSVECT_LEN  1U                                            // Vector length
// Sensors errors
extern unsigned char SYS_SensorError;
// Sensors measured quantities: status
#define SYS_SMQSTATUSVECT_LEN     1U                                            // Vector length
// Sensors measured quantities: lenght, indexes
#define SYS_SMQVALX1VECT_LEN      1U                                            // Vector length
// Aliases for sensors quantities (scale x10: most unsigned int)
#define SYS_SMQVALXSVECT_LEN      1U                                            // Vector length

#define SYS_TEMP_IDX  1u
#define SYS_TEMP_MAXTEMP 100u


// Sensor models used by TEMP_Update to select sensor model from TEMP_NtcTable[6]
// For each model of sensor we have different translation vectors from ADC_Value to Temperature 
#define SYS_SENSORMODEL_BRAHMA_3435     0
#define SYS_SENSORMODEL_KOBER_3380      0 //1 in caz ca se foloseste tot vectorul de modele de senzor
#define SYS_SENSORMODEL_OTHER_3530      2
#define SYS_SENSORMODEL_TASSERON_3760   3
#define SYS_SENSORMODEL_TASSERON_3977   4
#define SYS_SENSORMODEL_KOBER_3450      5



#define F_CPU   8000000                                                         /* Frecventa sistem in Hz */
#define TIMER1_INTERRUPT_FREQUENCY 4000                                         /* 1 / 0.00025ms = 4000 in Hz */
#define OCR1_VALUE (F_CPU/TIMER1_INTERRUPT_FREQUENCY)-1                         /* Valoarea OCR1 */




#if  (TIMER1_INTERRUPT_FREQUENCY!=4000)                         
#error "Modifica valoarea lui TIMER1_INTERRUPT_FREQUENCY la 4000!!!"
#endif

#if  (F_CPU!=8000000) 
#error "F_CPU modificat!!! Verifica timerele soft si perioada intreruperii de TIMER0"
#endif

#define START_TIMER2 {\
	SET( TIFR, OCF2 );\
	SET( TIMSK, OCIE2 );\
	TCCR2 |= ( (1<<CS21)  );\
}

#define STOP_TIMER2 {\
	TCCR2 &= ~( (1<<CS21)  );\
	CLR( TIMSK, OCIE2 );\
}

#define ENABLE_INT2 {\
	SET( GIFR, INTF2 );\
	SET( GICR, INT2 );\
}

#define DISABLE_INT2 {\
	CLR( GICR, INT2 );\
}

#define ENABLE_INT0 {\
	SET( MCUCR, ISC01);\
	SET( MCUCR, ISC00);\
	SET( GIFR, INTF0 );\
	SET( GICR, INT0 );\
}

#define DISABLE_INT0 {\
	CLR( GICR, INT0 );\
}

#define START_ADC {\
	SET( ADCSRA, ADSC );\
}

/* Define-uri pentru starile standului */
#define IDLE                    0
#define MANUAL                  1
#define	AUTOMAT                 2
#define ENDURANCE               3

/* Define-uri pentru starile centralei */
#define	WAITING       	0
#define	PREPURGE      	1
#define	IGNITION       	2
#define	BURNING      	3
#define WAIT4ERROR      4
#define ERROR_NO_BURN   5
#define BUZZER          6
#define CT_RESTART      7
#define WAIT4CT_POWERUP 8

#define DHW_REQ         0
#define CH_REQ          1

#define DHW_REQ_ON   	SET(SYS_Requests,DHW_REQ)
#define DHW_REQ_OFF   	CLR(SYS_Requests,DHW_REQ)
#define DHW_REQ_XOR   	XOR(SYS_Requests,DHW_REQ)

#define	CH_REQ_ON	SET(SYS_Requests,CH_REQ)
#define	CH_REQ_OFF   	CLR(SYS_Requests,CH_REQ)
#define CH_REQ_XOR   	XOR(SYS_Requests,CH_REQ)

#if(STAND_MODEL==STAND_FORTATE)
#define E10             0
#define E11_SENSOR      1
#define E20             2
#define E26_TS          3
#define E26_PA          4
#define E40             5
#define E52             6
#define DHW             7
#define TESTS_NR        8

#define NATURALA                0
#define FIXA                    1
#define VARIABILA               2
#define SEMICONDENSATIE         3
#else
#define C3825_29_35_BA          0
#define C38GC35_CH1_CH2         1

#endif

#if(STAND_MODEL==STAND_CONDENSATIE)
#define E10             0
#define E11_SENSOR      1
#define E20             2
#define E26_TS          3
#define E40             4
#define E51             5
#define DHW             7
#define TESTS_NR        8

#endif

#define TERMOSTAT       0
#define POMPA           1
#define IONIZARE        2



/*------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
// Critical errors flags/items
extern unsigned char SYS_UserIntfError;        // User-interface board failure (disconnected, broken, etc)

extern unsigned int SYS_TAST_EN_Data;           // Variabila ce contine valoarea tastelor si semnalelor de enable 
                                                // citite de catre IO_Driver direct de la hardware
extern unsigned int SYS_CMD_Data;               // Variabila ce contine comanda releelor de pe registrele SIPO

extern unsigned char SYS_SensorModels;
// Sensors measured quantities: status
extern unsigned char SYS_SMQuantitiesStatus;
// Sensors measured quantities: values
extern unsigned char SYS_SMQuantitiesValx1;             // Non-scaled values (scale x1): Used to store integer part of temps (flow and pressure locations are dummy)!
extern unsigned int  SYS_SMQuantitiesValxS;             // Scaled values (most @ scale x10)

extern unsigned char SYS_State;
extern unsigned char SYS_ManualState;
extern unsigned char SYS_AutoState;

extern unsigned int SYS_Flow_Tick_Nr;
extern unsigned char SYS_Requests;
extern unsigned char SYS_AutoTest;
extern unsigned char SYS_StartAutoTest; 


extern unsigned char SYS_EnduranceState;
extern unsigned char SYS_EnduranceTest;
extern unsigned char SYS_StartEnduranceTest; 

extern unsigned int SYS_EnduranceTestsNumber;

__eeprom extern unsigned char SYS_BoilerType;

extern unsigned char SYS_BoilerErrors;

/* Vector ce contine secventele de erori ce trebuiesc testate pe cele MENU_BOILER_PARAMETERS_NR tipuri de CT*/
extern unsigned char TEST_SEQUENCES[MENU_BOILER_PARAMETERS_NR][TESTS_NR];

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/
void SystemInit(void);
void SYS_State_Update(void); 
/*__SYSTEM_H__ */
#endif 