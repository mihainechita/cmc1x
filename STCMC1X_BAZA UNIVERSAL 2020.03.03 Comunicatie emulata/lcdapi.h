/*******************************************************************************
* Description : Fisier sursa pentru driverul de LCD
* File name   : lcdapi.h
* Author      : Ing. Mihai Nechita   
* Date	      : 10.12.2018          
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __lcdapi_h
#define __lcdapi_h
#include <string.h>
#include "system.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/




////Define-uri pentru RG12864
//
//#if( SYS_LCDMODEL == RG12864 )       //daca LCD este RG12864
//#define LCD_RS          C_LCD_CD
//#define LCD_WR          C_LCD_WR
//#define LCD_EN          C_LCD_CE
//#define LCD_CS1         C_LCD_RD
//#define LCD_CS2         C_LCD_RES
//     
//#define LCD_COLUMN_NR   64     
//#define LCD_PAGE_NR     8
//     
//#define LCD_PAGE0        0xB8                                   // linia cea mai de sus
//#define LCD_PAGE1        0xB9
//#define LCD_PAGE2        0xBA
//#define LCD_PAGE3        0xBB
//#define LCD_PAGE4        0xBC
//#define LCD_PAGE5        0xBD
//#define LCD_PAGE6        0xBE
//#define LCD_PAGE7        0xBF                                   // linia cea mai de jos
//
//#define LCD_COLUMN0  0x40
//#define LCD_COLUMN63 0x7F
//          
//
//
//#define LCD_ACTIVATE_PAGE1       {CLR(PORTA,LCD_CS1); SET(PORTA,LCD_CS2);}
//#define LCD_ACTIVATE_PAGE2       {SET(PORTA,LCD_CS1); CLR(PORTA,LCD_CS2);}
//#define LCD_ACTIVATE_PAGES       {CLR(PORTA,LCD_CS1); CLR(PORTA,LCD_CS2);}
//     
//#define SET_LCD_RS      CLR(PORTA,LCD_RS);
//#define CLR_LCD_RS      SET(PORTA,LCD_RS);
//
//#define SET_LCD_WR      CLR(PORTA,LCD_WR);
//#define CLR_LCD_WR      SET(PORTA,LCD_WR);
//
//#define SET_LCD_EN      CLR(PORTA,LCD_EN);
//#define CLR_LCD_EN      SET(PORTA,LCD_EN);
// 
//#endif	





/* Define pentru linia de clock _|     */
#define RISING_EDGE_CLOCK               { SET(PORTA,C_SCLK); asm ("nop"); CLR(PORTA,C_SCLK); }
#define FALLING_EDGE_CLOCK              { CLR(PORTA,C_SCLK); SET(PORTA,C_SCLK); }

/* Define pentru linia de REG_LCD (front crescator)    */    
#define STROBE_LCD 			{ SET(PORTB,REG_LCD); CLR(PORTB,REG_LCD); }

     
#define LCD_CONTROL_SET_COMMAND         CLR(PORTA,LCDAPI_CONTROL_CD)      // HIGH
#define LCD_CONTROL_SET_DATA            SET(PORTA,LCDAPI_CONTROL_CD)      // LOW

                                      
#define LCD_CONTROL_CE_ENABLE           SET(PORTA,LCDAPI_CONTROL_CE)      // LOW
#define LCD_CONTROL_CE_DISABLE          CLR(PORTA,LCDAPI_CONTROL_CE)     // HIGH

                                      
#define LCD_CONTROL_WR_ENABLE           SET(PORTA,LCDAPI_CONTROL_WR)      // LOW
#define LCD_CONTROL_WR_DISABLE          CLR(PORTA,LCDAPI_CONTROL_WR)      // HIGH


#define LCDAPI_CONTROL_CD                       C_LCD_CD
#define LCDAPI_CONTROL_CE                       C_LCD_CE
#define LCDAPI_CONTROL_WR                       C_LCD_WR




     
     
     

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
#if( SYS_LCDMODEL == RG12864 )       //daca LCD este RG12864
extern  unsigned char LCD_NumberTable[];
extern  unsigned char LCD_UpperCase_CharTable[];
extern  unsigned char LCD_LowerCase_CharTable[];
extern  unsigned char LCD_Special_CharTable[];    
#endif   
     





/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/


// Functii pentru RG24064A
#if( SYS_LCDMODEL == RG24064 )       //daca LCD este RG24064

void LCD_Reset(void);
void LCD_InitHW( void ) ;
void LCD_InitSW( void ) ;
void LCD_SendCommand(unsigned char command);
void LCD_SendData(unsigned char data);
void LCD_Set_Cursor(unsigned char X_pos, unsigned char Y_pos);
void LCD_ClearMem(void);
void LCD_Init_Text(unsigned char low, unsigned char high, unsigned char len);
void LCD_SendByte(signed char byte);
void LCD_TxtGoToXY(unsigned char col, unsigned char row);
void LCD_TxtClrScr(void);
void LCD_Init_CGRAM(unsigned char low, unsigned char high);
void LCD_Load_CGRAM(void);
void LCD_Set_ADP(unsigned int address);
void LCD_API_DefineCharacter(unsigned int address, unsigned char charCode, unsigned char * defChar);
#endif

#if( SYS_LCDMODEL == RG12864 )       //daca LCD este RG12864

// Functii pentru RG12864

void LCD_Init(void);
void LCD_SendCommand(uint8_t command);
void LCD_SendData(uint8_t data);
void LCD_HelloWorld(void);
void LCD_ClearRAM(void);
void LCD_PrintMotan(void);
void LCD_PrintChar(unsigned char *ptr);
void LCD_GotoXY(unsigned char page, unsigned char column);
#endif




#endif //__lcdapi_h
 



















