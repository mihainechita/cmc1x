/*******************************************************************************
* Description : Fisier sursa pentru driverul de LCD
* File name   : lcdapi.c
* Author      : Ing. Mihai Nechita   
* Date	      : 04.02.2019            
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "lcdapi.h"     // Header specific
#include "system.h"     // Includ define-urile hardware
#include "stdmacros.h"  // Includ macro-uri precum SET,CLR  
#include "lcd.h"
#include "swtimers.h"
#include "types.h"
#include "Comm595.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
extern uint16_t TXT_HOME;
extern uint16_t GF_HOME;

extern uint16_t TXT_LEN;
extern uint16_t GF_LEN;

#if( SYS_LCDMODEL == RG12864 )       //daca LCD este RG12864
unsigned char LCD_NumberTable[60]={
0x7E, 0x81, 0x81, 0x81, 0x7E, 0x00,  // Code for char 0
0x00, 0x82, 0xFF, 0x80, 0x00, 0x00,  // Code for char 1
0xC2, 0xA1, 0x91, 0x89, 0x86, 0x00,  // Code for char 2
0x42, 0x81, 0x89, 0x89, 0x76, 0x00,  // Code for char 3
0x18, 0x14, 0x12, 0xFF, 0x10, 0x00,  // Code for char 4
0x4F, 0x89, 0x89, 0x89, 0x71, 0x00,  // Code for char 5
0x7C, 0x8A, 0x89, 0x89, 0x70, 0x00,  // Code for char 6
0x01, 0xC1, 0x31, 0x0D, 0x03, 0x00,  // Code for char 7
0x76, 0x89, 0x89, 0x89, 0x76, 0x00,  // Code for char 8
0x0E, 0x91, 0x91, 0x51, 0x3E, 0x00,  // Code for char 9
};

unsigned char LCD_UpperCase_CharTable[]=
{
0xE0, 0x3C, 0x23, 0x23, 0x3C, 0xE0,  // Code for char A
0xFF, 0x89, 0x89, 0x89, 0x76, 0x00,  // Code for char B
0x3C, 0x42, 0x81, 0x81, 0x81, 0x81,  // Code for char C
0xFF, 0x81, 0x81, 0x81, 0x42, 0x3C,  // Code for char D
0xFF, 0x89, 0x89, 0x89, 0x81, 0x00,  // Code for char E
0xFF, 0x09, 0x09, 0x09, 0x09, 0x00,  // Code for char F
0x3C, 0x42, 0x81, 0x91, 0x91, 0xF1,  // Code for char G
0xFF, 0x08, 0x08, 0x08, 0x08, 0xFF,  // Code for char H
0x81, 0xFF, 0x81, 0x00, 0x00, 0x00,  // Code for char I
0x80, 0x81, 0x81, 0x7F, 0x00, 0x00,  // Code for char J
0xFF, 0x18, 0x24, 0x42, 0x81, 0x00,  // Code for char K
0xFF, 0x80, 0x80, 0x80, 0x00, 0x00,  // Code for char L
0xFF, 0x06, 0x0C, 0x0C, 0x06, 0xFF,  // Code for char M
0xFF, 0x03, 0x0C, 0x30, 0xC0, 0xFF,  // Code for char N
0x3C, 0x42, 0x81, 0x81, 0x81, 0x42,  // Code for char O
0xFF, 0x11, 0x11, 0x11, 0x0E, 0x00,  // Code for char P
0x3C, 0x42, 0x81, 0x81, 0x81, 0x42,  // Code for char Q
0xFF, 0x11, 0x11, 0x31, 0x4E, 0x80,  // Code for char R
0x86, 0x89, 0x89, 0x89, 0x71, 0x00,  // Code for char S
0x01, 0x01, 0xFF, 0x01, 0x01, 0x00,  // Code for char T
0x7F, 0x80, 0x80, 0x80, 0x80, 0x7F,  // Code for char U
0x07, 0x38, 0xC0, 0x38, 0x07, 0x00,  // Code for char V
0x7F, 0xC0, 0x3F, 0xC0, 0x7F, 0x00,  // Code for char W
0xC3, 0x24, 0x18, 0x24, 0xC3, 0x00,  // Code for char X
0x03, 0x0C, 0xF0, 0x0C, 0x03, 0x00,  // Code for char Y
0xC1, 0xA1, 0x99, 0x85, 0x83, 0x00,  // Code for char Z
};

unsigned char LCD_LowerCase_CharTable[]={
0x60, 0x94, 0x94, 0x94, 0xF8, 0x00,  // Code for char a
0xFF, 0x84, 0x84, 0x84, 0x78, 0x00,  // Code for char b
0x78, 0x84, 0x84, 0x84, 0x00, 0x00,  // Code for char c
0x78, 0x84, 0x84, 0x84, 0xFF, 0x00,  // Code for char d
0x78, 0x94, 0x94, 0x94, 0x58, 0x00,  // Code for char e
0xFF, 0x05, 0x05, 0x00, 0x00, 0x00,  // Code for char f
0x00, 0x8E, 0x8A, 0xFE, 0x00, 0x00,  // Code for char g
0xFF, 0x04, 0x04, 0x04, 0xF8, 0x00,  // Code for char h
0xFD, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char i
0x00, 0x80, 0x80, 0xFD, 0x00, 0x00,  // Code for char j
0xFF, 0x10, 0x28, 0x44, 0x80, 0x00,  // Code for char k
0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char l
0xFC, 0x04, 0x18, 0x04, 0xFC, 0x00,  // Code for char m
0xFC, 0x04, 0x04, 0x04, 0xF8, 0x00,  // Code for char n
0x78, 0x84, 0x84, 0x84, 0x78, 0x00,  // Code for char o
0x00, 0xFE, 0x12, 0x12, 0x1E, 0x00,  // Code for char p
0x00, 0x1E, 0x12, 0x12, 0xFE, 0x00,  // Code for char q
0xFC, 0x08, 0x04, 0x00, 0x00, 0x00,  // Code for char r
0x98, 0x94, 0xA4, 0x64, 0x00, 0x00,  // Code for char s
0x7F, 0x84, 0x84, 0x00, 0x00, 0x00,  // Code for char t
0x7C, 0x80, 0x80, 0x80, 0xFC, 0x00,  // Code for char u
0x0C, 0x30, 0xC0, 0x30, 0x0C, 0x00,  // Code for char v
0x3C, 0xC0, 0x3C, 0xC0, 0x3C, 0x00,  // Code for char w
0x84, 0x48, 0x30, 0x48, 0x84, 0x00,  // Code for char x
0x0C, 0x30, 0xC0, 0x30, 0x0C, 0x00,  // Code for char y
0xC4, 0xA4, 0x94, 0x8C, 0x00, 0x00,  // Code for char z   
};     

unsigned char LCD_Special_CharTable[]=
{
0x00, 0x00, 0x00, 0x07, 0x05, 0x07,  // Code for char Celsius degree
0x00, 0xBF, 0x00, 0x00, 0x00, 0x00,  // Code for char !
0x03, 0x00, 0x03, 0x00, 0x00, 0x00,  // Code for char "
0x20, 0xE4, 0x3C, 0xE7, 0x3C, 0x27,  // Code for char #
0x8C, 0x92, 0xFF, 0x92, 0x62, 0x00,  // Code for char $
0x06, 0x09, 0x09, 0xC6, 0x30, 0x0C,  // Code for char %
0x76, 0x89, 0x89, 0x96, 0x60, 0x58,  // Code for char &
0x03, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char '
0x7C, 0x83, 0x00, 0x00, 0x00, 0x00,  // Code for char (
0x00, 0x83, 0x7C, 0x00, 0x00, 0x00,  // Code for char )
0x05, 0x02, 0x0F, 0x02, 0x05, 0x00,  // Code for char *
0x10, 0x10, 0x10, 0xFE, 0x10, 0x10,  // Code for char +
0x00, 0xC0, 0x00, 0x00, 0x00, 0x00,  // Code for char ,
0x10, 0x10, 0x10, 0x00, 0x00, 0x00,  // Code for char -
0x00, 0xC0, 0x00, 0x00, 0x00, 0x00,  // Code for char .
0x80, 0x7C, 0x03, 0x00, 0x00, 0x00,  // Code for char /   
0x00, 0xCC, 0x00, 0x00, 0x00, 0x00,  // Code for char :
0x00, 0xCC, 0x00, 0x00, 0x00, 0x00,  // Code for char ;
0x00, 0x10, 0x28, 0x28, 0x44, 0x44,  // Code for char <
0x28, 0x28, 0x28, 0x28, 0x28, 0x28,  // Code for char =
0x00, 0x82, 0x44, 0x44, 0x28, 0x28,  // Code for char >
0x01, 0xB1, 0x09, 0x06, 0x00, 0x00,  // Code for char ?
0x7C, 0x82, 0x39, 0x45, 0x45, 0x7D,  // Code for char @
0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,  // Code for char ]
0x08, 0x04, 0x02, 0x01, 0x02, 0x04,  // Code for char ^
0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char _
0x00, 0x00, 0x01, 0x00, 0x00, 0x00,  // Code for char `
0x10, 0x10, 0xEF, 0x00, 0x00, 0x00,  // Code for char {
0x00, 0xFF, 0x00, 0x00, 0x00, 0x00,  // Code for char |
0x00, 0xEF, 0x10, 0x10, 0x00, 0x00,  // Code for char }
0x30, 0x08, 0x08, 0x10, 0x20, 0x20,  // Code for char ~
};





unsigned char H[]={0x7F,0x08,0x08,0x08,0x7F,0x00};
unsigned char E[]={0x7F,0x49,0x49,0x49,0x41,0x00};
unsigned char L[]={0x7F,0x40,0x40,0x40,0x40,0x00};
unsigned char O[]={0x3E,0x41,0x41,0x41,0x3E,0x00};

unsigned char MO[]={0xE0, 0x38, 0x0E, 0x02, 0x01, 0x03, 0x1C, 0x60, 0x18, 0x04, 0x38, 0xE0};
unsigned char TAN[]={0x07, 0x0C, 0x38, 0x60, 0xC0, 0x80, 0x80, 0xC0, 0x60, 0x38, 0x0C, 0x07};

unsigned char M_UP[]={ 0xFF, 0xFF, 0x0C, 0x18, 0x30, 0x60, 0x60, 0x30, 0x18, 0x0C, 0xFF, 0xFF};
unsigned char M_DOWN[]={ 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF};

unsigned char T_UP[]={ 0x07, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x07};
unsigned char T_DOWN[]={ 0x00, 0x00, 0x00, 0x00, 0x80, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x00, 0x00};

unsigned char A_UP[]={ 0xF8, 0xFC, 0x86, 0x83, 0x83, 0x83, 0x83, 0x83, 0x83, 0x86, 0xFC, 0xF8};
unsigned char A_DOWN[]={0xFF, 0xFF, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0xFF, 0xFF};

unsigned char N_UP[]={0xFF, 0xFF, 0x18, 0x30, 0x60, 0xC0, 0x80, 0x00, 0x00, 0x00, 0xFF, 0xFF};
unsigned char N_DOWN[]={0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x06, 0x0C, 0xFF, 0xFF};
#endif






/*------------------------------------------------------------------------------
* Private (static) variables
*----------------------------------------------------------------------------*/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/


/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/


#if( SYS_LCDMODEL == RG12864 )       //daca LCD este RG12864
void LCD_Init()
{
    LCD_ACTIVATE_PAGES;
    LCD_SendCommand(0x3F);
    LCD_SendCommand(0xC0);
    LCD_SendCommand(0x40);
    BKL_ON;
}

void LCD_SendCommand(uint8_t command)
{
    Comm595_Send1B_LCD(command); 
    SET_LCD_EN;
    CLR_LCD_RS;
    CLR_LCD_WR;
    CLR_LCD_EN;   
}



void LCD_SendData(uint8_t data)
{
    Comm595_Send1B_LCD(data);  
    SET_LCD_EN;
    SET_LCD_RS;
    CLR_LCD_WR; 
    CLR_LCD_EN;       
}


void LCD_PrintChar(unsigned char *p)
{
    unsigned char i;
    LCD_SendCommand(0xC0);
    LCD_GotoXY(0,0);
    for(i=0;i<6;i++)
        LCD_SendData(*(p+i));
}

void LCD_Print12(unsigned char *ptr)
{
    unsigned char i;
    for(i=0;i<12;i++)
        LCD_SendData(ptr[i]);
}

void LCD_GotoXY(unsigned char page, unsigned char column)
{
       LCD_SendCommand(LCD_COLUMN0+column);             //setare coloana [0-63]
       LCD_SendCommand(LCD_PAGE0+page);                 //setare pagina [0-7]
        
}



void LCD_ClearRAM(void)
    {
        LCD_ACTIVATE_PAGES;
        
        LCD_SendCommand(LCD_COLUMN0);   //set column 0
        
        
        for(unsigned char LCD_Page_Counter=0; LCD_Page_Counter < LCD_PAGE_NR ; LCD_Page_Counter++ )
            {
                LCD_SendCommand(LCD_PAGE0+LCD_Page_Counter);     //setare pagina
                
                for(unsigned char LCD_ColumnCounter=0; LCD_ColumnCounter < LCD_COLUMN_NR ; LCD_ColumnCounter++)
                    {
                        LCD_SendData(0x00); 
                    }
            }      
    }


void LCD_PrintMotan(void)
    {
        LCD_ACTIVATE_PAGES;
//        LCD_SendCommand(0xC0);
        LCD_SendCommand(LCD_COLUMN0);
        LCD_SendCommand(LCD_PAGE0);
      
        LCD_Print12(M_UP);
        LCD_SendCommand(LCD_COLUMN0);
        LCD_SendCommand(LCD_PAGE1);
        LCD_Print12(M_DOWN);
        
        LCD_SendCommand(LCD_COLUMN0+12);
        LCD_SendCommand(LCD_PAGE0);    
        LCD_Print12(MO);
        LCD_SendCommand(LCD_COLUMN0+12);
        LCD_SendCommand(LCD_PAGE1);
        LCD_Print12(TAN);
        
        LCD_SendCommand(LCD_COLUMN0+24);
        LCD_SendCommand(LCD_PAGE0);
        LCD_Print12(T_UP);
        LCD_SendCommand(LCD_COLUMN0+24);
        LCD_SendCommand(LCD_PAGE1);
        LCD_Print12(T_DOWN);
        
        LCD_SendCommand(LCD_COLUMN0+36);
        LCD_SendCommand(LCD_PAGE0);
        LCD_Print12(A_UP);
        LCD_SendCommand(LCD_COLUMN0+36);
        LCD_SendCommand(LCD_PAGE1);
        LCD_Print12(A_DOWN);
        
        LCD_SendCommand(LCD_COLUMN0+49);
        LCD_SendCommand(LCD_PAGE0);
        LCD_Print12(N_UP);
        LCD_SendCommand(LCD_COLUMN0+49);
        LCD_SendCommand(LCD_PAGE1);
        LCD_Print12(N_DOWN);
        
        
    }

void LCD_HelloWorld(void)
    {
        LCD_ACTIVATE_PAGES;
        LCD_SendCommand(0xC0);
        LCD_SendCommand(LCD_COLUMN0);
        LCD_SendCommand(LCD_PAGE0);
      
        LCD_PrintChar(H);
        LCD_PrintChar(E);
        LCD_PrintChar(L);
        LCD_PrintChar(L);
        LCD_PrintChar(O);
        
    }


#endif


#if( SYS_LCDMODEL == RG24064 )       //daca LCD este RG12864

//-------------------------------------------------------------------------------------------
//Hardware init routine - puts the controls signals into inactive state
//-------------------------------------------------------------------------------------------

void LCD_InitHW( void )
{
	CLR ( LCD_PORT, LCD_PIN_RES );	 
	CLR ( LCD_PORT, LCD_PIN_RD );	 
	CLR ( LCD_PORT, LCD_PIN_WR );	 
	CLR ( LCD_PORT, LCD_PIN_CD );	 	
        SET ( LCD_PORT, LCD_PIN_CE );
        BKL_ON;
}

//-------------------------------------------------------------------------------------------
//Software init routine - 
//-------------------------------------------------------------------------------------------

void LCD_InitSW( void ) 
{
	LCD_Reset();
	LCD_Init_Text(0x00, 0x00, 0x28);  
//        LCD_Init_CGRAM(0x01,0x00);
	//LCD_Graphic(0x00, 0x10, 0x28);                                          
	LCD_SendCommand(LCD_OR_MODE);                                           //OR mode
        LCD_SendCommand(LCD_TEXT_ON_GRAPHIC_OFF);                               //Text on, graphic on
	LCD_SendCommand(LCD_CURSOR_1LINE);                                                  
	LCD_TxtClrScr();
}

//-------------------------------------------------------------------------------------------
//Reset routine - reinitialize the LCD controller
//-------------------------------------------------------------------------------------------

void LCD_Reset(void)
{
	SET ( LCD_PORT, LCD_PIN_RES );    
	__delay_cycles(1000 );
	CLR ( LCD_PORT, LCD_PIN_RES );
}


//-------------------------------------------------------------------------------------------
//LCD_Command: send an instruction code to lcd
//-------------------------------------------------------------------------------------------

void LCD_SendCommand(uint8_t command)
{
        Comm595_Send1B_LCD(command);                      //test mode
	SET ( LCD_PORT, LCD_PIN_WR );	 
	CLR ( LCD_PORT, LCD_PIN_WR );	 	
}


//-------------------------------------------------------------------------------------------
//LCD Send data routine
//-------------------------------------------------------------------------------------------

void LCD_SendData(uint8_t data)
{
        Comm595_Send1B_LCD(data);                       //test mode
	SET ( LCD_PORT, LCD_PIN_CD );	 		
	SET ( LCD_PORT, LCD_PIN_WR );	 
	CLR ( LCD_PORT, LCD_PIN_WR );	 
	CLR ( LCD_PORT, LCD_PIN_CD );	 		
}

//-------------------------------------------------------------------------------------------
//LCD clear
//-------------------------------------------------------------------------------------------

void LCD_ClearMem(void)
{
  uint16_t a;
  LCD_SendData(0x00);
  LCD_SendData(0x00);
  LCD_SendCommand(0x24);
  for(a=0; a<0x1FFF; a++)
  {
    LCD_SendData(0);
    LCD_SendCommand(0xc0);
  }
}

//-------------------------------------------------------------------------------------------
//Configure LCD in text mode
//-------------------------------------------------------------------------------------------

void LCD_Init_Text(uint8_t low, uint8_t high, uint8_t len)
{
  TXT_HOME=((uint16_t)high)<<8+(uint16_t)low;
  TXT_LEN=len;
  LCD_SendData(low);
  LCD_SendData(high);
  LCD_SendCommand(0x40);
  LCD_SendData(len);
  LCD_SendData(0x00);
  LCD_SendCommand(0x41);
}

//-------------------------------------------------------------------------------------------
//Configure LCD in graphic mode
//-------------------------------------------------------------------------------------------

void LCD_Init_Graphic(uint8_t low, uint8_t high, uint8_t len)
{
  GF_HOME=((uint16_t)high)<<8+(uint16_t)low;
  GF_LEN=len;
  LCD_SendData(low);
  LCD_SendData(high);
  LCD_SendCommand(0x42);
  LCD_SendData(len);
  LCD_SendData(0x00);
  LCD_SendCommand(0x43);
}

//-------------------------------------------------------------------------------------------
//LCD send byte in text mode
//-------------------------------------------------------------------------------------------

void LCD_SendByte(signed char c)
{
  c = c-0x20;
  if ( c < 0 ) c = 0;	
  LCD_SendData(c);
  LCD_SendCommand(0xc0);
}

//------------------------------------------------------------------------------------------
//Set cursor position in text mode
//------------------------------------------------------------------------------------------

void LCD_TxtGoToXY(uint8_t row, uint8_t col)
{
  uint16_t addr;
  uint8_t low,high;
  addr=TXT_HOME+(((uint16_t)row)*TXT_LEN)+((uint16_t)col);
  low=((uint8_t)(addr%256));
  high=((uint8_t)(addr>>8));
  LCD_SendData(low);
  LCD_SendData(high);
  LCD_SendCommand(0x24);
}

//------------------------------------------------------------------------------------------
//clear LCD in text mode
//------------------------------------------------------------------------------------------

void LCD_TxtClrScr(void){
	uint8_t i;
	LCD_TxtGoToXY(0,0);
	for(i=0;i<220;i++)
		LCD_SendByte(' ');
	for(i=0;i<100;i++)
		LCD_SendByte(' ');	
}


void LCD_Set_ADP(unsigned int address)
{
    LCD_SendData(address & 0xFF); // first send low byte
    LCD_SendData(address>>8); // then send high byte
    LCD_SendCommand(LCD_SET_ADDR_POINTER);
}




#endif































