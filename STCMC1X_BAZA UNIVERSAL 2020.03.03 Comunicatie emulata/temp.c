/*******************************************************************************
* Description : Fisier sursa pentru determinare temperatura
* File name   : temp.c
* Author      : Birou Proiectare Automatizari-Soft             
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "headers.h"         // 


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/
// NTC sensor table constants
#define TEMP_NTCTABLE_LEN             (unsigned char)157                          // Table length
#define TEMP_ZERODEGREES_OFFSET       (unsigned char) 46                          // Offset of 0C degrees in table - sub

// Temperature sensor table max value (greater max value for 10-bit)
#define TEMP_NTCTABLE_MAXVAL       0x03FF
#if TEMP_NTCTABLE_MAXVAL < 0x03FF
    #error "temp.c: Max value from NTC table (value of last element) must be\
                    equal-to or greater-than maximum 10-bit ADC value (0x03FF) !"
#endif
/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char SYS_SMQuantitiesStatus;
unsigned char SYS_SMQuantitiesValx1; // Non-scaled values (scale x1): Used to store integer part of temps (flow and pressure locations are dummy)!
unsigned int  SYS_SMQuantitiesValxS; // Scaled values (most @ scale x10)
/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
// Table storing temperature sensor (NTC) operating curve
// -used to convert ADC values (voltages) in temperatures
// -stores ADC values for temperatures in range [-46...+109] C degrees
// -one Celsius degree at 40C = 7 units = 34mV
static __flash const unsigned int TEMP_NtcTable[ 1 ][ TEMP_NTCTABLE_LEN ] =
{
//    {   // Brahma/Tasseron/ChuangZhou sensor (beta = 3435)
//        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x0009) */
//        /*           -x9     -x8     -x7     -x6     -x5     -x4     -x3     -x2     -x1     -x0 */
//        /* -4x */                                 0x0009, 0x000A, 0x000B, 0x000B, 0x000C, 0x000D,
//        /* -3x */ 0x000E, 0x000F, 0x0010, 0x0011, 0x0012, 0x0013, 0x0014, 0x0015, 0x0017, 0x0018,
//        /* -2x */ 0x0019, 0x001B, 0x001C, 0x001E, 0x0020, 0x0022, 0x0023, 0x0025, 0x0027, 0x0029,
//        /* -1x */ 0x002C, 0x002E, 0x0030, 0x0033, 0x0035, 0x0038, 0x003B, 0x003E, 0x0041, 0x0044,
//        /* -0x */ 0x0047, 0x004A, 0x004E, 0x0051, 0x0055, 0x0059, 0x005D, 0x0061, 0x0065,
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */
//        /*  0x */ 0x0069, 0x006E, 0x0072, 0x0077, 0x007C, 0x0081, 0x0086, 0x008B, 0x0090, 0x0096,
//        /*  1x */ 0x009B, 0x00A1, 0x00A7, 0x00AD, 0x00B3, 0x00B9, 0x00BF, 0x00C5, 0x00CC, 0x00D3,
//        /*  2x */ 0x00D9, 0x00E0, 0x00E7, 0x00EE, 0x00F5, 0x00FD, 0x0104, 0x010B, 0x0113, 0x011A,
//        /*  3x */ 0x0122, 0x012A, 0x0132, 0x0139, 0x0141, 0x0149, 0x0151, 0x0159, 0x0161, 0x0169,
//        /*  4x */ 0x0172, 0x017A, 0x0182, 0x018A, 0x0192, 0x019A, 0x01A3, 0x01AB, 0x01B3, 0x01BB,
//        /*  5x */ 0x01C3, 0x01CB, 0x01D3, 0x01DB, 0x01E3, 0x01EB, 0x01F3, 0x01FB, 0x0203, 0x020A,
//        /*  6x */ 0x0212, 0x021A, 0x0221, 0x0229, 0x0230, 0x0238, 0x023F, 0x0246, 0x024D, 0x0254,
//        /*  7x */ 0x025B, 0x0262, 0x0269, 0x026F, 0x0276, 0x027D, 0x0283, 0x0289, 0x0290, 0x0296,
//        /*  8x */ 0x029C, 0x02A2, 0x02A8, 0x02AD, 0x02B3, 0x02B9, 0x02BE, 0x02C4, 0x02C9, 0x02CE,
//        /*  9x */ 0x02D3, 0x02D8, 0x02DD, 0x02E2, 0x02E7, 0x02EC, 0x02F0, 0x02F5, 0x02F9, 0x02FE,
//        /* 10x */ 0x0302, 0x0306, 0x030B, 0x030F, 0x0313, 0x0316, 0x031A, 0x031E, 0x0322, 0x0325,
//        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x0325) */
//    },
    {   // Kober sensor (beta = 3380)
        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x000A) */
        /*           -x9     -x8     -x7     -x6     -x5     -x4     -x3     -x2     -x1     -x0 */
        /* -4x */                                 0x000A, 0x000B, 0x000B, 0x000C, 0x000D, 0x000E,
        /* -3x */ 0x000F, 0x0010, 0x0011, 0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0018, 0x0019,
        /* -2x */ 0x001A, 0x001C, 0x001E, 0x001F, 0x0021, 0x0023, 0x0025, 0x0027, 0x0029, 0x002B,
        /* -1x */ 0x002D, 0x002F, 0x0032, 0x0034, 0x0037, 0x003A, 0x003C, 0x003F, 0x0042, 0x0045,
        /* -0x */ 0x0049, 0x004C, 0x004F, 0x0053, 0x0057, 0x005A, 0x005E, 0x0062, 0x0067,
        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */
        /*  0x */ 0x006B, 0x006F, 0x0074, 0x0078, 0x007D, 0x0082, 0x0087, 0x008C, 0x0091, 0x0097,
        /*  1x */ 0x009C, 0x00A2, 0x00A8, 0x00AE, 0x00B4, 0x00BA, 0x00C0, 0x00C6, 0x00CD, 0x00D3,
        /*  2x */ 0x00DA, 0x00E1, 0x00E7, 0x00EE, 0x00F5, 0x00FD, 0x0104, 0x010B, 0x0112, 0x011A,
        /*  3x */ 0x0121, 0x0129, 0x0131, 0x0138, 0x0140, 0x0148, 0x0150, 0x0158, 0x0160, 0x0168,
        /*  4x */ 0x016F, 0x0177, 0x017F, 0x0187, 0x0190, 0x0198, 0x01A0, 0x01A8, 0x01B0, 0x01B8,
        /*  5x */ 0x01C0, 0x01C8, 0x01CF, 0x01D7, 0x01DF, 0x01E7, 0x01EF, 0x01F6, 0x01FE, 0x0206,
        /*  6x */ 0x020D, 0x0215, 0x021C, 0x0224, 0x022B, 0x0232, 0x0239, 0x0240, 0x0247, 0x024E,
        /*  7x */ 0x0255, 0x025C, 0x0263, 0x0269, 0x0270, 0x0276, 0x027D, 0x0283, 0x0289, 0x028F,
        /*  8x */ 0x0295, 0x029B, 0x02A1, 0x02A7, 0x02AD, 0x02B2, 0x02B8, 0x02BD, 0x02C2, 0x02C8,
        /*  9x */ 0x02CD, 0x02D2, 0x02D7, 0x02DC, 0x02E1, 0x02E5, 0x02EA, 0x02EF, 0x02F3, 0x02F7,
        /* 10x */ 0x02FC, 0x0300, 0x0304, 0x0308, 0x030C, 0x0310, 0x0314, 0x0318, 0x031B, 0x031F,
        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x031F) */
    }
//    ,
//    {   // Other sensor (beta = 3530)
//        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x0008) */
//        /*           -x9     -x8     -x7     -x6     -x5     -x4     -x3     -x2     -x1     -x0 */
//        /* -4x */                                 0x0008, 0x0009, 0x000A, 0x000A, 0x000B, 0x000C,
//        /* -3x */ 0x000D, 0x000E, 0x000E, 0x000F, 0x0010, 0x0011, 0x0013, 0x0014, 0x0015, 0x0016,
//        /* -2x */ 0x0018, 0x0019, 0x001B, 0x001C, 0x001E, 0x0020, 0x0021, 0x0023, 0x0025, 0x0027,
//        /* -1x */ 0x0029, 0x002C, 0x002E, 0x0030, 0x0033, 0x0036, 0x0038, 0x003B, 0x003E, 0x0041,
//        /* -0x */ 0x0044, 0x0048, 0x004B, 0x004F, 0x0052, 0x0056, 0x005A, 0x005E, 0x0062,
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */
//        /*  0x */ 0x0067, 0x006B, 0x0070, 0x0074, 0x0079, 0x007E, 0x0083, 0x0088, 0x008E, 0x0093,
//        /*  1x */ 0x0099, 0x009F, 0x00A5, 0x00AB, 0x00B1, 0x00B7, 0x00BD, 0x00C4, 0x00CB, 0x00D2,
//        /*  2x */ 0x00D8, 0x00DF, 0x00E7, 0x00EE, 0x00F5, 0x00FD, 0x0104, 0x010C, 0x0113, 0x011B,
//        /*  3x */ 0x0123, 0x012B, 0x0133, 0x013B, 0x0143, 0x014C, 0x0154, 0x015C, 0x0164, 0x016D,
//        /*  4x */ 0x0175, 0x017D, 0x0186, 0x018E, 0x0197, 0x019F, 0x01AB, 0x01B0, 0x01B8, 0x01C1,
//        /*  5x */ 0x01C9, 0x01D1, 0x01DA, 0x01E2, 0x01EA, 0x01F2, 0x01FB, 0x0203, 0x020B, 0x0212,
//        /*  6x */ 0x021A, 0x0222, 0x022A, 0x0232, 0x0239, 0x0241, 0x0248, 0x024F, 0x0257, 0x025E,
//        /*  7x */ 0x0265, 0x026C, 0x0273, 0x027A, 0x0280, 0x0287, 0x028D, 0x0294, 0x029A, 0x02A0,
//        /*  8x */ 0x02A7, 0x02AD, 0x02B2, 0x02B8, 0x02BE, 0x02C4, 0x02C9, 0x02CF, 0x02D4, 0x02D9,
//        /*  9x */ 0x02DE, 0x02E4, 0x02E8, 0x02ED, 0x02F2, 0x02F7, 0x02FB, 0x0300, 0x0304, 0x0309,
//        /* 10x */ 0x030D, 0x0311, 0x0315, 0x0319, 0x031D, 0x0321, 0x0325, 0x0329, 0x032C, 0x0330,
//        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x0330) */
//    },
//    {   // Other sensor - Tasseron C33 (beta = 3760)
//        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x0007) */
//        /*           -x9     -x8     -x7     -x6     -x5     -x4     -x3     -x2     -x1     -x0 */
//        /* -4x */                                 0x0007, 0x0007, 0x0008, 0x0008, 0x0009, 0x0009,
//        /* -3x */ 0x000A, 0x000B, 0x000C, 0x000D, 0x000D, 0x000E, 0x000F, 0x0010, 0x0012, 0x0013,
//        /* -2x */ 0x0014, 0x0015, 0x0017, 0x0018, 0x001A, 0x001B, 0x001D, 0x001F, 0x0020, 0x0022,
//        /* -1x */ 0x0024, 0x0026, 0x0029, 0x002B, 0x002D, 0x0030, 0x0033, 0x0035, 0x0038, 0x003B,
//        /* -0x */ 0x003E, 0x0041, 0x0045, 0x0048, 0x004C, 0x0050, 0x0054, 0x0058, 0x005C,
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */
//        /*  0x */ 0x0060, 0x0065, 0x0069, 0x006E, 0x0073, 0x0078, 0x007D, 0x0083, 0x0088, 0x008E,
//        /*  1x */ 0x0094, 0x009A, 0x00A0, 0x00A6, 0x00AD, 0x00B3, 0x00BA, 0x00C1, 0x00C8, 0x00CF,
//        /*  2x */ 0x00D6, 0x00DE, 0x00E5, 0x00ED, 0x00F5, 0x00FD, 0x0105, 0x010D, 0x0115, 0x011D,
//        /*  3x */ 0x0126, 0x012E, 0x0137, 0x0140, 0x0148, 0x0151, 0x015A, 0x0163, 0x016C, 0x0175,
//        /*  4x */ 0x017E, 0x0187, 0x0190, 0x0199, 0x01A2, 0x01AB, 0x01B4, 0x01BD, 0x01C6, 0x01CF,
//        /*  5x */ 0x01D8, 0x01E1, 0x01EA, 0x01F2, 0x01FB, 0x0204, 0x020D, 0x0215, 0x021E, 0x0226,
//        /*  6x */ 0x022E, 0x0237, 0x023F, 0x0247, 0x024F, 0x0257, 0x025E, 0x0266, 0x026E, 0x0275,
//        /*  7x */ 0x027C, 0x0284, 0x028B, 0x0292, 0x0299, 0x029F, 0x02A6, 0x02AD, 0x02B3, 0x02B9,
//        /*  8x */ 0x02C0, 0x02C6, 0x02CC, 0x02D2, 0x02D8, 0x02DD, 0x02E3, 0x02E8, 0x02EE, 0x02F3,
//        /*  9x */ 0x02F8, 0x02FD, 0x0302, 0x0307, 0x030B, 0x0310, 0x0315, 0x0319, 0x031D, 0x0322,
//        /* 10x */ 0x0326, 0x032A, 0x032E, 0x0332, 0x0335, 0x0339, 0x033D, 0x0340, 0x0344, 0x0347,
//        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x0347) */
//    },
//    {   // Other sensor - Tasseron C33 (beta = 3977)
//        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x0005) */
//        /*           -x9     -x8     -x7     -x6     -x5     -x4     -x3     -x2     -x1     -x0 */
//        /* -4x */                                 0x0005, 0x0006, 0x0006, 0x0007, 0x0007, 0x0008,
//        /* -3x */ 0x0008, 0x0009, 0x000A, 0x000A, 0x000B, 0x000C, 0x000D, 0x000E, 0x000F, 0x0010,
//        /* -2x */ 0x0011, 0x0012, 0x0013, 0x0015, 0x0016, 0x0018, 0x0019, 0x001B, 0x001C, 0x001E,
//        /* -1x */ 0x0020, 0x0022, 0x0024, 0x0026, 0x0029, 0x002B, 0x002E, 0x0030, 0x0033, 0x0036,
//        /* -0x */ 0x0039, 0x003C, 0x003F, 0x0043, 0x0046, 0x004A, 0x004E, 0x0052, 0x0056,
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */
//        /*  0x */ 0x005B, 0x005F, 0x0064, 0x0068, 0x006D, 0x0073, 0x0078, 0x007D, 0x0083, 0x0089,
//        /*  1x */ 0x008F, 0x0095, 0x009B, 0x00A2, 0x00A9, 0x00AF, 0x00B6, 0x00BE, 0x00C5, 0x00CC,
//        /*  2x */ 0x00D4, 0x00DC, 0x00E4, 0x00EC, 0x00F4, 0x00FD, 0x0105, 0x010E, 0x0116, 0x011F,
//        /*  3x */ 0x0128, 0x0131, 0x013A, 0x0144, 0x014D, 0x0156, 0x0160, 0x0169, 0x0173, 0x017C,
//        /*  4x */ 0x0186, 0x0190, 0x0199, 0x01A3, 0x01AC, 0x01B6, 0x01C0, 0x01C9, 0x01D3, 0x01DC,
//        /*  5x */ 0x01E6, 0x01EF, 0x01F9, 0x0202, 0x020B, 0x0214, 0x021D, 0x0226, 0x022F, 0x0238,
//        /*  6x */ 0x0241, 0x0249, 0x0252, 0x025A, 0x0263, 0x026B, 0x0273, 0x027B, 0x0283, 0x028A,
//        /*  7x */ 0x0292, 0x0299, 0x02A1, 0x02A8, 0x02AF, 0x02B6, 0x02BD, 0x02C3, 0x02CA, 0x02D0,
//        /*  8x */ 0x02D6, 0x02DD, 0x02E3, 0x02E8, 0x02EE, 0x02F4, 0x02F9, 0x02FF, 0x0304, 0x0309,
//        /*  9x */ 0x030E, 0x0313, 0x0318, 0x031D, 0x0321, 0x0326, 0x032A, 0x032F, 0x0333, 0x0337,
//        /* 10x */ 0x033B, 0x033F, 0x0343, 0x0346, 0x034A, 0x034D, 0x0351, 0x0354, 0x0358, 0x035B,
//        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x035B) */
//    },
//    {   // EPCOS - kober C34 (beta = 3450) - high range 
//        0x0000, /* DUMMY value: used to "catch" cases where the ADC value is less than the minimum value (0x0009) */
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */        
//        /*  4x */                                         0x019A, 0x01A3, 0x01AB, 0x01B3, 0x01BB,
//        /*  5x */ 0x01C3, 0x01CB, 0x01D3, 0x01DB, 0x01E3, 0x01EB, 0x01F3, 0x01FB, 0x0203, 0x020A,
//        /*  6x */ 0x0212, 0x021A, 0x0221, 0x0229, 0x0230, 0x0238, 0x023F, 0x0246, 0x024D, 0x0254,
//        /*  7x */ 0x025B, 0x0262, 0x0269, 0x026F, 0x0276, 0x027D, 0x0283, 0x0289, 0x0290, 0x0296,
//        /*  8x */ 0x029C, 0x02A2, 0x02A8, 0x02AD, 0x02B3, 0x02B9, 0x02BE, 0x02C4, 0x02C9, 0x02CE,
//        /*            x0      x1      x2      x3      x4      x5      x6      x7      x8      x9 */        
//        /*  9x */ 0x02D3, 0x02D8, 0x02DD, 0x02E2, 0x02E7, 0x02EC, 0x02F0, 0x02F5, 0x02F9, 0x02FE,
//        /* 10x */ 0x0304, 0x0308, 0x030C, 0x0310, 0x0314, 0x0318, 0x031C, 0x0320, 0x0323, 0x0327,
//        /* 11x */ 0x032B, 0x032E, 0x0331, 0x0335, 0x0338, 0x033B, 0x033E, 0x0341, 0x0344, 0x0347,
//        /* 12x */ 0x034A, 0x034D, 0x0350, 0x0352, 0x0355, 0x0358, 0x035A, 0x035D, 0x035F, 0x0362,
//        /* 13x */ 0x0362, 0x0366, 0x0368, 0x036B, 0x036D, 0x036F, 0x0371, 0x0373, 0x0375, 0x0377,
//        /* 14x */ 0x0379, 0x037B, 0x037C, 0x037E, 0x0380, 0x0382, 0x0383, 0x0385, 0x0387, 0x0388,
//        /* 15x */ 0x038A, 0x038B, 0x038D, 0x038E, 0x0390, 0x0391, 0x0392, 0x0394, 0x0395, 0x0396,
//        /* 16x */ 0x0398, 0x0399, 0x039A, 0x039B, 0x039C, 0x039D, 0x039F, 0x03A0, 0x03A1, 0x03A2,
//        /* 17x */ 0x03A3, 0x03A4, 0x03A5, 0x03A6, 0x03A7, 0x03A8, 0x03A9, 0x03A9, 0x03AA, 0x03AB,
//        /* 18x */ 0x03AC, 0x03AD, 0x03AE, 0x03AE, 0x03AF, 0x03B0, 0x03B1, 0x03B2, 0x03B2, 0x03B3,
//        /* 19x */ 0x03B4, 0x03B4, 0x03B5, 0x03B6, 0x03B6, 0x03B7, 0x03B8, 0x03B8, 0x03B9, 0x03B9,
//        TEMP_NTCTABLE_MAXVAL /* DUMMY value: used to "catch" cases when ADC value is greater than max value (0x035B) */
//    }
};



/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/********************************************************************
 * Name       : TEMP_ComputeTemperature
 * Parameters :-sensorChannel=ADC channel of sensor for which
 *            : the temperature will be computed; used as index
 *            : in the ADC-average buffer;
 *            :-tempIndex=index in quantities vectors where the
 *            : temperature status, error and value will be stored.
 *            :-minTemp=used to check if temperature is below normal
 *            :-maxTemp=used to check if temperature is above normal
 *            :-sensorPresent= indicates if sensor is present !!!
 * Return     : None.
 * Description: Converts voltage (ADC-value) measured by sensor
 *            : to temperature (Celsius degrees).
 * Note       :-ADC-value is 10-bit.
 *            :-Temp status is automatically considered OK if
 *            : the sensor is broken in order to avoid triggering
 *            : false errors (errors triggered by side-effects) !
 *            : Only if the sensor is operational, a trustworthy
 *            : analysis of the temperature status can be performed !
 *            : Could also be seen as a priority given to sensor
 *            : errors over temperature-status errors !
 *            :-Temperature is computed even if sensor-present
 *            : parameter is set to FALSE (useful sometimes) !
 * Warning    :-This function assumes quantities vectors have same
 *            : length and same organization !
 * timp de executie : Optimizari NONE min 112 us  max 116 us
 *                  : Optimizari HIGH min 51 us   max 62 us
 ********************************************************************/
static void TEMP_ComputeTemperature( unsigned char sensorChannel,
                                     unsigned char tempIndex,
                                     unsigned char minTemp,
                                     unsigned char maxTemp,
                                     unsigned char sensorPresent )
{
      signed char l_temp=0;        // Reminder: Sign needed (temp = index - zero_degrees_offset !)
    unsigned char l_temp10ths=0;
      signed  int l_tempValuex10=0;// Note: Sign needed for exterior temp. case !
    unsigned char l_indexLo=0;
    unsigned char l_indexHi=0;
    unsigned char l_indexMid=0;
    unsigned char l_range=0;
    unsigned int  l_compValue=0;
    unsigned int  l_adcValue=0;
    unsigned int  l_adcValueLo=0;
    unsigned int  l_adcValueHi=0;
    unsigned int  l_adcPrevValue=0;
    unsigned char l_sensorError=0;
    unsigned char l_tempStatus=0;
    unsigned char l_sensorModel=0; 

    // Check if ADC average is ready 
    // Note: Function ADC_IsAverageReady will automatically clear channel average-ready flag when called !
    if( TRUE == ADC_IsAverageReady( sensorChannel ) )
    {
        //1 Read sensor ADC value (10-bit ADC value, averaged on 500ms time-window)
      
        l_adcValue     = ADC_CurrentAverages [ sensorChannel ]; // Current  value
        l_adcPrevValue = ADC_PreviousAverages[ sensorChannel ]; // Previous value

        //2 Identify/Get temperature sensor model
        l_sensorModel = tempIndex;

        //3 Search temperature using sensor operating table (binary-search based implementation)
        // ! Warning !
        // -Variable l_adcValueLo must be init-ed with TEMP_NtcTable[x][0] for case when temp is less than -45C (lowest measurable temp) !
        // -Variable l_adcValueHi must be init-ed with TEMP_NtcTable[x][LAST] for case when temp is greater than +108C (highest measurable temp) !
        l_indexLo    = 0U;
        l_adcValueLo = 0U;
        l_indexHi    = (TEMP_NTCTABLE_LEN - 1U);
        l_adcValueHi = TEMP_NTCTABLE_MAXVAL;
        while( (unsigned char)( l_indexHi - l_indexLo ) > 1U ) // Keep searching until we find the temp interval in the NTC table
        {
            // ! WARNING ! Use uint for index because might have a situation where (hi + lo > 0xFF) !
            l_indexMid = ( (unsigned int)( (unsigned int)l_indexHi + l_indexLo ) >> 1U );

            l_compValue   = TEMP_NtcTable[ l_sensorModel ][ l_indexMid ];
            if( l_adcValue < l_compValue )
            {
                l_indexHi    = l_indexMid;
                l_adcValueHi = l_compValue;
            }
            else
            {
                l_indexLo    = l_indexMid;
                l_adcValueLo = l_compValue;
            }
        } // will exit on: hi = lo + 1

        //4 Convert table index to real temp in C degrees (temp can be negative for exhaust/external sensor)
        l_temp = (signed char)( l_indexLo - TEMP_ZERODEGREES_OFFSET );

        // 4_0 Convert table index to real temp in C degrees (temp can be negative for exhaust/external sensor)        
/*        if ( tempIndex == SYS_TEMPTANK2_IDX ) 
        {
         l_temp = (signed char)( l_temp + TEMP_ZERODEGREES_OFFSET +  TEMP_ZERODEGREES_OFFSET_HIGH_RANGE ); 
        }
*/        
        //5 Limit temperature and compute/limit temperature tenths
        // If temperature is less than minimum measurable temp, then limit to min temperature
        if( l_temp <= (signed char)TEMP_SENSOR_MINTEMP )
      	{
            // Limit to 0 all negative temperatures except that from exterior sensor
//            if( sensorChannel != ADC_CHANNEL_SNS_TEMPEXT )
//            {
//    	   	    l_temp = TEMP_SENSOR_MINTEMP; // Limit to 0 (to avoid working with negative/signed data)
//            }
//            else
//            {
//                // Nothing (isolated case with possible negative temperatures to exterior sensor)
//            }
//            l_temp10ths = 0U;  // tenths not used in this case
        }
        // If temperature is greater than maximum measurable temp, then limit to max temperature

        else if ( l_temp > (signed char)TEMP_SENSOR_MAXTEMP )  

        {             
            l_temp      = TEMP_SENSOR_MAXTEMP;
            l_temp10ths = 0U;  // tenths not used in this case
            
        }
        else// For temperatures in valid range, compute tenths (more precision)
        {
            // l_temp = "as_just_computed";
            l_range     = (unsigned char)( l_adcValueHi - l_adcValueLo );
            if(l_range!=0)
            l_temp10ths = (unsigned char)( ( ((l_adcValue - l_adcValueLo) * 10U) + ((l_range + 1U) >> 1U) ) / l_range );
        }

        //6 Update sensor error and temperature status
        // If sensor is not present, consider sensor and temp status are OK.
        // Note: Temperature is computed even if sensor-present parameter is set to "NOT_PRESENT" !
        if( FALSE == sensorPresent )
        {
            l_sensorError = FALSE;
            l_tempStatus  = OK;
        }
        // If [sensor is present and] adc-value is very low/high, then sensor is broken !
        // Also, in order to immediately detect an error, check diff. between prev and crt temps !
        // ! WARNING ! Avoid substractions in next conditions to eliminate SIGN problems !
        else
        if(  ( l_adcValue < TEMP_SENSOR_MINVAL )
           ||( l_adcValue > TEMP_SENSOR_MAXVAL )
           ||( (l_adcValue + TEMP_SENSOR_MAXDIF) <  l_adcPrevValue                       )   // crt < prev - delta
           ||(  l_adcValue                       > (l_adcPrevValue + TEMP_SENSOR_MAXDIF) )  )// crt > prev + delta
        {
            l_sensorError = FALSE; 
            l_tempStatus  = NOT_OK;    // Temp status is considered OK if sensor is broken !
        }
        // If [sensor present & operational and] temp is outside valid range, then temp status is NOT OK !
        // Note:-Sign needed for exterior temperature case (can be negative) !
        else
        {
//            l_sensorError = FALSE;
            if(  ( l_temp < (signed char)minTemp ) || ( l_temp > (signed char)maxTemp )  )
            {
                l_tempStatus = NOT_OK; 
            }
            // If [sensor present & operational and] temp is in valid range, then temp status is OK
            else
            {
                l_tempStatus = OK;
            }
        }
    
        //[7] Store local-outputs to global-outputs
        SYS_SensorError      = l_sensorError;
        SYS_SMQuantitiesStatus = l_tempStatus;
        //-Save integer part of temperature
        SYS_SMQuantitiesValx1  = (unsigned char)l_temp;
        
   
        
        
        //-Save temperature at scale x10
# if ( C34_BOILER_MODEL_for_C15 == 1U )        
        if ( tempIndex == SYS_TEMPTANK2_IDX ) 
        {
        l_tempValuex10 = ( ( ((signed int)l_temp) + TEMP_ZERODEGREES_OFFSET +  TEMP_ZERODEGREES_OFFSET_HIGH_RANGE ) * 10 )\
                                                  + (signed int)l_temp10ths;   // Sign for exterior temperature ! 
        }
        else
        {
        l_tempValuex10 = ( (signed int)l_temp * 10 ) + (signed int)l_temp10ths;// Sign for exterior temperature !
        }
#else
        l_tempValuex10 = ( (signed int)l_temp * 10 ) + (signed int)l_temp10ths;// Sign for exterior temperature !
#endif
        
//        SYS_SMQuantitiesValxS[ tempIndex ] = (unsigned int)l_tempValuex10;
        SYS_SMQuantitiesValxS = l_tempValuex10;
   //     }
    }
    else// ADC-average is not ready for specified sensor !
    {
        // Makes no sense to update temperature: Ignore call !
    }
  
}




/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/



/********************************************************************
 * Name       : TEMP_Update
 * Parameters : None
 * Return     : Nothing
 * Description: Updates temperatures values, status and errors
 *            : when the ADC temperature averages are ready.
 * Note       :-The ADC temperature averages need 50 ADC samples.
 *            : Consequently, the averages are ready after each
 *            : 50 alternances on Mains as samples are taken
 *            : at each mains zero-cross.
 *            :-For tank temperatures, averages are computed by P2 MCU.
 *            :-CH-turn fast-increase error has two parameters:
 *            : >maximum allowed *temperature* difference
 *            :  (between oldest and newest temp in queue)
 *            : >time unit: equal to time to fill queue (Tq):
 *            :             Tq = queue-length x queue_update_time
 *            : >the equivalent of km/hour would be here:
 *            :             Celsius_degrees/fill_seconds !
 *            :-Priority of CH-turn errors from highest to lowest:
 *            : sensor broken, temp fast increase,
 *            : temp too low/high, difference between turn & return.
 *            :-TO DO: Execution time must be checked/measured (1.4ms)!
 *            : If too high, then split execution: use states:
 *            : in each state a different temperature will be updated.
 * Warning    :-This function assumes quantities vectors have
 *            : same length and same organization !
 ********************************************************************/
void TEMP_Update( void )
{

    //1. Update sensors temperature value & status if it is time (each 50 alternances from mains)
    // WARNING: Do not use SW-timers to update temps because in case a sensor is disconnected
    //          the associated sensor error flag will be set, but then cleared because
    //          the ADC is synched with Mains, but the temps are synched with an internal timer!

    // Update SYS_TEMP
    TEMP_ComputeTemperature( ADC_CHANNEL_SYS_TEMP, SYS_SENSORMODEL_KOBER_3380,
                             (unsigned char)(-10)   , SYS_TEMP_MAXTEMP,
                             PRESENT );

}















