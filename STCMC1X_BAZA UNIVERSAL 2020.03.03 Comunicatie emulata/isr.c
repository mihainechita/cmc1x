/*******************************************************************************
* Description : Fisier sursa pentru Interrupt Service Routine
* File name   : isr.c
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>
#include <inavr.h>

/* System */
#include "types.h"
#include "isr.h"         
#include "adc.h"
#include "stdmacros.h"
#include "system.h"
#include "io_drv.h"
#include "menu.h"
#include "twi.h"
#include "usart.h"
#include "sensors_prot.h"

/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/
uint16_t TXT_HOME;
uint16_t GF_HOME;

uint16_t TXT_LEN;
uint16_t GF_LEN;
/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/

uint16_t SYS_VOFF_IDX_Length;
/* Counter with tick @250us Not-Synchronized with Mains: for PWM and SW-timer */
volatile uint8_t ISR_TicksCounterNSM;                                
/*---------------------------------------------------------------------------*/
/* Variabila folosita de ISR_TIMER0 */
static uint16_t LCD_API_Data=0U;

/* Variabila contor cu valori in intervalul [0..LCDAPI_CHARNR_TO_SEND] folosita
pentru trimiterea a 40/80 caractere per trecere prin main */
uint8_t LCD_API_Character_Counter=0U;

/* Variabila folosita pentru selectia liniei pentru setarea address pointerului 
(pozitiei unde se scrie in RAM-ul display-ului) si pentru selectia offsetului
de linie in cadrul matricii de LCD  */
volatile uint16_t LCD_API_Segment_Select=0U;

/* Matricea principala a LCD-ului, buffer pentru primire si trimitere continut
catre LCD; la fel ca si LCD */ 
/* plus inca un seg_size cand da cu virgula impartirea */
uint8_t LCD_API_DATA[ LCD_API_CHAR_MAXNR + LCD_API_SEG_SIZE + LCD_API_SEG_SIZE]; 

/*  Variabila ce indica intreruperii ISR_TIMER0 ca este timpul sa schimbe
offset-ul de la care trebuie sa citeasca date  */
uint8_t ISR_LCD_API_ChangeOffset = FALSE;

/* Vector principal de offset fata de pozitia LCD_API_DATA[0]. Contine offset-uri
de la care rutina de intrerupere incepe sa ia caractere si sa le trimita spre 
LCD */
uint16_t VOFFSET[LCD_VOFF_IDX_LENGTH];

/* Variabila folosita la interschimbarea offseturilor */
uint16_t temp;


uint16_t ISR_FlowTickCounter=0U;

volatile uint16_t ISR_INT0_Refresh=0U;

/******************************************************************************
* Variabile folosite de TIMER2_COMP_vect si de INT2_vect pentru a procesa
* comunicatia emulata pe pinii PB2 RX si PD7 TX
******************************************************************************/

/* Flag ce activeaza/dezactiveaza procesul de receptie */
static uint8_t SENSORS_PROT_RXEnable;

/* Cea mai recenta valoare de bit receptionata (10 bits/frame: 1 start bit, 8 data bits, 1 stop bit) */
static uint8_t SENSORS_PROT_RXBitValue;

/* Fiecare perioada de bit este esantionata de 8 ori; variabila contor pentru sample-uri pentru receptie*/
static uint8_t SENSORS_PROT_RXBitSampleCnt;

/* Valoarea ultimului byte receptionat */
static uint8_t SENSORS_PROT_RXByte;

/* Indexul bitului curent in frame ul de receptie (10 bit) */
static uint8_t SENSORS_PROT_RXBitIndex;

/* Valoarea curenta a sample-ului esantionat pentru procesul de receptie */
static uint8_t SENSORS_PROT_RXBit;

/* Fiecare perioada de bit este esantionata de 8 ori; variabila contor pentru sample-uri pentru transmisie*/
static uint8_t SENSORS_PROT_TXBitSampleCnt;

/* Valoarea byte-ului ce este transmis */
static uint8_t SENSORS_PROT_TXByte;

/* Indexul bitului curent in frame ul de transmisie (10 bit) */
static uint8_t SENSORS_PROT_TXBitIndex;


/*******************************************************************************/
/*---------------------------------------------------------------------------*/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/

/* Functie de initializare cu offset-uri de la pozitia de capat LCD_API_DATA[0]*/
void LCD_API_VOFFSET_Init( void )
{
    uint16_t i=0U;
    do
    {
        VOFFSET[i]= LCD_API_SEG_SIZE * ( ( uint16_t ) i );
        i=i+1U;
    }
    while(VOFFSET[i-1] < LCD_API_CHAR_MAXNR );
    SYS_VOFF_IDX_Length = i;   
}

/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/******************************************************************************
* Name       : ISR__INT0_EdgeDetected
* Description: Intrerupere de sincronizare cu frecventa de 50 Hz a tensiunii de 
                alimentare cu 230VAC
* Start achizitie in momentul de zgomot de retea 0  
* Max freq.  : 100 Hz (10 ms)
* Exec.time  : 2.24 us  Max: 2.24 us  @ 8MHz    Optimizari None
* Exec.time  : 1.88 us   Max: 1.88 us  @ 8MHz    Optimizari HIGH Speed  
******************************************************************************/

#pragma vector=INT0_vect
__interrupt void ISR_INT0_EdgeDetected( void )                                 
{
/* Pornire conversie ADC */
//    ADC_START_CONVERSION();
    
/* Toggle pe ISR_INT0_Refresh pentru executie alternativa scriere relee sau citire
 tastatura si semnale feedback de la elementele de executie ale centralei (pompa, 
 ventilator, vana cu 3 cai ) */    
//    ISR_INT0_Refresh ^=0x01U;
}



///******************************************************************************
//* Name       : ISR__TIMER0
//* Description: Citire date din zona de memorie alocata LCD si trimiterea acestora
//*               catre LCD 
//* Max freq.  : 500Hz (2 ms)
//* Exec.time  : 48us  Max: 290us  @ 8MHz    Optimizari None
//* Exec.time  : 42us  Max: 240us  @ 8MHz    Optimizari HIGH Speed  
//******************************************************************************/  
//
#pragma vector=TIMER0_OVF_vect
__interrupt void ISR_TIMER0_OVF_vect( void )
{   
//    static uint8_t l_LCD=0U;
//    static uint8_t l_LCD_Data=0U;
//    static uint8_t l_ISR_Character_Counter=0U;
//    static uint16_t l_ISR_LCD_Segment_Select=0U;
//    static uint8_t i=0U;
//    static uint8_t ISR_LCD_Search_Index=0U;
//    
//    /* Scrierea TCNT se face pentru a modifica perioada intreruperii */
//    /* Timerul va numara de la TCNT la maxim (255) */
//    /* TCNT=7 pt periodada 2ms */
//    /* TCNT=132 pt periodada 1ms */
//    TCNT0=7U;
//    
//    /* Incarcare variabila locala cu continut din globala; selectie segment de memorie 
//    pentru setare address pointer LCD (selectie locatie in RAM LCD )*/
//    l_ISR_LCD_Segment_Select=LCD_API_Segment_Select;
//    
//    /* Daca trimiterea se face cu offset sau fara */    
//#if( SYS_ISR_SEND_WITH_OFFSET_ROTATION)                
//    temp = VOFFSET[SYS_VOFF_IDX_Length - 1U];
//    VOFFSET[SYS_VOFF_IDX_Length - 1U] = VOFFSET[l_ISR_LCD_Segment_Select];
//    VOFFSET[l_ISR_LCD_Segment_Select] = temp;
//#endif  
//
//    /* selectie segment pentru pozitionare */
//    LCD_API_Data=( l_ISR_LCD_Segment_Select * LCD_API_SEG_SIZE ) ;
//    
//    
//    
//    /* trimitere low byte pentru pozitionare */  
//    l_LCD_Data=(LCD_API_Data);
//    l_LCD=((l_LCD_Data&0x80U));
//    l_LCD=l_LCD>>6U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x40U));
//    l_LCD=l_LCD>>5U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x20U));
//    l_LCD=l_LCD>>4U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x10U));
//    l_LCD=l_LCD>>3U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x08U));
//    l_LCD=l_LCD>>2U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x04U));
//    l_LCD=l_LCD>>1U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=(l_LCD_Data&0x02U);
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x01U));
//    l_LCD=l_LCD<<1U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    STROBE_LCD;
//    LCD_CONTROL_SET_DATA;
//    LCD_CONTROL_CE_ENABLE;
//    LCD_CONTROL_WR_ENABLE;
//    asm("nop");
//    LCD_CONTROL_WR_DISABLE;
//    LCD_CONTROL_CE_DISABLE;
//    
//    
//    /* trimitere high byte pentru pozitionare */
//    l_LCD_Data=(LCD_API_Data>>8U);
//    l_LCD=((l_LCD_Data&0x80U));
//    l_LCD=l_LCD>>6U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x40U));
//    l_LCD=l_LCD>>5U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x20U));
//    l_LCD=l_LCD>>4U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x10U));
//    l_LCD=l_LCD>>3U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x08U));
//    l_LCD=l_LCD>>2U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x04U));
//    l_LCD=l_LCD>>1U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=(l_LCD_Data&0x02U);
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    l_LCD=((l_LCD_Data&0x01U));
//    l_LCD=l_LCD<<1U;
//    PORTB=l_LCD;
//    RISING_EDGE_CLOCK;
//    STROBE_LCD;
//    LCD_CONTROL_SET_DATA;
//    LCD_CONTROL_CE_ENABLE;
//    LCD_CONTROL_WR_ENABLE;
//    asm("nop");
//    LCD_CONTROL_WR_DISABLE;
//    LCD_CONTROL_CE_DISABLE;
//    
//    
//    /* trimitere comanda 0x24 pentru pozitionare */
//    LCD_API_SETPOS_CMD;    
//    
//    
//    /* trimitere comanda 0xB0 pentru pornire autoscriere (trimitere comanda de   
//    scriere urmata de trimitere la rafala de bytes doar de date, fara comanda 0xC0
//    dupa trimitere data, cum se procedeaza in cazul scrierii normale)*/ 
//    LCD_API_ACTIVATE_AUTOWRITE;
//    
//    
//    
//    /* Bucla principala in care se trimit efectiv caractere spre RAM-ul LCD-ului */         
//    for( l_ISR_Character_Counter=0U ; l_ISR_Character_Counter < LCD_API_SEG_SIZE ; l_ISR_Character_Counter++ )
//    {
//        
//        /* Selectie caracter in functie de metoda de trimitere 
//        (se scade 0x20 pentru conversie in ASCII LCD) */
//#if( SYS_ISR_SEND_WITH_OFFSET_ROTATION)                
//        LCD_API_Data = (LCD_API_DATA[ VOFFSET[SYS_VOFF_IDX_Length - 1U] + l_ISR_Character_Counter]-0x20U);
//#else            
//        LCD_API_Data = (LCD_API_DATA[ VOFFSET[l_ISR_LCD_Segment_Select] + l_ISR_Character_Counter]-0x20U);
//#endif                
//        /* Scriere caracter */       
//        l_LCD_Data=LCD_API_Data;
//        l_LCD=((l_LCD_Data&0x80U));
//        l_LCD=l_LCD>>6U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x40U));
//        l_LCD=l_LCD>>5U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x20U));
//        l_LCD=l_LCD>>4U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x10U));
//        l_LCD=l_LCD>>3U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x08U));
//        l_LCD=l_LCD>>2U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x04U));
//        l_LCD=l_LCD>>1U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=(l_LCD_Data&0x02U);
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        l_LCD=((l_LCD_Data&0x01U));
//        l_LCD=l_LCD<<1U;
//        PORTB=l_LCD;
//        RISING_EDGE_CLOCK;
//        STROBE_LCD;
//        LCD_CONTROL_SET_DATA;
//        LCD_CONTROL_CE_ENABLE;
//        LCD_CONTROL_WR_ENABLE;
//        asm("NOP");
//        LCD_CONTROL_WR_DISABLE;
//        LCD_CONTROL_CE_DISABLE;
//        
//    }
//    
//    /* trimitere comanda 0xB2 pentru oprire autoscriere */
//    LCD_API_DEACTIVATE_AUTOWRITE;  
//    
//    
//    /* Incrementare modulo segmentare a indexului de segment (incrementare + resetare cand pozitia este egala cu segmentarea) */  
//    if(SYS_VOFF_IDX_Length != 0U)
//    l_ISR_LCD_Segment_Select=(l_ISR_LCD_Segment_Select+1) % SYS_VOFF_IDX_Length;
//    
//    /* Salvare index bloc de memorie curent */
//    LCD_API_Segment_Select = l_ISR_LCD_Segment_Select;
//    
///*--------------------------------------------------------------------------------------------------------------------------------------------------*/    

}


/******************************************************************************
* Name       : ISR__TIMER1_Overflow
* Description:-This ISR is executed at each Timer 1 overflow (each 250us)
*            :-Used to:
*            :1- Software timers
* Max freq.  : 4000Hz (250us)
* Exec.time  : 28.8us  Max: 34.4us (112cycles @ 4MHz)
* WARNING    : Assumes mains frequency is 50Hz (period = 20ms) !
*            : Consequently, SYNC is expected to generate a pulse each 10ms !
******************************************************************************/  

#pragma vector=TIMER1_OVF_vect
__interrupt void ISR_TIMER1_Overflow( void )
{
//    SET(PORTD,PD7);
    /* Update Software timers */
    uint8_t l_ticksCounter;      
    
    /* TIMER variables used for Software Timers  */
    /* Load ticks counter to increment previous value of it */
    l_ticksCounter=ISR_TicksCounterNSM;	     
    
    /* Update the counter to signify that another 250us have passed */
    l_ticksCounter++;
    /* Store/save ticks counter so it's content won't be lost */
    ISR_TicksCounterNSM=l_ticksCounter;	       
    
    
    /* Variabila folosita pentru a genera pe OC1B frecventa pentru simulare debit  */
    l_FlowTickCounter=ISR_FlowTickCounter;
   
    if(l_FlowTickCounter < (SYS_Flow_Tick_Nr/2))
    {
        BUZZ_ON;
    }
    else
    {
        BUZZ_OFF;
    }
    l_FlowTickCounter++;
    if(SYS_Flow_Tick_Nr != 0)
    l_FlowTickCounter = l_FlowTickCounter % SYS_Flow_Tick_Nr;
    ISR_FlowTickCounter = l_FlowTickCounter;
//    CLR(PORTD,PD7);
}


/*-----------------------------------------------------------------------------
TWI_Write va fi apelata dupa ce data, ora, minutul etc au fost setate si se apasa 
ENTER, si se iese in meniul principal.
	La intrarea in meniul de setare data, se afiseaza ultimele valori aled
datei inainte de a intra in meniu de setare.
 Functia populeaza bufferul de trimitere inainte de a da start trimiterii
 si implicit copierii de date din bufferul de trimitere in TWDR. 
Conform spec-urilor din datasheetul PCF8523T legate de secventa de scriere date 
in PCF, softul standului ar trebui sa execute urmatoarele:
	1. Setare comanda de START � data de catre functia TWI_Write, executata 
in zona de menu.c
	2. Automatul din intreruperea de TWI ar trebui sa detecteze daca comanda
 de START s-a dat cu succes, daca TWSR(TWI Status Register) (mascat cu 0xF8 pt a
 ignora bitii de prescaler), contine valoarea 0x08; Daca se gaseste valoare 0x08,
 automatul va incarca in TWDR TWI_TransmitBuffer[0], unde se gaseste adresa PCF 
(0xD0) plus 0 pe LSB pentru a semnaliza ca vreau sa fac o scriere (date incarcate
 deja de catre TWI_Write in TWI_TransmitBuffer, ISR doar va copia cand trebuie date
 din TWI_TransmitBuffer in TWDR). Din acest moment automatul poate functiona singur,
 nemaiavand nevoie de interventie din partea MAIN. Bitul TWINT este 0 cand TWI 
si-a terminat treaba, iar pt a incepe sa transmita din nou trebuie pus manual pe 1,
 deci starea TWI_Start va copia adresa PCF + 0 pe LSB si va scrie TWINT pe 1, 
pt a transmite acest byte.
	3.  Dupa ce TWI transmite SLA+W, bitul TWINT se va pune pe 0, si se va 
genera intreruperea de TWI din nou; de aceasta data automatul ar trebui sa intre 
in starea 2, si anume SLA+W_ACK; daca s-a transmis adresa si PCF a raspuns cu 
Acknowledge, TWSR ar trebui sa aiba valoarea 0x18; Daca TWST=0x18 atunci incarc 
in TWSR TWI_TransmitBuffer[1], unde TWI_Write a incarcat inainte adresa registrului
 de secunde, si anume 0x03; Setez TWINT pt a transmite adresa
	4. Daca se triggereaza ISR si TWSR = 0x28, incarc in TWDR valoarea 
registrului de secunde.
	5. Daca TWSR = 0x28, TWDR=valoarea registrului de minute
	6. Daca TWSR = 0x28, TWDR=valoarea registrului de ore
	7. Daca TWSR = 0x28, TWDR=valoarea registrului de zile ale lunii
	8. Daca TWSR = 0x28, TWDR=valoarea registrului de zile ale saptamanii
	9. Daca TWSR = 0x28, TWDR=valoarea registrului de luni
	10. Daca TWSR = 0x28, TWDR=valoarea registrului de ani
	11. STOP
------------------------------------------------------------------------------*/
#pragma vector=TWI_vect
__interrupt void ISR_TWI( void )
{
    /* Verificare TWI Status Register; mascare biti de prescaler */
    switch (TWSR & 0xF8U)
    {
	/* Start transmis */
        case TWI_START:
	{
            if(TWI_Transmit_Receive_Mode==0)
            {
                /* Resetare index buffer de transmisie */
                TWI_TransmitBuffer_Index=0U;
                /* Incarcare adresa slave + bit scriere (0xD0); TWI_TransmitBuffer
                este deja incarcat de catre functia TWI_Write */
                TWDR = TWI_TransmitBuffer[TWI_TransmitBuffer_Index];
                /* Reactivare transmisie */
                TWI_ResetTWINT;
                /* Incrementare index buffer de transmise */
                TWI_TransmitBuffer_Index++;
            }
            else
            {
                /* Incarcare adresa slave + bit citire (0xD0); TWI_ReceiverBuffer
                este deja incarcat de catre functia TWI_Read */
                TWDR = TWI_ReceiveBuffer[TWI_ReceiveBuffer_Index];
                /* Reactivare transmisie */
                TWI_ResetTWINT;
                /* Incrementare index buffer de receptie */
                TWI_ReceiveBuffer_Index++;
            }
	    break;
	}
        case  TWI_MTX_SLA_W_ACK:
	{
            if(TWI_Transmit_Receive_Mode==0U)
            {
                /* Incarcare adresa registru secunde (0x03) */
                TWDR = TWI_TransmitBuffer[TWI_TransmitBuffer_Index];
                /* Reactivare transmisie */
                TWI_ResetTWINT;
                TWI_TransmitBuffer_Index++;	    
            }
            else
            {
                /* Incarcare adresa registru de control (0x00) */
                TWDR = TWI_ReceiveBuffer[TWI_ReceiveBuffer_Index];
                /* Reactivare transmisie */
                TWI_ResetTWINT;
                TWI_ReceiveBuffer_Index++;	     
            }
            
	    break;
	}
        
        case TWI_MTX_SLA_W_NACK:
        {
            TWI_Stop;
            break;
        }
        
        case  TWI_MTX_DATA_ACK:
	{
            if(TWI_Transmit_Receive_Mode==0U)
            {
                if(TWI_TransmitBuffer_Index < TWI_TransmitBufferLength-1)
                {
                    /* Incarcare adresa regsitru secunde (0x03); */
                    TWDR = TWI_TransmitBuffer[TWI_TransmitBuffer_Index];
                    /* Reactivare transmisie */
                    TWI_ResetTWINT;	    
                    TWI_TransmitBuffer_Index++;
                }
                else
                    TWI_Stop; 
            }
            else
            {
                TWI_Stop;
                TWI_Start;
            }
	    break;
	}	
	
	case  TWI_MRX_SLA_R_ACK:
	{
            
            TWI_SendACK;

                        
	    break;
	}
        
        
        case  TWI_MRX_DATA_ACK:
	{
            if(TWI_ReceiveBuffer_Index < TWI_ReceiveBufferLength)
            {
            TWI_ReceiveBuffer[TWI_ReceiveBuffer_Index]=TWDR;
            TWI_ReceiveBuffer_Index++;
            TWI_SendACK;
            }
            else
            {
              
                TWI_SendNACK;
                

            }
	    break;
	}


        case  TWI_MRX_DATA_NACK:
	{
            TWI_ReceiveBuffer[TWI_ReceiveBuffer_Index]=TWDR;
            TWI_ReceiveBuffer_Index=0U;
            TWI_Stop;
	    break;
	}

        case  TWI_MRX_SLA_R_NACK:
	{
            TWI_Stop;
	    break;
	}
        

	
        default:
        {
	TWI_Stop;
        break;
        }
    }
}




/******************************************************************************
* Name       : ISR__ADC_ConversionComplete
* Description: ISR executed each time an ADC conversion is completed. 
* Max freq.  : 4464Hz (224us): Fosc/64= 62.50Khz => T= 16us (13*16us / sample)
* Exec.time  : Max: 23.25us (93cycles @ 4MHz)
* Note       : This ISR will be executed 8 times only after each zero-cross 
*            : on mains voltage ! Consequently, will load the main loop
*            : execution time with a maximum of 8x ADC_ISR_Time= 8x23.25=186us!
******************************************************************************/
#pragma vector=ADC_vect
__interrupt void ISR__ADC_ConversionComplete( void )
{
//    SET(PORTD,PD7);
    /* ADC-channel currently converted */
    uint8_t l_channel;      
    
    /* ADC current sample */
    uint16_t  l_sample;                       
    
    /* ADC samples counter for current channel */
    uint8_t l_counter;                        
    
    /* Load index of currently sampled channel (no mask needed as bits 7:3 of ADMUX are always 0) */
    l_channel = ADMUX;                      
    
    /* Load samples counter for current channel */
    l_counter = ADC_SamplesCounters[ l_channel];  
    
    /* Check if number of needed samples is reached */
    if( l_counter != 0U )                          
    {
        /* Decrement samples counter */
        l_counter--;            
        
        /* Save samples counter */
        ADC_SamplesCounters[ l_channel] = l_counter;
        
        /* Read sample from ADC-unit */
        __in_word( ADC, l_sample ); 
        
        /* Sum sample in ADC sum-buffer (ADC is 10-bit wide + data-bus is 8-bit wide => copy byte-by-byte) */
        ADC_SamplesSumsISR[l_channel] += l_sample;  
    }
    else
    {
        /* Nothing: Number of needed samples for channel reached ! */
    }

    /* Check if acquisitions did not finish (there are other channels to be sampled) */
    if( l_channel <= ADC_LAST_CHANNEL )
    {
        /* Start conversion on next channel */
        ADC_START_CONVERSION();                     
    }
    /* All channels sampled ! */
    else
    {
        /* Nothing */
    }
//    CLR(PORTD,PD7);
}

/******************************************************************************
* Name       : ISR__USART_RXComplete
* Description:-This ISR is executed each time a byte reception is completed.
*            :-Used for receiving messages sent by PC application.
* Max freq.  : 1920Hz (520.83us @19200bps)
* Exec.time  : Max: 16.50us (66cycles @ 4MHz)
******************************************************************************/
#pragma vector=USART_RXC_vect
__interrupt void ISR__USART_RXComplete( void )
{
    uint8_t l_data;
    uint8_t l_head;
    
    l_head = USART_RX_Head;
        
    // Read received data-byte: read USART Data Register (UDR)
    l_data = UDR;

    /* Buffer full */
    if ((l_head + 1) % USART_RX_BUFFER_SIZE == USART_RX_Tail )  
    {
        asm ("NOP");
    }
    else
    {
        USART_RX_Buffer[USART_RX_Head]=l_data;
        USART_RX_Head = ( USART_RX_Head + 1 ) % USART_RX_BUFFER_SIZE;
    }
}

/******************************************************************************
* Name       : ISR__USART_TXComplete
* Description:-This ISR is executed each time a byte transmission is completed.
*            :-Used for sending messages to PC application.
* Max freq.  : 1920Hz (520.83us @19200bps)
* Exec.time  : Max: 11.75us (47 cycles @ 4MHz)
******************************************************************************/
#pragma vector=USART_UDRE_vect
__interrupt void ISR__USART_TX_UDREmpty( void )
{
    uint8_t l_tail=0;
    
    l_tail = USART_TX_Tail;
    
    /* Daca bufferul nu este gol */
    if (l_tail != USART_TX_Head )  
    {
        UDR = USART_TX_Buffer[ l_tail ];
        l_tail = ( l_tail + 1 ) % USART_TX_BUFFER_SIZE;
    }
    else
    {
        asm ("NOP");
        /* Dezactivare intrerupere TX-UDRE */
        CLR( UCSRB, UDRIE );
    }
    
    USART_TX_Tail = l_tail;
}

/******************************************************************************
* Name       : INT2_vect_interrupt
* Description:-This ISR is executed each time a byte transmission is completed.
*            :-Used for sending messages to PC application.
* Max freq.  : 240Hz (416us @2400bps)
* Exec.time  : Max: tbd (25 cycles @ 8MHz)
******************************************************************************/
  // receiver process uses external interrupt 2 on falling edge to detect start condition,
	// to intialize variables and to enable receiver process
#pragma vector = INT2_vect
__interrupt void INT2_vect_interrupt( void )
{
//    SET(PORTD,PD7);
    if ( SENSORS_PROT_RXEnable == 0 )
    {
        // initialize data for receiver process
        // TCNT2 = 0x00; // think about that !!! it works fine without it
        SENSORS_PROT_RXBitIndex = 0;
        SENSORS_PROT_RXBitSampleCnt = 0;
        SENSORS_PROT_RXEnable = 1;	
    }
    
    /* disable this interrupt until current data frame is completly received (10 bits per frame)
    the interrupt wil be enabled again after stop bit is received */
    DISABLE_INT2;
//    CLR(PORTD,PD7);
}


/******************************************************************************
* Name       : TIMER2_COMP_vect_interrupt
* Description:-This ISR is executed each time a sample is transmitted or received
*            :-Used for sending/receiving messages to PC application.
* Max freq.  : tbd (tbd @2400bps)
* Exec.time  : Max: tbd (25 cycles @ 8MHz)
******************************************************************************/
// timer2 compare match interrupt is used for timing control of receiver and transmitter processes
// this interrupt is triggered each 52 us
// 2400 baud -> 416us bit period, 416us / 8 samples per bit = 52 us
// execution time is
#pragma vector = TIMER2_COMP_vect
__interrupt void TIMER2_COMP_vect_interrupt( void )
{
    
    SET(PORTD,PD7);
    
  /*   receiver process implemented next */
    if ( SENSORS_PROT_RXEnable == 1 )
    {
        
//        SET(PORTD,PD7);
        // read RX pin
        SENSORS_PROT_RXBit = (SENSORS_PROT_READ_RX_PIN != 0);
        
        // update bit sample index for receiver process; it should have values between 0 and 7
        INC_TO_2_PWR( SENSORS_PROT_RXBitSampleCnt, 3);
        
        if ( SENSORS_PROT_RXBitSampleCnt != 0 )
        {
            // for each bit, two samples are taken : those with indexes 3 and 4 (out of 8 samples per bit)
            // stop bit has custom treatment because it has fewer samples
            if ( SENSORS_PROT_RXBitIndex == 9 )
            {
                // this is for stop bit sampling
                if ( SENSORS_PROT_RXBitSampleCnt > 5 )
                {
                    SENSORS_PROT_RXBitValue += SENSORS_PROT_RXBit;
                }
            }
            else
            {
                // this is for data and start bit sampling
                // verify if SENSORS_PROT_RXBitSampleCnt is 3 or 4 !
                if ( SENSORS_PROT_RXBitSampleCnt > 2 && SENSORS_PROT_RXBitSampleCnt < 5 )
                {
                    SENSORS_PROT_RXBitValue += SENSORS_PROT_RXBit;
                }
            }
        }
        else
        {
            // once at 8 samples per bit (SENSORS_PROT_RXBitSampleCnt is 0 then) update bit value
            // stop bit has less samples than data bits because its processing must end up sooner
            // to prevent missing (loosing) the start condition for next byte
            // if both samples for one bit are 1 then this bit is one
            if ( SENSORS_PROT_RXBitValue > 1 )
            {
                SENSORS_PROT_RXBitValue = 0x80;
            }
            else
            {
                SENSORS_PROT_RXBitValue = 0x00;
            }
            // treat received bits as start bit (SENSORS_PROT_RXBitIndex is 0), data bits (SENSORS_PROT_RXBitIndex between 1 and 8)
            // or stop bit (SENSORS_PROT_RXBitIndex is 9)
            if ( SENSORS_PROT_RXBitIndex != 0 )
            {
                if ( SENSORS_PROT_RXBitIndex != 9 )
                {
                    if ( SENSORS_PROT_RXBitIndex == 8 )
                    {
                        // set less samples count for stop bit
                        SENSORS_PROT_RXBitSampleCnt = SENSORS_PROT_STOP_BIT_SAMPLES;
                    }
                    // process data bit received : put it in SENSORS_PROT_RXByte variable
                    SENSORS_PROT_RXByte >>= 1;
                    SENSORS_PROT_RXByte |= SENSORS_PROT_RXBitValue;
                }
                else
                {
                    // stop bit process
                    if ( SENSORS_PROT_RXBitValue != 0 )
                    {
                        // if STOP BIT is ok (eq. it is 1) then put data byte in circular buffer
                        SENSORS_PROT_RX_Buffer[ SENSORS_PROT_RX_Buffer_Head ] = SENSORS_PROT_RXByte;
                        INC_TO_2_PWR( SENSORS_PROT_RX_Buffer_Head, 5 );	
                    }
                    // else means STOP_BIT_ERROR
                    // data received is ignored eq. skip variable store in buffer
                    
                    // initialize variables for next incoming byte
                    SENSORS_PROT_RXBitIndex = 0xff;
                    SENSORS_PROT_RXEnable = 0;
                    ENABLE_INT2;					
                }
            }
            else
            {
                // start bit process
                if ( SENSORS_PROT_RXBitValue != 0)
                {
                    // START_BIT_ERROR;
                    // abort current receiving byte and restart reception
                    // initialize variables for next incoming byte
                    SENSORS_PROT_RXBitIndex = 0xff;
                    SENSORS_PROT_RXEnable = 0;
                    ENABLE_INT2;					
                }
            }
            // update variables for receiver process
            SENSORS_PROT_RXBitIndex++;
            SENSORS_PROT_RXBitValue = 0;
        }
    }
            CLR(PORTD,PD7);

    
//    /* TRANSMISIE */
//    /* Zona activa doar cand exista elemente de transmis */
//    if ( SENSORS_PROT_TX_Buffer_Head != SENSORS_PROT_TX_Buffer_Tail )
//    {
//        if ( SENSORS_PROT_TXBitSampleCnt == 0 )
//        {
//            // treat transmitted bits as start bit (SENSORS_PROT_TXBitIndex is 0), data bits (SENSORS_PROT_TXBitIndex between 1 and 8)
//            // or stop bit (SENSORS_PROT_TXBitIndex is 9)		
//            if ( SENSORS_PROT_TXBitIndex == 0 )
//            {
//                // transmit start bit
//                SENSORS_PROT_WRITE_TX_PIN( 0 );
//                // get the byte to be sent from transmitter circular buffer
//                SENSORS_PROT_TXByte = SENSORS_PROT_TXBuffer[ SENSORS_PROT_TX_Buffer_Tail ];
//            }
//            else
//            {
//                if ( SENSORS_PROT_TXBitIndex < 9 )
//                {
//                    // transmit data bit from SENSORS_PROT_TXByte variable (bit LSB is transmitted first)
//                    SENSORS_PROT_WRITE_TX_PIN( SENSORS_PROT_TXByte & 0x01 );
//                    SENSORS_PROT_TXByte >>= 1;
//                }
//                else
//                {
//                    if ( ( SENSORS_PROT_TXBitIndex == 9 )                                                                                                                                                                                                                       || ( SENSORS_PROT_TXBitIndex == 10 ) ) // laci modified 2010.04.28: added 2 STOP bits
//                    {
//                        // transmit stop bit
//                        SENSORS_PROT_WRITE_TX_PIN( 1 );
//                    }
//                    else
//                    {
//                        // stop bit was sent, so go to the next byte in transmitter circular buffer
//                        INC_TO_2_PWR( SENSORS_PROT_TX_Buffer_Tail, 5 );
//                        // initialize variables for next transmitted byte						
//                        // SENSORS_PROT_TXBitIndex will be incremented next, so in order to be 0, now it is set to 0xff (= -1)
//                        SENSORS_PROT_TXBitIndex = 0xFFU;
//                        // SENSORS_PROT_TXBitSampleCnt will be incremented next, so in order to be 0 after inc, now it is set to 0x07
//                        SENSORS_PROT_TXBitSampleCnt = (SENSORS_PROT_SAMPLES_PER_BIT - 1);
//                    }
//                }
//            }
//            SENSORS_PROT_TXBitIndex++;
//        }
//        INC_TO_2_PWR ( SENSORS_PROT_TXBitSampleCnt, 3);
//    }

//    CLR(PORTD,PD7);

}







/***********************************************************************
* Name       : ISR__Unused_interrupts
* Description: Empty ISR used to catch and ignore unused interrupts !
*            : (unused means not handled by application)
*            : Recommended for safety reasons !
* Warning    :-Comment pragma directives for used vectors/interrupts!
*            :-Leave only pragmas for UNUSED vectors/interrupts !!!
* Compiler   : In IAR compiler: General Options -> System:
*            : Uncheck option "Init unused interrupt vectors with RETI"
***********************************************************************/

#pragma vector=INT1_vect,\
                TIMER2_OVF_vect,\
                TIMER1_CAPT_vect,\
                TIMER1_COMPA_vect,\
                TIMER1_COMPB_vect,\
                TIMER0_COMP_vect,\
                SPI_STC_vect,\
                EE_RDY_vect,\
                ANA_COMP_vect,\
                SPM_RDY_vect
                                                            
                                                            
__interrupt void ISR__Unused_interrupts( void )
{
    /* All blocking points (terminal points) which are after system init
     shall be preceded by a safety shutdown performed via safety-enable pin.
     This pin is logical ANDed with gas-valve and igniter commands.
     Unexpected call of unused ISR: wait for Watchdog-Timer reset */
    for( ;  ; )
    {
        asm ("NOP");
        /* Loop here [until WDT reset occurs] ! */
    }
}




























