/*******************************************************************************
* Description : Fisier sursa pentru calcul CRC
* File name   : crc.c
* Author      : Ing. Mihai Nechita   
* Date	      : 22.11.2019          
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
 * Includes
 *----------------------------------------------------------------------------*/
#include "crc.h"

/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private (static) defines
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public variables
 *----------------------------------------------------------------------------*/
// Table of pre-computed CRC-16 constants: implements CRC-16-CCITT polynom 0x1021(x^16+x^12+x^5+1)
__flash const unsigned int CRC_Table[ 256U ] =
{
    0x0000U, 0x1021U, 0x2042U, 0x3063U, 0x4084U, 0x50a5U, 0x60c6U, 0x70e7U, //  0
    0x8108U, 0x9129U, 0xa14aU, 0xb16bU, 0xc18cU, 0xd1adU, 0xe1ceU, 0xf1efU, //  1
    0x1231U, 0x0210U, 0x3273U, 0x2252U, 0x52b5U, 0x4294U, 0x72f7U, 0x62d6U, //  2
    0x9339U, 0x8318U, 0xb37bU, 0xa35aU, 0xd3bdU, 0xc39cU, 0xf3ffU, 0xe3deU, //  3
    0x2462U, 0x3443U, 0x0420U, 0x1401U, 0x64e6U, 0x74c7U, 0x44a4U, 0x5485U, //  4
    0xa56aU, 0xb54bU, 0x8528U, 0x9509U, 0xe5eeU, 0xf5cfU, 0xc5acU, 0xd58dU, //  5
    0x3653U, 0x2672U, 0x1611U, 0x0630U, 0x76d7U, 0x66f6U, 0x5695U, 0x46b4U, //  6
    0xb75bU, 0xa77aU, 0x9719U, 0x8738U, 0xf7dfU, 0xe7feU, 0xd79dU, 0xc7bcU, //  7
    0x48c4U, 0x58e5U, 0x6886U, 0x78a7U, 0x0840U, 0x1861U, 0x2802U, 0x3823U, //  8
    0xc9ccU, 0xd9edU, 0xe98eU, 0xf9afU, 0x8948U, 0x9969U, 0xa90aU, 0xb92bU, //  9
    0x5af5U, 0x4ad4U, 0x7ab7U, 0x6a96U, 0x1a71U, 0x0a50U, 0x3a33U, 0x2a12U, // 10
    0xdbfdU, 0xcbdcU, 0xfbbfU, 0xeb9eU, 0x9b79U, 0x8b58U, 0xbb3bU, 0xab1aU, // 11
    0x6ca6U, 0x7c87U, 0x4ce4U, 0x5cc5U, 0x2c22U, 0x3c03U, 0x0c60U, 0x1c41U, // 12
    0xedaeU, 0xfd8fU, 0xcdecU, 0xddcdU, 0xad2aU, 0xbd0bU, 0x8d68U, 0x9d49U, // 13
    0x7e97U, 0x6eb6U, 0x5ed5U, 0x4ef4U, 0x3e13U, 0x2e32U, 0x1e51U, 0x0e70U, // 14
    0xff9fU, 0xefbeU, 0xdfddU, 0xcffcU, 0xbf1bU, 0xaf3aU, 0x9f59U, 0x8f78U, // 15
    0x9188U, 0x81a9U, 0xb1caU, 0xa1ebU, 0xd10cU, 0xc12dU, 0xf14eU, 0xe16fU, // 16
    0x1080U, 0x00a1U, 0x30c2U, 0x20e3U, 0x5004U, 0x4025U, 0x7046U, 0x6067U, // 17
    0x83b9U, 0x9398U, 0xa3fbU, 0xb3daU, 0xc33dU, 0xd31cU, 0xe37fU, 0xf35eU, // 18
    0x02b1U, 0x1290U, 0x22f3U, 0x32d2U, 0x4235U, 0x5214U, 0x6277U, 0x7256U, // 19
    0xb5eaU, 0xa5cbU, 0x95a8U, 0x8589U, 0xf56eU, 0xe54fU, 0xd52cU, 0xc50dU, // 20
    0x34e2U, 0x24c3U, 0x14a0U, 0x0481U, 0x7466U, 0x6447U, 0x5424U, 0x4405U, // 21
    0xa7dbU, 0xb7faU, 0x8799U, 0x97b8U, 0xe75fU, 0xf77eU, 0xc71dU, 0xd73cU, // 22
    0x26d3U, 0x36f2U, 0x0691U, 0x16b0U, 0x6657U, 0x7676U, 0x4615U, 0x5634U, // 23
    0xd94cU, 0xc96dU, 0xf90eU, 0xe92fU, 0x99c8U, 0x89e9U, 0xb98aU, 0xa9abU, // 24
    0x5844U, 0x4865U, 0x7806U, 0x6827U, 0x18c0U, 0x08e1U, 0x3882U, 0x28a3U, // 25
    0xcb7dU, 0xdb5cU, 0xeb3fU, 0xfb1eU, 0x8bf9U, 0x9bd8U, 0xabbbU, 0xbb9aU, // 26
    0x4a75U, 0x5a54U, 0x6a37U, 0x7a16U, 0x0af1U, 0x1ad0U, 0x2ab3U, 0x3a92U, // 27
    0xfd2eU, 0xed0fU, 0xdd6cU, 0xcd4dU, 0xbdaaU, 0xad8bU, 0x9de8U, 0x8dc9U, // 28
    0x7c26U, 0x6c07U, 0x5c64U, 0x4c45U, 0x3ca2U, 0x2c83U, 0x1ce0U, 0x0cc1U, // 29
    0xef1fU, 0xff3eU, 0xcf5dU, 0xdf7cU, 0xaf9bU, 0xbfbaU, 0x8fd9U, 0x9ff8U, // 30
    0x6e17U, 0x7e36U, 0x4e55U, 0x5e74U, 0x2e93U, 0x3eb2U, 0x0ed1U, 0x1ef0U  // 31
};

/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private functions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public functions
 *----------------------------------------------------------------------------*/
/**********************************************************************
 * Name       : CRC_AddByteToCRC
 * Parameters :-CRC =previous CRC
 *            :-byte=byte to be added to CRC
 * Return     :-newly computed CRC value
 * Description: Adds specified byte to CRC and updates CRC.
 * Note       :-Do not use for Flash memory checking !
 *            :-The CRC algorithm has built-in augmentation, meaning
 *            : there is no need to include two bytes of 0 at end
 *            : of CRC compution !
 *********************************************************************/
unsigned int CRC_AddByteToCRC( unsigned int CRC, unsigned char byte )
{
	unsigned int l_newCRC;

    l_newCRC = CRC_Table[ ((CRC >> 8) ^ byte) & 0xFF ] ^ (CRC << 8);

	return l_newCRC;
}

/**********************************************************************
 * Name       : CRC_ComputeAreaCRC
 * Parameters :-sramStartAddr=address from where to start reading bytes
 *            : to be included in CRC
 *            :-nrBytes=number of bytes to be included in CRC
 * Return     :-computed CRC value
 * Description: Computes CRC for an area of bytes.
 * Note       :-This function is for areas located in RAM !
 *            :-Not more than 255 bytes in CRC (nrBytes is uchar) !
 *            :-The CRC algorithm has built-in augmentation, meaning
 *            : there is no need to include two bytes of 0 at end
 *            : of CRC compution for the specified area ! 
 *********************************************************************/
unsigned int CRC_ComputeAreaCRC( unsigned char *startAddr, unsigned char nrBytes )
{
	unsigned int   l_CRC;
	unsigned char  l_byte;
    unsigned char *l_address;
    unsigned char *l_stopAddr;

    // Initialize CRC
    l_CRC = CRC_INIT_VAL;

    // Start putting bytes in CRC
    l_stopAddr = ( startAddr + (nrBytes - 1U) );
    for( l_address = startAddr; l_address <= l_stopAddr; l_address++ )
    {
        l_byte = (*l_address); // Read data byte
        l_CRC  = ( CRC_Table[ ( (l_CRC >> 8) ^ l_byte ) ] ^ (l_CRC << 8) );
    }

    return l_CRC;
}




