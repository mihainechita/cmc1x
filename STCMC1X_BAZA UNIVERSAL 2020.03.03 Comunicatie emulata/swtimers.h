/*******************************************************************************
 * File name  : SWTIMERS.H
 * Author     : Ing. Mihai Nechita  
 * Description: Definition of macros, declaration of data and functions
 *            : for software timers.
 * Note       :-Software timers are updated at each 10ms / 100ms / 1000ms/ 1min
 *            : depending on timer class
 *            : >CLASS 1: 250 us timers: updated each 250 us (tick @ 250us)
 *            : >CLASS 2: 10  ms timers: updated each 10  ms (tick @ 10ms)
 *            : >CLASS 3: 100 ms timers: updated each 100 ms (tick @ 100ms)
 *            : >CLASS 4: 1000ms timers: updated each 1000 ms(tick @ 1s)
 *            : >CLASS 5: 1  min timers: updated each 1min(tick @ 1min):uses RTC
 *            :-For some timers, the load values are not declared here, 
 *            : because are already declared as constant parameters in CPARAM.H
 *            : or as EEPROM parameters in EPARAM.H !
 ******************************************************************************/
#ifndef _SWTIMER_H_
#define _SWTIMER_H_

#include "types.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
// Total number of timers
#define SWTIMER_NO			      				        ( SWTIMER_NO_250US_TIMERS	\
                                                                                + SWTIMER_NO_10MS_TIMERS	\
                                                                                + SWTIMER_NO_100MS_TIMERS	\
                                                                                + SWTIMER_NO_1S_TIMERS		\
                                                                                + SWTIMER_NO_1MIN_TIMERS )
// Timers class offsets
// Note: 10ms class has no offset (first class in timers table) !
#define SWTIMER_10MS_CLASS_OFS                                                  ( 0U + SWTIMER_NO_250US_TIMERS )
#define SWTIMER_100MS_CLASS_OFS						        ( SWTIMER_10MS_CLASS_OFS + SWTIMER_NO_10MS_TIMERS )
#define SWTIMER_1SEC_CLASS_OFS						        ( SWTIMER_100MS_CLASS_OFS + SWTIMER_NO_100MS_TIMERS )
#define SWTIMER_1MIN_CLASS_OFS						        ( SWTIMER_1SEC_CLASS_OFS  + SWTIMER_NO_1S_TIMERS )

// Number of timers from each class (assumes at least 1 timer per class)
#define SWTIMER_NO_250US_TIMERS						        ( SWTIMER_250US_LASTIDX  + 1U )
#define SWTIMER_NO_10MS_TIMERS						        ( SWTIMER_10MS_LASTIDX  + 1U )
#define SWTIMER_NO_100MS_TIMERS						        ( SWTIMER_100MS_LASTIDX + 1U )
#define SWTIMER_NO_1S_TIMERS						        ( SWTIMER_1SEC_LASTIDX  + 1U )
#define SWTIMER_NO_1MIN_TIMERS						        ( SWTIMER_1MIN_LASTIDX  + 1U )

// Timer status values
#define SWTIMER_DISABLED						        0xFF
#define SWTIMER_STOPPED							        0x00
#define SWTIMER_ELAPSED							        SWTIMER_STOPPED
// Macro for checking if a timer elapsed
#define SWTIMER_HASELAPSED( index )				 	        ( SWTIMER_ELAPSED == SWTIMER_GetStatus( index ) )

// Indexes for all timers (relative to timers table)
// Naming Convention: SWTIMER_CLASS_MODULE-WHERE-USED_SHORT-DESCRIPTION


// Indexes for 250us timers
// Note: Use 250us timers for delays in range [0.25-63.5]ms
// in order to have the best possible precision: max.abs.error = 250us !
//#define SWTIMER_250US_NAME						        i
#define SWTIMER_250US_TEST1		                                        0U
#define SWTIMER_250US_TEST2                                                     1U
#define SWTIMER_250US_LASTIDX			                                1U



// Indexes for 10ms timers
// Note: Use 10ms timers for delays in range [40-2500]ms
// in order to have the best possible precision: max.abs.error = 10ms !
//#define SWTIMER_10MS_NAME							( iU + SWTIMER_10MS_CLASS_OFS )
#define SWTIMER_10MS_KEYS_LONGPRESS				                ( 0U + SWTIMER_10MS_CLASS_OFS )
#define SWTIMER_10MS_TESTMAIN					                ( 1U + SWTIMER_10MS_CLASS_OFS )
#define SWTIMER_10MS_LASTIDX						        1U


// Indexes for 100ms timers
// Note: Use 100ms timers for delays in range [2-25]s
// in order to have the best possible precision !
// Max.abs.error = 100ms, max.rel.error = 5% [@2s] !
//#define SWTIMER_100MS_NAME						        ( iU + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_USERINTF_KEYS    					        ( 0U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_BUZZER     				                ( 1U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_MENU_LOAD_TEMPERATURE			                ( 2U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_TEST1                     		                ( 3U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_TEST2                     		                ( 4U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_GET_RTC_TIME      			                ( 5U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_BLINK       			                        ( 6U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_STARTUP_RESET   		                                ( 7U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_BKL					                ( 8U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_TEST10					                ( 9U + SWTIMER_100MS_CLASS_OFS )
#define SWTIMER_100MS_LASTIDX						        9U

// Indexes for 1second timers
// Note: Use 1sec timers for delays in range [20-250]s
// in order to have the best possible precision !
// Max.abs.error = 1s, max.rel.error = 5% [@20s] !
//#define SWTIMER_1SEC_NAME						        (iU + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_USERINTF_ERR	        			        (0U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_USERINTF_ESC				                (1U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_LCD_REINIT                 			        (2U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_FLAME				        	        (3U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_VENT						        (4U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_VG 			                	        (5U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_E11        			                	(6U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_TPRP			                	        (7U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_DHW			                	        (8U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_VENT_E51			                	        (9U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_WAIT_FOR_ERROR                                             (10U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_E10                                                        (11U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_E40                                                        (12U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_BUZZER                                                     (13U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_CT_RESTART                                                 (14U + SWTIMER_1SEC_CLASS_OFS)
#define SWTIMER_1SEC_CT_POWERUP                                                 (15U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_ENDURANCE                                                  (16U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_IONIZARE                                                   (17U + SWTIMER_1SEC_CLASS_OFS) 
#define SWTIMER_1SEC_LASTIDX						        17U
	

// Indexes for 1minute timers
//#define SWTIMER_1MIN_NAME						        (iU + SWTIMER_1MIN_CLASS_OFS)
#define SWTIMER_1MIN_TEST1      					        (0U + SWTIMER_1MIN_CLASS_OFS)
#define SWTIMER_1MIN_TEST2	        				        (1U + SWTIMER_1MIN_CLASS_OFS)
#define SWTIMER_1MIN_LASTIDX						        1U


// Values to be loaded in timers !!!!!!!!!
// ! Warning ! Values MUST be less than 255, because value 0xFF (255) means TIMER_DISABLED !


// Values to be loaded in 250 us timers
#define SWTIMER_250US_TEST1_LOAD        	                                (uint8_t) 1	//  1x0.25ms=0.25ms
#define SWTIMER_250US_TEST2_LOAD                                                (uint8_t) 1	//  1x0.25ms=0.25ms


// Values to be loaded in 10 ms timers
#define SWTIMER_10MS_KEYS_LPRESS_LOAD			                        (uint8_t) 20	//  1 x 10ms
#define SWTIMER_10MS_TESTMAIN_LOAD			                        (uint8_t) 1	//  1 x 10ms


// Values to be loaded in 100 ms timers
#define SWTIMER_100MS_USERINTF_KEYS_LOAD			                (uint8_t) 10	//  1 x 100ms 
#define SWTIMER_100MS_USERINTF_KEYS_LONG_LOAD			                (uint8_t) 40	//  1 x 100ms 
#define SWTIMER_100MS_MENU_LOAD_TEMPERATURE_LOAD                                (uint8_t) 10	//  1 x 100ms 
#define SWTIMER_100MS_SHORT_BUZZER_LOAD				                (uint8_t) 2	//  1 x 100ms 
#define SWTIMER_100MS_LONG_BUZZER_LOAD                  	                (uint8_t) 10	//  1 x 100ms 
#define SWTIMER_100MS_GET_RTC_TIME_LOAD				                (uint8_t) 5	//  1 x 100ms 
#define SWTIMER_100MS_BLINK_LOAD		                                (uint8_t) 5	//  1 x 100ms 
#define SWTIMER_100MS_STARTUP_RESET_LOAD			                (uint8_t) 60	//  1 x 100ms 
#define SWTIMER_100MS_BKL_LOAD                                                  (uint8_t) 5	/* 500 ms */
#define SWTIMER_100MS_TEST10_LOAD				                (uint8_t) 1	//  1 x 100ms 

// Values to be loaded in 1sec timers
#define SWTIMER_1SEC_USERINTF_ERR_LOAD		                                (uint8_t) 17	//  1 x 1sec
#define SWTIMER_1SEC_USERINTF_ESC_LOAD		                                (uint8_t) 240	//  1 x 1sec
#define SWTIMER_1SEC_LCD_REINIT_LOAD	                                        (uint8_t) 1	//  1 x 1sec
#define SWTIMER_1SEC_FLAME_LOAD			                                (uint8_t) 1	//  1 x 1sec
#define SWTIMER_1SEC_VENT_LOAD  			                        (uint8_t) 2	//  1 x 1sec
#define SWTIMER_1SEC_VG_LOAD		                                        (uint8_t) 1	//  1 x 1sec
#define SWTIMER_1SEC_E11_LOAD                                                   (uint8_t) 2	//  1 x 1sec
#define SWTIMER_1SEC_TPRP_LOAD			                	        (uint8_t) 5	//  5 x 1sec
#define SWTIMER_1SEC_DHW_LOAD                                                   (uint8_t) 2	//  10 x 1sec
#define SWTIMER_1SEC_WAIT_FOR_E10_LOAD                                          (uint8_t) 25	//  10 x 1sec
#define SWTIMER_1SEC_WAIT_FOR_E11_LOAD                                          (uint8_t) 25	//  10 x 1sec
#define SWTIMER_1SEC_WAIT_FOR_E20_LOAD                                          (uint8_t) 5	//  10 x 1sec
#define SWTIMER_1SEC_WAIT_FOR_E26_LOAD                                          (uint8_t) 2	//  10 x 1sec  
#define SWTIMER_1SEC_WAIT_FOR_E40_LOAD                                          (uint8_t) 25	//  10 x 1sec  
#define SWTIMER_1SEC_WAIT_FOR_E51_LOAD                                          (uint8_t) 27	//  10 x 1sec 
#define SWTIMER_1SEC_WAIT_FOR_E52_LOAD                                          (uint8_t) 25	//  10 x 1sec  
#define SWTIMER_1SEC_WAIT_FOR_DHW_LOAD                                          (uint8_t) 5	//  10 x 1sec 
#define SWTIMER_1SEC_E10_LOAD                                                   (uint8_t) 15	//  10 x 1sec
#define SWTIMER_1SEC_E40_LOAD                                                   (uint8_t) 6	//  10 x 1sec.
#define SWTIMER_1SEC_CT_RESTART_LOAD                                            (uint8_t) 1	//  10 x 1sec.
#define SWTIMER_1SEC_CT_POWERUP_LOAD                                            (uint8_t) 5	//  10 x 1sec.
#define SWTIMER_1SEC_ENDURANCE_LOAD                                             (uint8_t) 5	//  10 x 1sec. 
#define SWTIMER_1SEC_IONIZARE_LOAD                                              (uint8_t) 15	/*  */                                                                             


// Values to be loaded in 1min timers
#define SWTIMER_1MIN_TEST1_LOAD				                        (uint8_t) 1	//  1 x 1 min
#define SWTIMER_1MIN_TEST2_LOAD				                        (uint8_t) 1	//  1 x 1 min

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/
// Alias for SW-timers data type
#define SWTIMER_TYPE 						uint8_t						// Timer value stored in one byte

/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/
// Counter for testing system clock frequency
// and related interrupts (INT0 and TMR1_OVF)
extern volatile uint8_t SWTIMER_ZCrossCnter1s;									// Counter measuring 1s in 10ms ticks based on external source (inc each zero cross of mains voltage - via INT0 ISR)

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions
 *----------------------------------------------------------------------------*/
extern void				SWTIMER_Initialize		( void );
extern void				SWTIMER_Update			( void );
extern void				SWTIMER_TestIntrClock		( void );
extern void				SWTIMER_Start			( uint8_t timerIndex, SWTIMER_TYPE loadValue );
extern void				SWTIMER_Stop			( uint8_t timerIndex );
extern void				SWTIMER_Disable			( uint8_t timerIndex );
extern SWTIMER_TYPE			SWTIMER_GetStatus		( uint8_t timerIndex );

#endif


