/*******************************************************************************
* File name   : enables.h
* Author      : Ing. Mihai Nechita
* Description : Fisier header pentru citire semnale de feedback de la modulele de simulare
* Date        :        
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

#ifndef __enables_h
#define __enables_h

#include "types.h"
#include "system.h"
/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Public defines
*----------------------------------------------------------------------------*/
#define EN_FS           7U
#define EN_ON_OE        6U
#define EN_AUX          5U
#define EN_VANAGAZ      4U

#define EN_VENT         3U
#define EN_V3C          2U
#define EN_PMPBOILER    1U
#define EN_PMPCENTRALA  0U





/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
* Public (exported) variables
*----------------------------------------------------------------------------*/

extern uint8_t SYS_ENABLES;
extern uint8_t ENABLES_UpdateEnabled; 
extern uint8_t ENABLES_Active;
/*******************************************************************************
*
* Functions (algorithms)
*
*******************************************************************************/

void ENABLES_Update(void);
/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/


#endif //__enables_h
