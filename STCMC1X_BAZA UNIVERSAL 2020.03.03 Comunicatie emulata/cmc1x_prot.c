/*******************************************************************************
* Description : Fisier sursa pentru 
* File name   : cmc1x_prot.c
* Author      : Ing. Mihai Nechita   
* Date	      :           
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// General
#include "stdmacros.h"  // Standard macros

// Services
#include "swtimers.h"    // Software timers
#include "crc.h"        // CRC support

// Communication
#include "usart.h"	// Byte    layer for PC protocol (USART communication driver)
#include "msg.h"      // Message layer for PC protocol
#include "cmc1x_prot.h"    // Headers include 


/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private (static) defines
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public variables
 *----------------------------------------------------------------------------*/
unsigned char PCPROT_CommunicationActive;       // Communication control flag

/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/
// Response to received message (for each received msg a response msg is sent)
static unsigned char PROT_ResponseMsg;
// Status messages index
static unsigned char PROT_StatusMsgsIndex;
// Data used for vectorized (indexed) messages
static unsigned char PROT_VectorIndex;        // Vector index
static unsigned char PROT_VectorLen;          // Vector length
// Counter for received command messages
static unsigned char PROT_CmmdMsgsCounter;

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private functions
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Public functions
 *----------------------------------------------------------------------------*/

/********************************************************************
 * Name       : PCPROT_ReadCommandFromPC
 * Description: Reads command messages sent by PC and executes them.
 *            : Also updates related statistic (no.of processed msgs).
 * Note       :-Some messages are only available in DEBUG version !
 *            :-Some are executed only if EEPROM parameter FWARE_TYPE = TEST(1) !
 * Warning    :-Call from main before PCPROT_WriteMsgsToPC() !
 *            :-This function can not check if all data sent by PC is valid (i.e. manufacturer params
 *            : limits are not known by firmware, but only by PC application).
 *            :-Also, trying to do all checks from firmware can be very resource consuming (memory, execution time, etc).
 *            :-Therefore, this function will perform only those checks which are considered important / critical !
 *            : The remaining checks should be performed by PC app. !
 ********************************************************************/
void CMC1X_PROT_Read( void )
{
	unsigned char l_status;
    unsigned char l_response;
    unsigned int  l_msgID;
    unsigned int  l_msgData;
    unsigned char l_msgDataHi;
    unsigned char l_msgDataLo;

    // Init with previous response (because might not have been sent yet!)
    l_response = PROT_ResponseMsg;

    // Check if a message was successfully received
    // Note: Successfully means no errors detected by lower layer modules, USART & PCMSG !
    l_status = MSG_Get( &l_msgID, &l_msgData );
    if( MSG_STATUS_GET_OK == l_status )
    {
        // Message received (and no errors detected by lower level modules)
        // Update number-of-command-messages statistic at each 10 command messages
    	PROT_CmmdMsgsCounter++;

        // Assume command msg is OK (Set appropriate msg response)
        l_response = PROT_RESP_CMD_OK;

        // Extract msg data
        l_msgDataLo = LO_BYTE( l_msgData );
        l_msgDataHi = HI_BYTE( l_msgData );
        // DECode message fields and EXecute
        switch( l_msgID )
        {
//            case PROT_MSGID_CC: // Communication Control/Enable
//            {
//                // Check if communication pass-key is valid
//                if(  ( PROT_COMM_PASSKEYLO == l_msgDataLo )
//                   &&( PROT_COMM_PASSKEYHI == l_msgDataHi )  )
//                {
//                    SWTIMER_Start( SWTIMER_100MS_PCPROT_DISCOMM, SWTIMER_100MS_PCPROT_DISCOMM_LOAD );
//                }
//                else// Pass-key is not valid
//                {
//                    l_response = PROT_RESP_VAL_INVALID;
//                }
//                break;
//            }
            case PROT_MSGID_SS: // EEPROM Read & Reset
            {
                __delay_cycles(1);
                break;
            }
            
        case PROT_MSGID_ER: // EEPROM Read & Reset
            {
                __delay_cycles(1);
                break;
            }
//            #ifdef __DEBUG_VERSION// Note: In DEBUG version CRC check for manuf params is disabled !
//            case PCPROT_MSGID_OP: // Operation Parameters
//            {
//                // Note: New value of parameter will not be saved in EEPROM !
//                // Value is not checked if in range ! Only PC app. can do that !
//
//                // If msg index is greater or equal to table length, signal error
//                if( l_msgDataHi >= EEPMAP_OPERAREA_USIZE )
//                {
//                    l_response = PCPROT_RESP_IDX_INVALID;
//                }
//                else// Index is OK: modify specified param in table
//                {
//                    EPARAM_ManufTable.OperParams[ l_msgDataHi ] = l_msgDataLo;
//                }
//                break;
//            }
//            #endif
//            case PCPROT_MSGID_IP: // Installer Parameters
//            {
//                // If msg index is greater or equal to table length, signal error
//                if( l_msgDataHi >= EEPMAP_INSTALAREA_USIZE )
//                {
//                    l_response = PCPROT_RESP_IDX_INVALID;
//                }
//                else// Index is OK: modify specified param in table and save in EEPROM
//                {
//                    EPARAM_InstalParams[ l_msgDataHi ] = l_msgDataLo;
//                    EPARAM_InstalParamsChanged = TRUE;
//                    EPARAM_InstalParamsSaveReq = TRUE;
//                }
//                break;
//            }
//            case PCPROT_MSGID_US: // User Settings
//            {
//                // If msg index is greater or equal to block size, signal error
//                if( l_msgDataHi >= EEPMAP_USERBLOCK_USIZE )
//                {
//                    l_response = PCPROT_RESP_IDX_INVALID;
//                }
//                else// Index is OK: modify specified setting in user block and save it in EEPROM
//                {
//                    EPARAM_UserSettings[ l_msgDataHi ] = l_msgDataLo;
//                    SWTIMER_Start( SWTIMER_100MS_PCPROT_USSAVE, SWTIMER_100MS_PCPROT_USSAVE_LOAD );
//                }
//                break;
//            }
//            case PCPROT_MSGID_CS: // Chrono Settings
//            {
//                // If msg index is greater or equal to table length, signal error
//                if( l_msgDataHi >= EEPMAP_CHRONOAREA_SIZE )
//                {
//                    l_response = PCPROT_RESP_IDX_INVALID;
//                }
//                else// Index is OK: modify specified setting and save it in EEPROM
//                {
//                    *( (unsigned char *)EPARAM_ChronoSettings + l_msgDataHi ) = l_msgDataLo;
//                    EPARAM_ChronoRecSaveReq = TRUE;
//                }
//                break;
//            }
//            case PCPROT_MSGID_RQ: // test ReQuest(s)
//            {
//                // If hi byte is equal to 1, then start test-request with priority specified by lo byte !
//                if( l_msgDataHi == 1U ) 
//                {
//                    if( l_msgDataLo <= REQUEST_POS_MAXPRIORITY )
//                    {
//                        REQUEST_Source_PTR = REQUEST_Source_TR;
//                        REQUEST_Source_TR  = (1U << l_msgDataLo);
//                        //l_response = PCPROT_RESP_CMD_OK; (default response when command received)
//                    }
//                    else// Priority of test-request is out of range: signal invalid msg value !
//                    {
//                        //REQUEST_Source_PTR = UNCHANGED;
//                        //REQUEST_Source_TR  = UNCHANGED;
//                        l_response = PCPROT_RESP_VAL_INVALID;
//                    }
//                }
//                else// If hi byte is not 1, then stop test-request
//                {
//                    // Note: Do not clear REQUEST_Source_PTR to be able to detect transitions on REQUEST_Source_TR !
//                    //REQUEST_Source_PTR = UNCHANGED;
//                      REQUEST_Source_TR  = NONE;
//                    //l_response = PCPROT_RESP_CMD_OK; (default response when command received)
//                }
//                break;
//            }
//            case PCPROT_MSGID_MT: // Modulating-elements Test
//            {
//                // Extract test-duty (bring to 0-1000 scale)
//                l_msgData = ( l_msgDataLo * 10U );
//
//                //1 If firmware-type is NORMAL, then consider message ID is invalid (not supported)
//                if( EPARAM_FWARETYPE != FWARE_TYPE_TEST )
//                {
//                    l_response = PCPROT_RESP_ID_INVALID;
//                }
//                //2 If msg index is greater or equal to vector length, signal index error
//                else if( l_msgDataHi >= SYS_MODTESTCMDSVECT_LEN )
//                {
//                    l_response = PCPROT_RESP_IDX_INVALID;
//                }
//                //3 If test-duty greater than max duty (999), signal value error
//                else if( l_msgData > 999U )
//                {
//                    l_response = PCPROT_RESP_VAL_INVALID;
//                }
//                //4 Otherwise (everything OK), update system test-duty with received test-duty
//                else
//                {
//					//l_response = PCPROT_RESP_CMD_OK;
//					#if ( TRUE == EEPVAL_CONDENSING_MODEL )  //-For condensing models only
//					SYS_FanTestPWM = l_msgData;
//					#endif                                   //-For condensing models only
//					#if ( FALSE == EEPVAL_CONDENSING_MODEL ) //-For non-condensing models only
//					SYS_VentTestRPM = l_msgData;
//					#endif                                   //-For non-condensing models only
//                }
//                break;
//            }
//#if ( CXX_BOILER_RESET_FROM_PC == 1U )
//            case PCPROT_MSGID_RR:
//            {            
//              SYS_OpMode = OPMODE_FAILURE;
//              SYS_ResetRequest = TRUE;
//			// Clear error-lock for non-volatile critical errors and save it in EEPROM
//			// ! WARNING ! ???
//			// Before reseting the system, make sure enough time is left
//			// for the EEPROM write sequence to finish !
//			// Otherwise, the error-lock may not be cleared (or may be corrupted)
//		EPARAM_SaveErrorLock( 0u );
//                //while(1);
//                break;
//            }
//#endif
//            //case PCPROT_MSGID_TI: // TBD
//            default:
//            {
//                // Set response to "Invalid message ID" (inexistent command ID)
//                l_response = PCPROT_RESP_ID_INVALID;
//                break;
//            }
        }
    }
//    else if( l_status != PCPROT_STATUS_NONE ) // Errors detected by PC comm. lower layers (modules)
//    {
//        // Update and save number of errors related to messages from PC
//        EPARAM_IncAndSaveStatistic( EPARAM_ERRORMSGSNO_IDX ); 
//
//        // Decode error flags and set appropriate message response
//        if( HI_BITS( l_status, (PCPROT_STATUS_BL_FE | PCPROT_STATUS_BL_DOR) ) )
//        {
//            l_response = PCPROT_RESP_CHAR_ERROR;     // Byte-level errors: FE / DOR
//        }
//        else if( HI_BITS( l_status, PCPROT_STATUS_ML_OE ) )
//        {
//            l_response = PCPROT_RESP_BUF_OVERRUN;    // More than one message received
//        }
//        else if( HI_BITS( l_status, PCPROT_STATUS_ML_SE ) )
//        {
//            l_response = PCPROT_RESP_SYNTAX_ERR;     // Message syntax error
//        }
//        else if( HI_BITS( l_status, PCPROT_STATUS_ML_CE ) )
//        {
//            l_response = PCPROT_RESP_CRC_ERROR;      // Message CRC error (message corrupted)
//        }
//        else
//        {
//            // Nothing: Should be impossible to get here !
//        }
//    }
//    else
//    {
//        // Nothing: No message received !
//    }
//
//    // Check if user-settings modified via PC-interface must be saved
//    if( SWTIMER_HASELAPSED( SWTIMER_100MS_PCPROT_USSAVE ) )
//    {
//        SWTIMER_Disable( SWTIMER_100MS_PCPROT_USSAVE );
//        EPARAM_UserSettingsSaveReq = TRUE;
//    }
//    else
//    {
//        // Nothing: Wait for related timeout to elapse !
//    }

    // Update message response
    PROT_ResponseMsg = l_response;
}
//
///********************************************************************
// * Name       : PCPROT_SendEEPROMtoPC
// * Description: Reads content of entire EEPROM and sends it to PC one byte at a time (one message, one EEPROM byte)
// * Note       :
// * Warning    :-This is a blocking function, meaning it takes a lot of time to execute (a few seconds) !
// *            :-Only call when in FAILMODE, when an "EEPROM Read" message has been received !
// *            :-Assumes all execution elements have been stopped as this is a blocking function and we really do not want
// *            : to have a situation where execution elements are not OFF and this blocking function is called !
// *            : Anyway, safety processor P2 shall stop execution elements after atmost 1second if P1 processor (this one)
// *            : will stop communication on TWI bus !
// ********************************************************************/
//void PCPROT_SendEEPROMtoPC( void )
//{
//	unsigned char l_byte;
//	unsigned int  l_eepromAddr;
//	
//    // Wait if EEPROM memory is busy (wait if an EEPROM-write cycle is on-going)
//    while( EEPROM_ISBUSY() )
//    {
//        __watchdog_reset();       // Clear WDT to avoid a MCU reset
//    }
//
//    // Read EEPROM memory byte-by-byte and send it byte-by-byte to PC
//    l_eepromAddr = EEPROM_FIRST_ADDRESS;
//    do
//	{
//		//__watchdog_reset(); // Clear WDT to avoid a MCU reset
//		
//        EEPROM_READBYTE( l_eepromAddr, l_byte ); // Read EEPROM byte
//		
//		//__watchdog_reset(); // Clear WDT to avoid a MCU reset
//		
//        // Wait until USART TX is ready and then send message with byte address
//        while( PCMSG_Put( PCPROT_MSGID_EA, l_eepromAddr ) != PCMSG_STATUS_PUT_OK )
//        {
//            __watchdog_reset();   // Clear WDT to avoid a MCU reset
//        }
//		
//		//__watchdog_reset(); // Clear WDT to avoid a MCU reset
//		
//        // Wait until USART TX is ready and then send message with byte value
//        while( PCMSG_Put( PCPROT_MSGID_EV,  (unsigned int)l_byte ) != PCMSG_STATUS_PUT_OK )
//        {
//            __watchdog_reset();   // Clear WDT to avoid a MCU reset
//        }
//		
//        // Move to next EEPROM location
//        l_eepromAddr++;
//    }
//    while( l_eepromAddr <= EEPROM_LAST_ADDRESS );
//}
