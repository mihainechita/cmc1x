/*******************************************************************************
* Description : Fisier sursa pentru citire intrari
* File name   : inputs.c
* Author      : Ing. Mihai Nechita             
* Date        : 08/11/2018         
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)
#include <inavr.h>      // Compiler intrinsic functions (i.e. __no_operation)

// System
#include "stdmacros.h"    
#include "swtimers.h"
#include "keys.h"    
#include "io_drv.h"



/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned char KEYS_UpdateEnabled;  // Signals keys update event
unsigned char KEYS_Pressed;        // Keys pressed event
unsigned char KEYS_Released;       // Keys released event 
/*------------------------------------------------------------------------------
* Private (static) variables
*----------------------------------------------------------------------------*/
static unsigned char KEYS_PrevSample;
static unsigned char KEYS_CrtSample;

static unsigned char KEYS_PrevStatus;
static unsigned char KEYS_CrtStatus;

static unsigned char KEYS_LongPressEnabled;

static unsigned char KEYS_MultipleButtonsPressed = FALSE;
/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/





 


/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/

/******************************************************************************
* Functia KEYS_Update se executa o data la 20 de ms; toate tastele au debounce 
* time de 40 ms  

*****************************************************************************/     
void KEYS_Update(void)
{
    // variabile pentru butoane
    unsigned char l_bitSumKeys;
    unsigned char l_bitIdxKeys;
    unsigned char l_crtStatusKeys;
    unsigned int l_KEYS_Buffer=0;
    unsigned int l_TAST_EN_Buffer=0;
    unsigned char l_KEYS=0;

    
    
    if( KEYS_UpdateEnabled == TRUE)
    {
//        SET(PORTA,EXT_RES_IO);
        // resetare flag; informatie consumata 
        KEYS_UpdateEnabled = FALSE;
        
        l_TAST_EN_Buffer=SYS_TAST_EN_Data;              // stocare in variabila locala
        
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        l_KEYS_Buffer=l_KEYS_Buffer<<1;
        l_KEYS_Buffer|= (l_TAST_EN_Buffer & 0x4000)>>14;
        l_TAST_EN_Buffer=l_TAST_EN_Buffer<<2;
        
        
        l_KEYS=(unsigned char)l_KEYS_Buffer;

        l_KEYS=~l_KEYS;
        
        /* incarcare variabile cu informatii filtrate */
        KEYS_PrevSample = KEYS_CrtSample;
        
        /* incarcare variabile cu informatii filtrate */            
        KEYS_CrtSample  = l_KEYS;
  
        
        /* detectie scurtcircuit pe taste */
        if( SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_1SEC_USERINTF_ERR ) )
        {
            SWTIMER_Disable( SWTIMER_1SEC_USERINTF_ERR );
            SYS_UserIntfError = TRUE;
        }
        
        
        /* verificare daca butoanele sunt stabile/ nu au bounce */
        if( KEYS_PrevSample == KEYS_CrtSample)
        {
            /* Update stare taste */
            KEYS_PrevStatus = KEYS_CrtStatus;
            KEYS_CrtStatus = KEYS_CrtSample;
            
            if( KEYS_PrevStatus != KEYS_CrtStatus )
            {
                /* zona de combinare de taste speciale */
                if ( ( ( KEYS_UP_DOWN) == KEYS_CrtStatus ) && ( KEY_UP == KEYS_PrevStatus ) )
                {
                    KEYS_Pressed  = KEYS_CrtStatus;
                    KEYS_Released = 0x00u;
                }
                else
                {
                    /* initializare sumator biti */
                    l_bitSumKeys = 0;
                    
                    /* initializare variabila temporara care salveaza continutul KEYS_CrtStatus */
                    l_crtStatusKeys = KEYS_CrtStatus;
                    
                    /* sumare biti de 1 din KEYS_CrtStatus */
                    for ( l_bitIdxKeys = 0; l_bitIdxKeys < 8; l_bitIdxKeys++ )
                    {
                        l_bitSumKeys += ( l_crtStatusKeys & 0x01u );
                        l_crtStatusKeys = l_crtStatusKeys >> 1u;
                    }
                    
                    // Check events with maximum one button pressed
                    if ( ( l_bitSumKeys <= 1 ) && ( KEYS_MultipleButtonsPressed == FALSE ) )
                    {
                        KEYS_Pressed  = KEYS_CrtStatus;
                        KEYS_Released = KEYS_PrevStatus;
                        
                        //enable 1 second delay used for power switch and for initial increment/decrement for continuous intrement/decrement
                        if( KEY_LEFT_PRESSED  || 
                            KEY_RIGHT_PRESSED ||  
                            KEY_UP_PRESSED    || 
                            KEY_DOWN_PRESSED  ||
                            KEY_BACK_PRESSED  ||
                            KEY_MENU_PRESSED  ||
                            KEY_ENTER_PRESSED ||
                            KEY_OK_PRESSED)
                            
                        {
                            SWTIMER_Start( SWTIMER_100MS_USERINTF_KEYS, SWTIMER_100MS_USERINTF_KEYS_LOAD );
                        }
                    }
                    else
                    {
                        if ( ( l_bitSumKeys <= 2 ) && ( KEYS_MultipleButtonsPressed == FALSE ) )
                        {
                            KEYS_Pressed  = KEYS_CrtStatus;
                            KEYS_Released = KEYS_PrevStatus;
                            //enable 3.5 second delay used for power switch and for initial increment/decrement for continuous intrement/decrement
                            if( KEY_RIGHT_UP_PRESSED)
                            {
                                // while(1);
                                SWTIMER_Start( SWTIMER_100MS_USERINTF_KEYS, SWTIMER_100MS_USERINTF_KEYS_LOAD );
                            }
                        }
                        // Ignore other multi pressed buttons events
                        else
                        {
                            if(( l_bitSumKeys <= 3 ) && ( KEYS_MultipleButtonsPressed == FALSE ) )
                            {
                                KEYS_Pressed  = KEYS_CrtStatus;
                                KEYS_Released = KEYS_PrevStatus;
                                
                                if( KEY_RIGHT_UP_DOWN_PRESSED)
                                {
                                    // while(1);
                                    SWTIMER_Start( SWTIMER_100MS_USERINTF_KEYS, SWTIMER_100MS_USERINTF_KEYS_LOAD );
                                }  
                            }
                            else
                            {
                                KEYS_Pressed  = 0x00u;
                                KEYS_Released = 0x00u;
                                KEYS_MultipleButtonsPressed = TRUE;
                            }
                        }
                    }
                    // Ignore other multi pressed buttons events
                }
                
                KEYS_LongPressEnabled = FALSE;
                //this sw timer is used to go back from other user-interface states in user-mode state after 30/60 sec of interface inactivity
                SWTIMER_Start( SWTIMER_1SEC_USERINTF_ESC, SWTIMER_1SEC_USERINTF_ESC_LOAD );   // TODO: ESC
                if( KEYS_CrtStatus != 0x00u )
                {
                    SWTIMER_Start( SWTIMER_1SEC_USERINTF_ERR, SWTIMER_1SEC_USERINTF_ERR_LOAD );
                }
            }
            
            if ( KEYS_CrtStatus == 0x00u )
            {
                KEYS_MultipleButtonsPressed = FALSE;
            }
            // activare backlight
            //            if( KEYS_CrtStatus != 0x00u )
            //            {
            //                //enable delay for back-light sleep ( 3 sec )
            //                //back-light will go to sleep state only after 3 seconds of keyboard inactivity
            //                //if keys are kept pressed, this timer is restarted
            //                SWTIMER_Start( SWTIMER_100MS_USERINTF_BKLOFF, SWTIMER_100MS_USERINTF_BKLOFF_LOAD );
            //            }
            
            //enable keys long-press after one second of key pressed
            if( ( SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_USERINTF_KEYS ) ) )           
            {
                KEYS_LongPressEnabled = TRUE;
                //while(1); // !!! TBD !!! ???
                
                //don't disable timer for power key here because is read in USERINTF.c module
                //for other key, disable timer
//                SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
            }
            
            
            // verificare apasare lunga (simulat ca apasari scurte la fiecare TLONGPRESS ms)
            if( (TRUE == KEYS_LongPressEnabled) )
            {
                if( SWTIMER_DISABLED == SWTIMER_GetStatus( SWTIMER_10MS_KEYS_LONGPRESS ) )
                {
                    // Start counting the long-press time (from the last keys change)
                    SWTIMER_Start( SWTIMER_10MS_KEYS_LONGPRESS, SWTIMER_10MS_KEYS_LPRESS_LOAD );
                }
                else// If the long-press time elapsed, then signal a long-press event
                {
                    if( SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_10MS_KEYS_LONGPRESS ) )
                    {
                        KEYS_Pressed = KEYS_CrtStatus;
                        // Restart counting the long-press time
                        SWTIMER_Start( SWTIMER_10MS_KEYS_LONGPRESS, SWTIMER_10MS_KEYS_LPRESS_LOAD );
                    }
                }
            }
            else
            {
                SWTIMER_Disable( SWTIMER_10MS_KEYS_LONGPRESS );
            }
        }              
    }//if( KEYS_PrevSample == KEYS_CrtSample)
    else
    {
        SWTIMER_Disable( SWTIMER_1SEC_USERINTF_ERR );
//        SWTIMER_Disable( SWTIMER_100MS_USERINTF_KEYS );
    }
    
    
    
    
}  






























