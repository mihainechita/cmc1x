/*******************************************************************************
* Description : Fisier sursa pentru citire si scriere registre de shiftare
* File name   : io_drv.c
* Author      : Ing. Mihai Nechita   
* Date	      : 05.12.2018           
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>      
#include <inavr.h>      

/* System */
#include "io_drv.h"     
#include "system.h"
#include "stdmacros.h"
#include "lcdapi.h"
#include "swtimers.h"
#include "lcd.h"
#include "lcdapi.h"
#include "isr.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*------------------------------------------------------------------------------
* Private defines
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Type definitions
*----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
* Public variables
*----------------------------------------------------------------------------*/
unsigned int SYS_TAST_EN_Data=0U;                   // Variabila ce contine valoarea tastelor si semnalelor de enable 
                                                   // citite de catre IO_Driver direct de la hardware
volatile unsigned char SYS_CMD_Relay_High=0x00U;                  // Variabila principala cu care se activeaza/dezactiveaza releele
volatile unsigned char SYS_CMD_Relay_Low=0x00U;                  // Variabila principala cu care se activeaza/dezactiveaza releele

volatile unsigned char PORTA_Buffer=0U;
volatile unsigned char PORTB_Buffer=0U;
volatile unsigned char PORTC_Buffer=0U;
volatile unsigned char PORTD_Buffer=0U;
/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/

void IO_DRV_Backlight_Toggle()
{
    static uint8_t IO_DRV_Backlight_State=0;
    

    switch(IO_DRV_Backlight_State)
    {
      case ON: 
        {
            BKL_ON;
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BKL ))
            {
                IO_DRV_Backlight_State = OFF ;
                SWTIMER_Start(SWTIMER_100MS_BKL,SWTIMER_100MS_BKL_LOAD); 
            }
            break;
        }
        
       case OFF: 
        {
            BKL_OFF;
            if(SWTIMER_ELAPSED == SWTIMER_GetStatus( SWTIMER_100MS_BKL ))
            {
                IO_DRV_Backlight_State = ON ;
                SWTIMER_Start(SWTIMER_100MS_BKL,SWTIMER_100MS_BKL_LOAD); 
            }
            break;
        } 
        
    }
    
    
}

/*------------------------------------------------------------------------------
* Public (exported) functions 
*----------------------------------------------------------------------------*/
void IO_DRV_Update(void)
{
    
        /* valoare ce trebuie incarcata in registrele de SIPO ( CMD ), 16 biti */
    uint8_t l_CMD_data=0U;                             
    uint8_t l_CMD_Relay_High=0U,l_CMD_Relay_Low=0U; 
    uint16_t  l_Tast_En=0U;    
    
    /* valoarea citita de la registele PISO ( tastatura si feedback ) */
    uint8_t l_Tast_En_Buffer=0U;   

        if(ISR_INT0_Refresh == 0U)
    {
        /* Trimitere relee  */
        
        l_CMD_Relay_High = SYS_CMD_Relay_High;
        l_CMD_Relay_Low = SYS_CMD_Relay_Low;
        
        
        /******************************************************************************/              
        /* Scriere bit 15 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x80U);
        PORTB=l_CMD_data>>7U;
        RISING_EDGE_CLOCK;
  	
        /******************************************************************************/
        
        /******************************************************************************/             
        /* Scriere bit 14 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x40U);
        PORTB=l_CMD_data>>6U;
        RISING_EDGE_CLOCK;
        /******************************************************************************/
        
        /******************************************************************************/         
        /* Scriere bit 13 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x20U);
        PORTB=l_CMD_data>>5U;
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/            
        /* Scriere bit 12 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x10U);
        PORTB=l_CMD_data>>4U;
        RISING_EDGE_CLOCK;    
        /******************************************************************************/       
        
        /******************************************************************************/              
        /* Scriere bit 11 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x08U);
        PORTB=l_CMD_data>>3U;
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/           
        /* Scriere bit 10 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x04U);
        PORTB=l_CMD_data>>2U;
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/           
        /* Scriere bit 9 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x02U);
        PORTB=l_CMD_data>>1U;
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/             
        /* Scriere bit 8 registre CMD */
        l_CMD_data=(l_CMD_Relay_High & 0x01U);
        PORTB=l_CMD_data ;
        RISING_EDGE_CLOCK;      
        /******************************************************************************/        
        
        
        /******************************************************************************/        
        /* Scriere bit 7 registre CMD */
        l_CMD_data=(l_CMD_Relay_Low & 0x80U);
        PORTB=l_CMD_data>>7U;
     	RISING_EDGE_CLOCK;	
        /******************************************************************************/        
        
        
        /******************************************************************************/        
        /* Scriere bit 6 registre CMD */
        l_CMD_data=(l_CMD_Relay_Low & 0x40U);
        PORTB=l_CMD_data>>6U;
     	RISING_EDGE_CLOCK;		
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 5 registre CMD */
        l_CMD_data=(l_CMD_Relay_Low & 0x20U);
        PORTB=l_CMD_data>>5U;
     	RISING_EDGE_CLOCK;	
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 4 registre CMD */
        l_CMD_data=(l_CMD_Relay_Low & 0x10U);
        PORTB=l_CMD_data>>4U;
     	RISING_EDGE_CLOCK;		
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 3 registre CMD */
        l_CMD_data=(l_CMD_Relay_Low & 0x08U);
        PORTB=l_CMD_data>>3U;
     	RISING_EDGE_CLOCK;	
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 2 registre CMD */
        l_CMD_data=( l_CMD_Relay_Low & 0x04U);
        PORTB=l_CMD_data>>2U;
     	RISING_EDGE_CLOCK;		
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 1 registre CMD */
        l_CMD_data=( l_CMD_Relay_Low & 0x02U);
        PORTB=l_CMD_data>>1U;
     	RISING_EDGE_CLOCK;			
        /******************************************************************************/         
        
        /******************************************************************************/        
        /* Scriere bit 0 registre CMD */
        PORTB=( l_CMD_Relay_Low & 0x01U );
     	RISING_EDGE_CLOCK;
        /******************************************************************************/    
        /* puls pe linia de enable shift register CMD */
        STROBE_CMD;             
    }
    else
    {
        /* Citire taste  
        Setare P/S pentru stocare P1-P8 in registrele interne ale IN1 si IN2
        Trecere in mod Paralel */
        SET_PARALLEL_MODE_MC14014;
        RISING_EDGE_CLOCK;
        /* Trecere in mod Serial */
        SET_SERIAL_MODE_MC14014;
        RISING_EDGE_CLOCK;
        
	/* citire cate 2 biti de la registrele PISO, primul bit reprezinta continutul de pe PC7(EN_IN) 
        iar al doilea reprezinta continutul de pe PC6(TAST_IN)
        La decodificare, bitii pari reprezinta bitii EN_IN, iar cei impari bitii TAST_IN
        Shiftarea continutului de pe PINC se face pentru a aduce valorile de pe pinii PC7, PC6 pe
        pozitiile bitilor 1 si 0 din variabila l_Tast_En_Buffer;
        Variabila l_Tast_En este shiftata stanga cu 2 biti pentru a face loc
        celor 2 biti noi ce trebuie incarcati pe pozitiile 1 si 0 */
        
        /******************************************************************************/        
        /* Citire biti 7 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/
        
        /******************************************************************************/        
        /* Citire biti 6 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/
        
        /******************************************************************************/        
        /* Citire biti 5 TASTE + ENABLES */ 
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/        
        /* Citire biti 4 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/       
        
        /******************************************************************************/        
        /* Citire biti 3 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/        
        /* Citire biti 2 TASTE + ENABLES */ 
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/        
        /* Citire biti 1 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        /******************************************************************************/        
        /* Citire biti 0 TASTE + ENABLES */
        l_Tast_En_Buffer=(PINC>>6U);
        l_Tast_En=(l_Tast_En<<2U)|l_Tast_En_Buffer;       
        RISING_EDGE_CLOCK;
        /******************************************************************************/        
        
        SYS_TAST_EN_Data=l_Tast_En;
    }
    
    /* Update porturi controller */    
    PORTA=PORTA_Buffer;
    PORTC=PORTC_Buffer;
    PORTD=PORTD_Buffer; 
}




















































