/*******************************************************************************
* Description : Fisier sursa pentru comunicatia USART
* File name   : usart.c
* Author      : Ing. Mihai Nechita             
* Copyright   : (C) 2019 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*------------------------------------------------------------------------------
 * Includes
 *----------------------------------------------------------------------------*/
// Compiler
#include <ioavr.h>      // Compiler definitions for I/O registers (i.e. PORTD, PC7, EECR)

// General
#include "stdmacros.h"  // Standard macros

// Communication
#include "usart.h"		// Byte layer for PC protocol (USART communication driver)
#include "isr.h"
#include "msg.h"
#include "crc.h"
#include "swtimers.h"
#include "system.h"
#include "menu.h"
#include "io_drv.h"
#include "pc_prot.h"
/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private (static) defines
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public variables
 *----------------------------------------------------------------------------*/

/* Structuri de receptie date USART */
/* Buffer de receptie USART */
volatile uint8_t USART_RX_Buffer[ USART_RX_BUFFER_SIZE ];	

/* Buffer de receptie USART */
volatile uint8_t USART_RX_Head;          

/* Buffer de receptie USART */
volatile uint8_t USART_RX_Tail;

/* Buffer de receptie USART */
volatile uint8_t USART_RX_Buffer_Length;


/* Structuri de transmisie date USART */
/* Buffer de transmisie USART */
volatile uint8_t USART_TX_Buffer[ USART_TX_BUFFER_SIZE ];	

/* Buffer de transmisie USART */
volatile uint8_t USART_TX_Head;          

/* Buffer de transmisie USART */
volatile uint8_t USART_TX_Tail;

/* Buffer de transmisie USART */
volatile uint8_t USART_TX_Buffer_Length;

/* Flag ce semnalizeaza functiei USART_Start_Transmit daca intreruperea trimite
 date sau nu, pornirea intreruperii se da doar daca aceasta este oprita */
volatile uint8_t USART_TX_Interrupt_ON=0;


/* Index pentru transmisie ; temporar delete after finalizing */
volatile uint8_t USART_TX_Buffer_Index;
volatile uint8_t USART_StatusFlags;                       // Status flags

/*------------------------------------------------------------------------------
 * Private (static) variables
 *----------------------------------------------------------------------------*/


/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Private functions
 *----------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
 * Public functions
 *----------------------------------------------------------------------------*/

 /********************************************************************
 * Name       : USART_Initialize
 * Parameters : None
 * Description: Configuration of USART unit
 * Warning    :-This function shall be called only once: after reset!
 *            :-Do not use TXC interrupt ! Use UDRE interrupt instead!
 *            : PC seams to miss/swap bytes when TXC interrupt used !
 ********************************************************************/
void USART_Initialize( void )
{
	//1 Configure USART clock & baudrate
	// Set speed/baud-rate and speed type (normal/double speed)
    // Note:-UBBRH is 0 for any speed if clock is 4MHz !
	//UBRRH = ( uint8_t )( USART_BAUD_19200_BPS >> 8 ); // Not needed ! Commented to avoid lint warns !
	UBRRL = ( uint8_t )USART_BAUDU1X_19200_BPS;         // 19200 bps @
	UCSRA = ( 0U << U2X );                                    // @ normal-speed


	//2 Configure frame format
	//  Frame: 1 start + 8 data + 1 stop (10 bits / frame)
	UCSRC =   ( 1U << URSEL )   // Select UCSRC register
	        | ( 1U << UCSZ1 )       // setare 8 biti (UCSZ1 si UCSZ0 trebuiesc setati pe 1)
	        | ( 1U << UCSZ0 );      

	//3 Configure transmitter and receiver
	UCSRB =   ( 1U << RXEN  ) 	// Enable USART receiver
			| ( 1U << RXCIE ) 	// with interrupt triggering on RX-complete
			| ( 1U << TXEN  );	// Enable USART transmitter
}

/*************************************************************************
 * Name       : USART_Transmit
 * Parameters : Data to be sent (source string)
 * Description:-Writes data in transmission buffer if empty
 *            :-Starts USART transmission
 * Return     : Transmit status: NOT_OK=buffer not empty,OK=TX buffer empty
 *************************************************************************/
void USART_Transmit( const uint8_t *srcString  )
{
    uint8_t l_USART_TX_Head;
    uint8_t l_index=0;
    
    l_USART_TX_Head = USART_TX_Head;
    
    for( l_index = 0U; l_index < 13; l_index++ )
    {
         
        USART_TX_Buffer[ l_USART_TX_Head + l_index ] = srcString[ l_index ];
    }
    
    l_USART_TX_Head = ( l_USART_TX_Head + 13 ) % USART_TX_BUFFER_SIZE; 

    
    
    /* Cat timp bufferul contine elemente, doar daca intreruperea nu trimite deja mesaje, activez ISR de transmisie */
    if((l_USART_TX_Head != USART_TX_Tail ) && USART_TX_ISREADY())
    {
        SET( UCSRB, UDRIE );
    }
    
    USART_TX_Head = l_USART_TX_Head; 
    
}

/*******************************************************************************
 * Name       : USART_Receive
 * Parameters : String where data from Reception buffer is stored
 * Description:-Reads USART RX buffer and writes data in "destString"
 *            :-Clears reception status flags and RX-buffer index
 *            :-Treats errors by flushing RX buffer
 * Return     : Reception status flags
 * Warning    :-This function clears the Reception status flags !
 *            :-The buffer is read and written to the destination string
 *            : ONLY if a msg has been completely recv-ed AND no error occured !
 ******************************************************************************/

void USART_Receive( void )
{
    uint8_t l_RX_Buffer_Length=0U;
    uint8_t l_RX_head=0U;
    uint16_t  l_storedCRC;
    uint16_t  l_actualCRC;
    uint8_t l_msgField[2U];
    uint8_t Message[13U];
    uint16_t Message_ID=0U;
//    uint16_t Message_Data=0;
//    static uint16_t Critical_Error=0U;
//    static uint16_t Normal_Error=0U;

    
    l_RX_head=USART_RX_Head;
    
    
/* Determinare lungime buffer (numar de elemente existente in coada) */    
    if(l_RX_head >= USART_RX_Tail)
    {
        /* */
        l_RX_Buffer_Length = (l_RX_head - USART_RX_Tail);
    }
    else
    {
        l_RX_Buffer_Length = (USART_RX_BUFFER_SIZE + l_RX_head - USART_RX_Tail);
    }
    
/* Verific mesajele din coada pana termin de parcurs toata coada */    
    for( ; l_RX_Buffer_Length > 0U ; l_RX_Buffer_Length -- )
    {
        /* Daca detectez EOM (<) char */        
        if( USART_RX_Buffer[USART_RX_Tail] == USART_EOM_CHAR)
        {
            /* Detectie sintaxa mesaj: daca Data End (#) si SOM start of message (>) se afla in pozitiile corecte */            
            if(  ( '#' ==  USART_RX_Buffer[(USART_RX_BUFFER_SIZE + USART_RX_Tail - 5U) % USART_RX_BUFFER_SIZE] ) && ( USART_SOM_CHAR ==  USART_RX_Buffer[(USART_RX_BUFFER_SIZE + USART_RX_Tail - 12U)%USART_RX_BUFFER_SIZE] ) )
            {
                /* Parcurg 13 elemente de la pozitia in care am gasit EOM */                
                for(uint8_t i=0U;i<13U;i++)
                {
                    /* Transfer datele intr-un byte local pentru separare ID si date mesaj */
                    Message[i] = USART_RX_Buffer[( USART_RX_BUFFER_SIZE + USART_RX_Tail - 12U + i ) % USART_RX_BUFFER_SIZE];
                }
                /* Conversie din ASCII in hex mesaj */
                MSG_ASCII2Hex( 16U, (Message + MSG_CRC_OFFSET), l_msgField );               
                l_storedCRC = ( ( (uint16_t)l_msgField[0U] ) << 8U ) + l_msgField[1U];
                l_actualCRC = CRC_ComputeAreaCRC( &Message[0U], (MSG_DEN_OFFSET + 1U) );
                
                /* Daca mesajul este corupt este ignorat */
                if( l_storedCRC != l_actualCRC )
                {
                    /* Mesaj corupt */
                }
                else
                {
                    /* Decodificare ID mesaj */
                    HI_BYTE2SET( Message_ID ) = Message[1];  // ID field: 1st byte
                    LO_BYTE2SET( Message_ID ) = Message[2];  // ID field: 2nd byte
                    
                    //4.2 Extract data from data-field
                    //    (Big-Endian storage in message -> Little-Endian storage in memory)
                    MSG_ASCII2Hex( 16, (Message + MSG_DATA_OFFSET), l_msgField );
                    
                   PC_Command_ID = Message_ID;
                   PC_Command_DATA[0U] = l_msgField[0U];
                   PC_Command_DATA[1U] = l_msgField[1U];
                   PC_Prot_Decode_CMD();            
                    
                }
                
            }
            else 
            {
                /* Sintaxa incorecta */
            }
            USART_RX_Buffer [ USART_RX_Tail ] = 0; 
        }
        
        /* Incrementare coada */
        USART_RX_Tail = ( USART_RX_Tail + 1U ) % USART_RX_BUFFER_SIZE;
        
        
    }
    
}


