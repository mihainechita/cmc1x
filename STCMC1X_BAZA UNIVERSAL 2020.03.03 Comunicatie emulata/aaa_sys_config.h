/*******************************************************************************
* Description : Fisier header pentru definuri de sistem folosite la precompilare
                Precompilatorul precompileaza fisierele in ordine alfabetica
* File name   : aaa_sys_config.h
* Author      : Ing. Mihai Nechita   
* Date	      : 2019.11.16         
* Copyright   : (C) 2018 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/
#ifndef __AAA_SYS_CONFIG_H
#define __AAA_SYS_CONFIG_H


/*******************************************************************************
 *
 * Data structures
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public defines
 *----------------------------------------------------------------------------*/
#define STAND_FORTATE           0
#define STAND_CONDENSATIE       1   
   
#define STAND_MODEL             STAND_FORTATE


#define RG24064         0
#define RG12864         1

#define SYS_LCDMODEL    RG24064           
/* #define SYS_LCDMODEL    RG12864    */

#define PCF8523T        0
#define DS1307          1

#define SYS_RTC_MODEL    PCF8523T
/* #define SYS_RTC_MODEL    DS1307     */

#define SYS_ISR_SEND_WITH_OFFSET_ROTATION       TRUE
/* #define SYS_ISR_SEND_WITH_OFFSET_ROTATION       FALSE  */

/*------------------------------------------------------------------------------
 * Type definitions
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Public (exported) variables
 *----------------------------------------------------------------------------*/

/*******************************************************************************
 *
 * Functions (algorithms)
 *
 ******************************************************************************/

/*------------------------------------------------------------------------------
 * Public (exported) functions 
 *----------------------------------------------------------------------------*/

/* __AAA_SYS_CONFIG_H */
#endif

