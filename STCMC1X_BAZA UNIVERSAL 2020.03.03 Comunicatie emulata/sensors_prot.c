/*******************************************************************************
* Description : Fisier sursa pentru protocolul de comunicatie cu placa de senzori
* File name   : sensors_prot.c
* Author      : Ing. Mihai Nechita   
* Date	      : 2020.02.27          
* Copyright   : (C) 2020 S.C. Kober S.R.L. All rights reserved.
*             : The information contained herein is property of S.C. Kober S.R.L.
*             : The use, copying, transfer or disclosure of such information
*             : is prohibited except by express written permission of S.C. Kober S.R.L.
******************************************************************************/

/*******************************************************************************
* Includes
*----------------------------------------------------------------------------*/
/* Compiler */
#include <ioavr.h>
#include <inavr.h>

/* System */
#include "isr.h"
#include "sensors_prot.h"


/*******************************************************************************
*
* Data structures
*
******************************************************************************/

/*******************************************************************************
* Private defines
******************************************************************************/

/*******************************************************************************
* Type definitions
******************************************************************************/

/*******************************************************************************
* Public variables
******************************************************************************/
/* circular buffer for receiver process */
uint8_t SENSORS_PROT_RX_Buffer[ SENSORS_PROT_RX_BUFFER_LEN ];
/* control variables of circular buffer for receiver process */
uint8_t SENSORS_PROT_RX_Buffer_Head=0U, SENSORS_PROT_RX_Buffer_Tail=0U;
	
/* circular buffer for transmitter process */
uint8_t SENSORS_PROT_TXBuffer[ SENSORS_PROT_TX_BUFFER_LEN ];
/* control variables of circular buffer for transmitter process */
uint8_t SENSORS_PROT_TX_Buffer_Head=0U, SENSORS_PROT_TX_Buffer_Tail=0U;



/*******************************************************************************
 * Private (static) variables
******************************************************************************/

/*******************************************************************************
*
* Functions (algorithms)
*
******************************************************************************/



/*******************************************************************************
* Public (exported) functions 
******************************************************************************/

/*******************************************************************************
* Initializare variabile buffere de comunicatie 
******************************************************************************/
void SENSORS_PROT_Init( void )
{
	SENSORS_PROT_RX_Buffer_Head = 0U;
	SENSORS_PROT_RX_Buffer_Tail = 0U;
	SENSORS_PROT_TX_Buffer_Head = 0U;
	SENSORS_PROT_TX_Buffer_Tail = 0U;
}

/*******************************************************************************
* Folosita in cazul in care se pierde sincronizarea  
******************************************************************************/
void SENSORS_PROT_RX_Flush( void )
{
	SENSORS_PROT_RX_Buffer_Head = 0U;
	SENSORS_PROT_RX_Buffer_Tail = 0U;
}

	// check the receiver circular buffer used space (number of locatios that are used)

/*******************************************************************************
* Verificare numar de locatii folosite din bufferul de receptie 
******************************************************************************/
uint8_t SENSORS_PROT_RX_Check ( void )
{
	return ( (SENSORS_PROT_RX_Buffer_Head - SENSORS_PROT_RX_Buffer_Tail) & (SENSORS_PROT_RX_BUFFER_LEN - 1) );
}

	// get a message from receicer circular buffer and place it in in parameter variable p
	// the message is removed form receiver circular buffer and placed in parameter variable p
void scomm_ReadBuffer( uint8_t * p, uint8_t len)
{
	uint8_t _i = 0;
		
		// copy message (data bytes) from receiver circular buffer to parameter variable p
	while( _i < len )
	{
		*p++ = SENSORS_PROT_RX_Buffer[ SENSORS_PROT_RX_Buffer_Tail ];
		INC_TO_2_PWR( SENSORS_PROT_RX_Buffer_Tail , 5 );	
		_i++;
	}
}

	// check the transmiter circular buffer used space (number of locatios that are used)
uint8_t scomm_TXCheck ( void )
{
	return ( SENSORS_PROT_TX_BUFFER_LEN - ( (SENSORS_PROT_TX_Buffer_Tail - SENSORS_PROT_TX_Buffer_Head ) & ( SENSORS_PROT_TX_BUFFER_LEN - 1 ) ) );
}

	// put a message from parameter variable p in transmitter circular buffer 
	// RETURN SCOMM_TX_BUFFER_FULL if there aren't enough free locations to store the message  
	// RETURN SCOMM_TX_BUFFER_LOADED if message was successfully stored in transmitter circular buffer
void scomm_WriteBuffer ( uint8_t * p, uint8_t len )
{
	uint8_t _i = 0; 
	
		// store len bytes in transmit circular buffer
	while( _i < len )
	{
		SENSORS_PROT_RX_Buffer[ SENSORS_PROT_TX_Buffer_Head ] = *p++ ;
		INC_TO_2_PWR( SENSORS_PROT_TX_Buffer_Head, 5 );
		_i++;			
	}
}
